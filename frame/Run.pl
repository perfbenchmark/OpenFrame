#!/usr/bin/perl

use strict;

use POSIX qw(strftime);
use Time::HiRes;
use Time::Local;
use IO::Handle;
use File::Path;
use FindBin;
############################################################################
#  UnixBench - Release 5.1.3, based on:
#  The BYTE UNIX Benchmarks - Release 3
#          Module: Run   SID: 3.11 5/15/91 19:30:14
# Original Byte benchmarks written by:
#       Ben Smith,              Tom Yager at BYTE Magazine
#       ben@bytepb.byte.com     tyager@bytepb.byte.com
# BIX:  bensmith                tyager
#
#######################################################################
# General Purpose Benchmark
# based on the work by Ken McDonell, Computer Science, Monash University
#
#  You will need ...
#       perl Time::HiRes IO::Handlecat cc chmod comm cp date dc df echo
#       kill ls make mkdir rm sed test time touch tty umask who
###############################################################################
#  Modification Log:
# $Header: run,v 5.2 88/01/12 06:23:43 kenj Exp $
#     Ken McDonell, Computer Science, Monash University
#     August 1, 1983
# 3/89 - Ben Smith - BYTE: globalized many variables, modernized syntax
# 5/89 - commented and modernized. Removed workload items till they
#        have been modernized. Added database server test.
# 11/14/89 - Made modifications to reflect new version of fstime
#        and elimination of mem tests.
# 10/22/90 - Many tests have been flipped so that they run for
#        a specified length of time and loops are counted.
# 4/3/91 - Cleaned up and debugged several test parameters - Ben
# 4/9/91 - Added structure for creating index and determing flavor of UNIX
# 4/26/91 - Made changes and corrections suggested by Tin Le of Sony
# 5/15/91 - Removed db from distribution
# 4/4/92    Jon Tombs <jon@robots.ox.ac.uk> fixed for GNU time to look like
#               BSD (don't know the format of sysV!)
# 12/95   - Massive changes for portability, speed, and more meaningful index
#               DCN     David C Niemi <niemi@tux.org>
# 1997.06.20    DCN     Fixed overflow condition in fstime.c on fast machines
# 1997.08.24    DCN     Modified "system", replaced double with
#                       whetstone-double in "index"
# 1997.09.10    DCN     Added perlbench as an Exhibition benchmark
# 1997.09.23    DCN     Added rgooch's select as an Exhibition benchmark
# 1999.07.28    DCN     "select" not compiled or run by default, because it
#                       does not compile on many platforms.  PerlBench also
#                       not run by default.
# 2007.09.26    IS      Huge rewrite -- see release notes in README.
# 2007.10.12    IS      Added graphics tests, categories feature.
# 2007.10.14    IS      Set and report LANG.  Added "grep" and "sysexec".
# 2007.12.22    IS      Tiny fixes; see README.
# 2011.01.13    KDL     Fix for parallel compilation.


############################################################################
# CONFIGURATION
############################################################################

# Version number of the script.
# my $version = "0.5";

# The setting of LANG makes a huge difference to some of the scores,
# particularly depending on whether UTF-8 is used.  So we always set
# it to the same value, which is configured here.
#
# If you want your results to be meaningful when compared to other peoples'
# results, you should not change this.  Change it if you want to measure the
# effect of different languages.
my $language = "en_US.utf8";

# The number of iterations per test.
my $longIterCount = 10;
my $shortIterCount = 3;

# Establish full paths to directories.  These need to be full pathnames
# (or do they, any more?).  They can be set in env.
# variable names are the first parameter to getDir() below.

# Directory where the test programs live.
my $BINDIR = Cwd::realpath(getDir('UB_BINDIR', $FindBin::Bin . "/../pgms"));

# Directory where the config files live.
my $CONFDIR = Cwd::realpath(getDir('UB_CONFDIR', $FindBin::Bin . "/../config"));

# Directory where the frame files live.
my $MAINDIR = Cwd::realpath(getDir('UB_FRMDIR', $FindBin::Bin . "/.."));

# Temp directory, for temp files.
my $TMPDIR = Cwd::realpath(getDir('UB_TMPDIR', $FindBin::Bin . "/tmp"));

# Directory to put results in.
my $RESULTDIR = Cwd::realpath(getDir('UB_RESULTDIR', $FindBin::Bin . "/../results"));

# Directory where the tests are executed.
my $DATADIR = Cwd::realpath(getDir('UB_TESTDIR', $FindBin::Bin . "/data"));

my $FRAMEDIR = Cwd::realpath(getDir('UB_FRAMEDIR', $FindBin::Bin . "/"));

my $BINSCFG = Cwd::realpath(getDir('UB_BUILDFILE', $FindBin::Bin . "/../bins.cfg"));

my $WKLDSCFG = Cwd::realpath(getDir('UB_WKLDSFILE', $FindBin::Bin . "/../workloads.cfg"));

my $ENVINFO  = Cwd::realpath(getDir('OF_ENVINFO',  $FindBin::Bin . "/../config/machine.info"));
my $HARDINFO = Cwd::realpath(getDir('OF_HARDINFO', $FindBin::Bin . "/../config/hard.info"));
my $SOFTINFO = Cwd::realpath(getDir('OF_SOFTINFO', $FindBin::Bin . "/../config/soft.info"));

############################################################################
# TEST SPECIFICATIONS
############################################################################

my $casesOpts = { };
my $catsRatioSum = 0;
my $affinities = [ ];

my @singleCoreBenchList = ();
my @failBenchList = ();
my $isSkipFail = 0;
my $isFail = 0;
my $isPeak = 0;

my $totalCasenum = 0;
my $INDEXBASE = "default.base";

require "$MAINDIR/TestCases.pl";
require "$CONFDIR/TestDefines.pl";

# CPU flags of interest.
my $x86CpuFlags = {
    'pae' => "Physical Address Ext",
    'sep' => "SYSENTER/SYSEXIT",
    'syscall' => "SYSCALL/SYSRET",
    'mmx' => "MMX",
    'mmxext' => "AMD MMX",
    'cxmmx' => "Cyrix MMX",
    'xmm' => "Streaming SIMD",
    'xmm2' => "Streaming SIMD-2",
    'xmm3' => "Streaming SIMD-3",
    'ht' => "Hyper-Threading",
    'ia64' => "IA-64 processor",
    'lm' => "x86-64",
    'vmx' => "Intel virtualization",
    'svm' => "AMD virtualization",
};

sub  trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };
my $file="${DATADIR}/version.ini";
open(my $config_fd, "<", $file) ||
                            die("Run: can't write to $file\n");
my $brandInfo = {
    'bench_name' => "",
    'version' => "",
    'bench_desc' => "",
};

while (<$config_fd>) {
    chomp;
    my ( $field, $val ) = split("=");
    next if (!$field || !$val);
    if (trim($field) eq "name") {
        $brandInfo->{'bench_name'} = trim($val);
    } elsif (trim($field) eq "version") {
        $brandInfo->{'version'} = trim($val);
    } elsif (trim($field) eq "desc") {
        $brandInfo->{'bench_desc'} = trim($val);
    }
}
close($config_fd);

############################################################################
# UTILITIES
############################################################################

# Exec the given command, and catch its standard output.
# We return an array containing the PID and the filehandle on the
# process' standard output.  It's up to the caller to wait for the command
# to terminate.
sub command {
    my ( $cmd ) = @_;

    my $pid = open(my $childFd, "-|");
    if (!defined($pid)) {
        die("Run: fork() failed (undef)\n");
    } elsif ($pid == 0) {
        exec($cmd);
        die("Run: exec() failed (returned)\n");
    }

    return ( $pid, $childFd );
}

# Get data from running a system command.  Used for things like getting
# the host OS from `uname -o` etc.
#
# Ignores initial blank lines from the command and returns the first
# non-blank line, with white space trimmed off.  Returns a blank string
# if there is no output; undef if the command fails.
sub getCmdOutput {
    my ( $cmd ) = @_;

    my ( $pid, $fd ) = command($cmd . " 2>/dev/null");
    my $result = "";
    while (<$fd>) {
        chomp;
        next if /^[ \t]*$/;

        $result = $_;
        $result =~ s/^[ \t]+//;
        $result =~ s/[ \t]+$//;
        last;
    }

    # Close the command and wait for it to die.
    waitpid($pid, 0);
    my $status = $?;

    return $status == 0 ? $result : undef;
}

sub getCmdOutputLines {
    my ( $cmd ) = @_;
    my @lines;
    my ( $pid, $fd ) = command($cmd . " 2>/dev/null");

    chomp(@lines = <$fd>);
    waitpid($pid, 0);
    return $? == 0 ? @lines: undef;
}


# Get a directory pathname from an environment variable, or the given
# default.  Canonicalise and return the value.
sub getDir {
    my ( $var, $def ) = @_;

    # If Environment variables(e.g. UB_RESULTDIR) is unset, use default value.
    my $val = $ENV{$var} || $def;

    # Only "execl.c" test needs the Environment variable(UB_BINDIR).
    $ENV{$var} = $val;

    return $val;
}

# Create direcotry(0755) if not exists.
sub createDirrectoriesIfNotExists {
    system("rm -rf ${TMPDIR}");
    foreach my $path (@_) {
        my $isDirectoryNotExists = ! -d $path;
        if ( $isDirectoryNotExists ) {
            mkpath($path, {chmod => 0755});
        }
    }
}

# Show use directories.
sub printUsingDirectories {
    printf "------------------------------------------------------------------------------\n";
    printf "   Use directories for:\n";
    printf "      * Results                      = ${RESULTDIR}\n";
    printf "------------------------------------------------------------------------------\n";
    printf "\n";
}


# Get the name of the file we're going to log to.  The name uses the hostname
# and date, plus a sequence number to make it unique.
sub logFile {
    my ( $sysInfo ) = @_;

    # If supplied output file name via Environment variable(UB_OUTPUT_FILE_NAME), then use it.
    #   * If exists same file, it will be overwrite completly.
    my $output_file_name_supplied_by_environment = $ENV{"UB_OUTPUT_FILE_NAME"};
    if ( defined($output_file_name_supplied_by_environment) && $output_file_name_supplied_by_environment ne "" ) {
        return ${RESULTDIR} . "/" . $output_file_name_supplied_by_environment;
    }


    # Use the date in the base file name.
    my $ymd = strftime "%Y-%m-%d", localtime;

    my $count = 1;
    while (1) {
        my $logName = sprintf "%s-%s-%03d", $sysInfo->{'name'}, $ymd, $count;
        my $reportDir = sprintf "%s/%s", ${RESULTDIR}, $logName;
        return $logName if (! -d $reportDir);
        ++$count;
    }
}


# Print a message to the named log file.  We use this method rather than
# keeping the FD open because we use shell redirection to send command
# output to the same file.
sub printLog {
    my ( $logFile, @args ) = @_;

    open(my $fd, ">>", $logFile) || abortRun("can't append to $logFile");
    printf $fd @args;
    close($fd);
}


# Display a number of something, auto-selecting the plural form
# if appropriate.  We are given the number, the singular, and the
# plural; if the plural is omitted, it defaults to singular + "s".
sub number {
    my ( $n, $what, $plural ) = @_;

    $plural = $what . "s" if !defined($plural);

    if (!defined($n)) {
        return sprintf "unknown %s", $plural;
    } else {
        return sprintf "%d %s", $n, $n == 1 ? $what : $plural;
    }
}


# Merge two sets of test parameters -- defaults and actual parameters.
# Return the merged parameter hash.
sub mergeParams {
    my ( $def, $vals ) = @_;

    my $params = { };
    foreach my $k (keys(%$def)) {
        $params->{$k} = $def->{$k};
    }
    foreach my $k (keys(%$vals)) {
        $params->{$k} = $vals->{$k};
    }

    $params;
}

sub parseAffinities {
    my ( $str ) = @_;
    my @cols = split(",\| ", $str);
    my $idx = 0;

    foreach my $col (@cols) {
        my ( $start, $append ) = split("-", $col);
        if ($start =~ /^\d+$/) {
            $affinities->[$idx++] = $start;
        } else {
            die("Run: parameter affinities should be number, but \"$start\" is not number\n");
        }

        if (defined($append)) {
            my ( $end, $tstep ) = split(":", $append);
            my $step = 1;

            if (defined($tstep)) {
                if ($tstep =~ /^\d+$/) {
                    $step = $tstep;
                } else {
                    die("Run: parameter affinities should be number, but \"$tstep\" is not number\n");
                }
            }

            if ($end =~ /^\d+$/) {
                for (my $v = $start + $step; $v <= $end; $v += $step) {
                    $affinities->[$idx++] = $v;
                }
            } else {
                die("Run: parameter affinities should be number, but \"$end\" is not number\n");
            }
        }
    }

    for (my $i = 0; $i < $idx; $i++) {
        print "gchen_tag: $affinities->[$i]\n";
    }
}

############################################################################
# SYSTEM ANALYSIS
############################################################################

sub getLinesFromFile {
    my ( $filename ) = @_;
    my $fd;
    my @lines;

    open($fd, "<", $filename) || die("Run: open $filename failed.\n");
    chomp(@lines = <$fd>);
    close($fd);

    @lines;
}

sub getInfoValue {
    my ( @match ) = @_;
    my ($tmp, $value) = split("=", @match[0]);
    $value;
}

sub shrinkInfoValue {
    my ( $str ) = @_;
    my $value = `echo "$str" | sed -e "s/\|/\\n/g" | sed -e "s/^ //g" | sed -e "s/ \$//" | sort | uniq -c | sed -e "s/^ .[ ]*//" | sed -e "s/ /*[/" -e "s/\$/]/" | xargs`;
    $value;
}

sub getInfoValues {
    my ( @match ) = @_;
    my @values;
    my $tmp;
    my $i;

    for ($i = 0; $i < @match; $i++) {
        ($tmp, @values[$i]) = split("=", @match[$i]);
    }

    @values;
}

sub getMachInfo {
    my $minfo;
    my @tmp0;
    my @tmp1;
    my @tmp2;
    my @tmp3;
    my @lines = getCmdOutputLines("cat \"$CONFDIR/machine.info\"");

    $minfo->{'machrole'} = getInfoValue(grep {/machrole=/} @lines);
    $minfo->{'description'} = getInfoValue(grep {/Description=/} @lines);
    $minfo->{'brandmodel'} = getInfoValue(grep {/brandmodel=/} @lines);
    $minfo->{'manufacturer'} = getInfoValue(grep {/manufacturer=/} @lines);
    $minfo->{'serialnumber'} = getInfoValue(grep {/serialnumber=/} @lines);
    $minfo->{'bios'} = getInfoValue(grep {/bios=/} @lines);
    $minfo->{'kernelname'} = getInfoValue(grep {/kernelname=/} @lines);
    $minfo->{'osrelease'} = getInfoValue(grep {/osrelease=/} @lines);
    $minfo->{'cpumodel'} = getInfoValue(grep {/cpumodel=/} @lines);
    $minfo->{'cpusocks'} = getInfoValue(grep {/cpusocks=/} @lines);
    $minfo->{'cpucores'} = getInfoValue(grep {/cpucores=/} @lines);
    $minfo->{'cputhrs'} = getInfoValue(grep {/cputhrs=/} @lines);
    $minfo->{'cpuid'} = getInfoValue(grep {/cpuid=/} @lines);
    $minfo->{'mac1'} = getInfoValue(grep {/mac1=/} @lines);
    $minfo->{'numacount'} = getInfoValue(grep {/numacount=/} @lines);
    $minfo->{'memtotal'} = getInfoValue(grep {/memtotal=/} @lines);
    $minfo->{'memcount'} = getInfoValue(grep {/memcount=/} @lines);
    $minfo->{'memmanu'} = shrinkInfoValue(getInfoValue(grep {/memmanu=/} @lines));
    $minfo->{'memtype'} = shrinkInfoValue(getInfoValue(grep {/memtype=/} @lines));
    $minfo->{'memspeed'} = shrinkInfoValue(getInfoValue(grep {/memspeed=/} @lines));
    $minfo->{'raidinfo'} = getInfoValue(grep {/raidinfo=/} @lines);

    $minfo->{'bash'} = getInfoValue(grep {/bash=/} @lines);
    $minfo->{'gcc'} = getInfoValue(grep {/gcc=/} @lines);
    $minfo->{'g++'} = getInfoValue(grep {/gpp=/} @lines);
    $minfo->{'binutils'} = getInfoValue(grep {/binutils=/} @lines);
    $minfo->{'python2'} = getInfoValue(grep {/python2=/} @lines);
    $minfo->{'python3'} = getInfoValue(grep {/python3=/} @lines);
    $minfo->{'java'} = getInfoValue(grep {/java=/} @lines);
    $minfo->{'glibc'} = getInfoValue(grep {/glibc=/} @lines);
    $minfo->{'glib'} = getInfoValue(grep {/glib=/} @lines);
    $minfo->{'stdc++'} = getInfoValue(grep {/stdcpp=/} @lines);
    $minfo->{'qt'} = getInfoValue(grep {/qt=/} @lines);
    $minfo->{'fs'} = getInfoValue(grep {/fs=/} @lines);

    @tmp0 = getInfoValues(grep {/harddisk=/} @lines);
    $minfo->{'harddisks'} = \@tmp0;
    @tmp1 = getInfoValues(grep {/ethcard=/} @lines);
    $minfo->{'ethcards'} = \@tmp1;
    @tmp2 = getInfoValues(grep {/cache=/} @lines);
    $minfo->{'caches'} = \@tmp2;
    @tmp3 = getInfoValues(grep {/dispcard=/} @lines);
    $minfo->{'dispcards'} = \@tmp3;

    $minfo->{'hid'} = getInfoValue(grep {/hid=/} @lines);
    $minfo->{'sid'} = getInfoValue(grep {/sid=/} @lines);

    $minfo;
}

# Extract interesting flags from the given processor flags string and
# convert them to descriptive names.
sub processCpuFlags {
    my ( $flagStr ) = @_;

    my @names;
    foreach my $f (sort split(/\s+/, $flagStr)) {
        my $name = $x86CpuFlags->{$f};
        push(@names, $name) if $name;
    }

    join(", ", @names);
}


# Get information on the CPUs in the system.  Returns a reference to an
# array of N entries, one per CPU, where each entry is a hash containing
# these fields:
# describing the model etc.  Returns undef if the information can't be got.
#
# future: on systems without /proc/cpuinfo, might check for Perl modules:
#   Sys::Info::Device::CPU or Sys::CpuAffinity
sub getCpuInfo {
    if (!("$^O" eq "darwin")) {
        open(my $fd, "<", "/proc/cpuinfo") || return undef;

        my $cpus = [ ];
        my $cpu = 0;
        while (<$fd>) {
            chomp;
            my ( $field, $val ) = split(/[ \t]*:[ \t]*/);
            next if (!$field || !$val);
            if ($field eq "processor") {
                $cpu = $val;
            } elsif ($field eq "model name") {
                my $model = $val;
                $model =~ s/  +/ /g; #Replace multiple spaces as a single space
                $model =~ s/ $//g;   #Delete last space
                $cpus->[$cpu]{'model'} = $model;
            } elsif ($field eq "bogomips" || $field eq "BogoMIPS") {
                $cpus->[$cpu]{'bogo'} = $val;
            } elsif ($field eq "flags") {
                $cpus->[$cpu]{'flags'} = processCpuFlags($val);
            }
        }

        close($fd);

        $cpus;

    } else {

        my $model = getCmdOutput("sysctl -n machdep.cpu.brand_string");
        my $flags = getCmdOutput("sysctl -n machdep.cpu.features | tr [A-Z] [a-z]");
        my $ncpu  = getCmdOutput("sysctl -n hw.ncpu");

        my $cpus = [ ];
        my $cpu = 0;

        for ($cpu = 0; $cpu < $ncpu; $cpu++) {
            $cpus->[$cpu]{'model'} = $model;
            $cpus->[$cpu]{'bogo'}  = 0;
            $cpus->[$cpu]{'flags'} = processCpuFlags($flags);
        }
        $cpus;
    }
}


# Get number of available (active) CPUs (not including disabled CPUs)
# or, if not num of available CPUs, the total number of CPUs on the system
# Returns undef if the information can't be obtained.
#
# There is no shortage of platform-specific methods to obtain this info.
# This routine -is not- exhaustive, but adds some additional portability.
# Most modern unix systems implement sysconf(_SC_NPROCESSORS_ONLN).
sub getNumActiveCpus {
    my $numCpus;

    #(POSIX::_SC_NPROCESSORS_ONLN value not typically provided by POSIX.pm)
    #$numCpus = POSIX::sysconf(POSIX::_SC_NPROCESSORS_ONLN);
    #if (defined($numCpus)) { chomp $numCpus; return $numCpus if $numCpus; }

    $numCpus = `getconf _NPROCESSORS_ONLN 2>/dev/null`;
    if (defined($numCpus)) { chomp $numCpus; return $numCpus if $numCpus; }

    $numCpus = `getconf NPROCESSORS_ONLN 2>/dev/null`;
    if (defined($numCpus)) { chomp $numCpus; return $numCpus if $numCpus; }

    $numCpus = `nproc 2>/dev/null`;
    if (defined($numCpus)) { chomp $numCpus; return $numCpus if $numCpus; }

    $numCpus = `python -c 'import os; print os.sysconf(os.sysconf_names["SC_NPROCESSORS_ONLN"]);' 2>/dev/null`;
    if (defined($numCpus)) { chomp $numCpus; return $numCpus if $numCpus; }

    # Windows
    return $ENV{"NUMBER_OF_PROCESSORS"} if $ENV{"NUMBER_OF_PROCESSORS"};

    return undef;
}


# Get information on the host system.  Returns a reference to a hash
# with the following fields:
#    name           Host name
#    os             Host OS name
#    osRel          Host OS release
#    osVer          Host OS version
#    mach           Host machine name (eg. "SparcStation 20", but on
#                   PC/Linux usually "i686" etc.)
#    platform       Hardware platform; on Linux, the base CPU type?
#    system         System name (eg. hostname and Linux distro, like
#                   "hostname: openSUSE 10.2 (i586)").
#    cpus           Value returned by getCpuInfo(), undef if not avail.
#    numCpus        Number of CPUs if known, else undef.
#    load           System load message as per "uptime".
#    numUsers       Number of users and/or open shell sessions.
sub getSystemInfo {
    my $info = { };
    my @tmp0;
    my @tmp1;

    # Get host system data.
    $info->{'name'} = getCmdOutput("hostname");
    $info->{'os'} = getCmdOutput("uname -o") || getCmdOutput("uname -s");
    $info->{'osRel'} = getCmdOutput("uname -r");
    $info->{'osVer'} = getCmdOutput("uname -v");
    $info->{'mach'} = $^O ne "aix"
      ? getCmdOutput("uname -m")
      : getCmdOutput("uname -p");
    $info->{'platform'} = getCmdOutput("uname -i") || "unknown";

    # Get the system name (SUSE, Red Hat, etc.) if possible.
    $info->{'system'} = $info->{'os'};
    if ( -r "/etc/SuSE-release" ) {
        $info->{'system'} = getCmdOutput("cat /etc/SuSE-release");
    } elsif ( -r "/etc/release" ) {
        $info->{'system'} = getCmdOutput("cat /etc/release");
    }

    # Get the language info.
    my $lang = getCmdOutput("printenv LANG");
    my $map = $^O ne "aix"
      ? getCmdOutput("locale -k LC_CTYPE | grep charmap") || ""
      : getCmdOutput("locale charmap") || "";
    $map =~ s/.*=//;
    my $coll = $^O ne "aix"
      ? getCmdOutput("locale -k LC_COLLATE | grep collate-codeset") || ""
      : getCmdOutput("locale | grep LC_COLLATE") || "";
    $coll =~ s/.*=//;
    $info->{'language'} = sprintf "%s (charmap=%s, collate=%s)",
                                   $lang, $map, $coll;

    # Get details on the CPUs, if possible.
    my $cpus = getCpuInfo();
    if (defined($cpus)) {
        $info->{'cpus'} = $cpus;
        $info->{'numCpus'} = scalar(@$cpus);
    }

    # Get available number of CPUs (not disabled CPUs), if possible.
    my $numCpus = getNumActiveCpus();
    if (defined($numCpus)) {
        $info->{'numCpus'} = $numCpus; # overwrite value from getCpuinfo()
    }

    # Get graphics hardware info.
    #$info->{'graphics'} = getCmdOutput("3dinfo | cut -f1 -d\'(\'");
    #get The number of total memory infomation -> $memoryTotal
    my $memoryTotal = getCmdOutput("cat /proc/meminfo | grep MemTotal | awk -F ' ' '{print\$2}'");

    # Get system run state, load and usage info.
    $info->{'runlevel'} = getCmdOutput("who -r | awk '{print \$2}'");
    $info->{'load'} = getCmdOutput("uptime");
    $info->{'numUsers'} = getCmdOutput("who | wc -l");

    $info->{'diskinfo'} = `df -Th`;
    $info->{'tmpdiskinfo'} = `df -Th \"${MAINDIR}\"`;
    $info->{'tmpdir'} = ${TMPDIR};
    $info->{'resultdir'} = ${RESULTDIR};
    $info->{'catsRatioSum'} = $catsRatioSum;
    $info->{'casesOpts'} = $casesOpts;
    $info->{'memTotal'} = $memoryTotal>>10;

    $info->{'minfo'} = getMachInfo();

    $info->{'ispeak'} = $isPeak;
    @tmp0 = getCmdOutputLines("cat \"${MAINDIR}/listbaseopt.mk\"");
    $info->{'baseopts'} = \@tmp0;
    if ($info->{'ispeak'} != 0) {
        @tmp1 = getCmdOutputLines("cat \"${MAINDIR}/listpeakopt.mk\"");
        $info->{'peakopts'} = \@tmp1;
    }
    $info;
}


############################################################################
# ERROR HANDLING
############################################################################

# Abort the benchmarking run with an error message.
sub abortRun {
    my ( $err ) = @_;

    printf STDERR "\n**********************************************\n";
    printf STDERR "Run: %s; aborting\n", $err;
    exit(1);
}

sub skipFailBench{
    my ($err) = @_;
    printf STDERR "\n**********************************************\n";
    printf STDERR "Run: %s; ERROR!\n", $err;
    return;
}
############################################################################
# TEST SETUP
############################################################################

# Do checks that everything's ready for testing.
sub preChecks {

    my ( $tests, $info, $makelog, $onlybuild ) = @_;
    my $cpus = $info->{'cpus'};
    my $i = 1;

    if (defined($cpus)) {
        $i = $#$cpus + 1;
    }
    if ($i == 0) {
        $i = `grep processor /proc/cpuinfo | wc -l | xargs echo -n`;
        printf "cpunum = <$i> \n";
    }

    open(my $fdd, ">", $WKLDSCFG) ||
                            die("Run: can't write to $WKLDSCFG\n");
    printf $fdd "  DIRS= \\\n";
    foreach my $t (@$tests) {
        if (exists($TestCases::testParams->{$t})) {
            if (exists($TestCases::testParams->{$t}->{'workloaddir'})) {
                printf $fdd "\t $TestCases::testParams->{$t}->{'workloaddir'} \\\n";
            } elsif (exists($TestCases::testParams->{$t}->{'prog'})) {
    #             printf $fdd "\t $TestCases::testParams->{$t}->{'__subdir'} \\\n";
		my @word = split("/", $TestCases::testParams->{$t}->{'prog'});
		my $icount = @word;
                    for(my $j = 0; $j < $icount; $j++)
                    {
                        my $strtmp = @word[$j];
                        if($strtmp eq "workloads")
                        {
                            printf $fdd "\t @word[$j+1] \\\n";
                            last;
                        }
                    }
            }
        }
    }
    printf $fdd "\n";
    close($fdd);

    # Set the language.
    $ENV{'LANG'} = $language;

    # Check that the required files are in the proper places.
    my $make = $ENV{MAKE} || "make";
    my $ret = system("$BINDIR/build.sh $i $makelog");
    if ($ret != 0) {
        abortRun("\"cd $BINDIR/..; $make all\" failed");
    }

    if($onlybuild == 1) {
        exit(0);
    }

    # Create a script to kill this run.
    system("echo \"kill -9 $$\" > \"${TMPDIR}/kill_run\"");
    chmod(0755, $TMPDIR . "/kill_run");
}

sub helpinfo() {
    my @suites = sort keys(%$TestCases::testCats);
    print "\nUsage:\n";
    print "    Run [Options] [testcases]\n";
    print "    Run [Options] [testsuites]\n";
    print "    Run [Options] -f [server.lst | desktop.lst]\n";
    print "\nExamples:\n";
    print "    Run -m [brandModel]\n";
    print "    Run -i 1 -c 1 -f server.lst\n";
    print "    Run -i 1 -c 4 -f desktop.lst\n";
    print "    Run -i 3 -c 2 dhry2reg fstime\n";
    print "    Run -i 1 -c 2 @suites\n";
    print "    Run -i 3 -c 4 -b server dhry2reg\n";
    print "    Run -i 1 -t 3 hpcg_s\n";
    print "\nOptions:\n";
    print "    -m, -machine       : get machine info, need to specify brand model and bios\n";
    print "    -i, -iterations    : set iterations number\n";
    print "    -c, -copies        : set copies number\n";
#    print "    -t, -threads       : set threads number\n";
    print "    -f, -config        : specify file with testcases config list: server.lst/desktop.lst\n";
    print "    -b, -baseline      : specify file with baseline list: server.base/desktop.base\n";
    print "    -h, -help          : show help info\n";
    print "    -q, -quiet         : show log in quiet mode\n";
    print "    -v, -verbose       : show log in verbose mode\n";
    print "    -a, -affinity      : specify affinity\n";
    print "    -s, -skipfail      : skip failed testcases without exit\n";
    print "    -l, -listcases     : show testcases list\n";
    print "    -L, -listcasesattr : show testcases attribute list\n";
#    print "    -C, -clean         : clean trash after compiled\n";
    print "    -D, -distclean     : clean all trash\n";
    print "    -B, -onlybuild     : compile testcases\n";
    print "    -V, -version       : show the version\n";
    print "    -peak              : specify compile opt for testcases\n";
    print "    -listpeakopt       : show testcases peak opt\n";

    my $casenum = 0;
    print "\nDetails:\n";
    print "    1. Test config file after -f parameter should be server.lst or desktop.lst.\n";
    print "    2. The testsuites should be choosed from below: < @suites >.\n";
    print "    3. The testcases should be choosed from below: < ";
    my @paras = sort keys(%$TestCases::testParams);
    foreach my $t (@paras) {
        print("$t ");
        $casenum++;
    }
    print(">, total number=$casenum.\n\n");
}

# index: all testcases except LAB and GRAPHIO category.
sub getIndexList {
    my $tests = ();
    my @sorted = sort keys(%$TestCases::testParams);
    foreach my $t (@sorted) {
        my $catval = $TestCases::testParams->{$t}->{'cat'};
        if (($catval ne "LAB") && ($catval ne "GRAPHIO")){
            push(@$tests, $t);
        }
    }
    $TestDefines::index = $tests;
    $TestDefines::testList->{'index'} = $tests;

    $tests
}

# gindex: all testcases except LAB category.
sub getGindexList {
    my $tests = ();
    my @sorted = sort keys(%$TestCases::testParams);
    foreach my $t (@sorted) {
        my $catval = $TestCases::testParams->{$t}->{'cat'};
        if ($catval ne "LAB") {
            push(@$tests, $t);
        }
    }
    $TestDefines::gindex = $tests;
    $TestDefines::testList->{'gindex'} = $tests;

    $tests
}

# Get baseline name by config list file name
sub getBaselineByConfigfile{
    my ($fname) = @_;
    my $bname = "";
    my @keylist=("server", "desktop", "default");

    while (my $keyword = shift(@keylist)) {
        if ($fname =~ /^$keyword/) {
            print "\nget baseline name by config file name: fname=<$fname>, keyword=<$keyword>.\n";
            $bname = "$keyword.base";
        }
    }
    if ($bname) {
        my $fileExist = -e "./config/$bname";
        if (!$fileExist) {
            helpinfo();
            die("Run: baseindex file error, <$bname> can not be opened\n");
        }
    }
    $bname;
}

# Parse the command arguments.
sub parseArgs {
    my @words = @_;

    # The accumulator for the bench units to be run.
    my $tests = [ ];
    my $intmp = [ ];
    my $params = { 'tests' => $tests, 'cmdargs' => $intmp };
    my $bname_opt = "";

    # Generate the requested list of bench programs.
    my $opt;
    my $word;
    my @cmdargs = @words;
    foreach my $t11 (@cmdargs) {
        push(@$intmp, $t11);
    }
    $params->{'cmdargs'} = $intmp;

    $ENV{'OMP_NUM_THREADS'} = 1;
    while ($word = shift(@words)) {
        if ($word !~ m/^-/) {               # A test name.
            if ($word eq "all") {
                my @list = sort keys(%$TestCases::testParams);
                foreach my $t (@list) {
                    push(@$tests, $t);
                }
            } elsif (exists($TestCases::testCats->{$word})) { # TestCats
                my @sorted = sort keys(%$TestCases::testParams);
                foreach my $t (@sorted) {
                    my $catval = $TestCases::testParams->{$t}->{'cat'};
                    if ($catval eq $word) {
                        push(@$tests, $t);
                    }
                }
            } elsif (exists($TestDefines::testList->{$word})) { # TestSuite
                my $val = $TestDefines::testList->{$word} || [ $word ];
                push(@$tests, @$val);
            } elsif (exists($TestCases::testParams->{$word})) { # TestCases
                push(@$tests, $word);
            } else {
                helpinfo();
                die("Run: unknown test \"$word\"\n");
            }
        } elsif (($word eq "-q") || ($word eq "-quiet")){
            $params->{'verbose'} = 0;
        } elsif (($word eq "-s") || ($word eq "-skipfail")) {
            $params->{'mark'} = 1;
        } elsif (($word eq "-v") || ($word eq "-verbose")) {
            $params->{'verbose'} = 2;
        } elsif (($word eq "-S") || ($word eq "-sendfile")) {
            $params->{'sendfile'} = 1;
        } elsif (($word eq "-i") || ($word eq "-iterations")) {
            my $num = shift(@words);
            if ($num =~ /^\d+$/) {
                $params->{'iterations'} = $num;
            } else {
                helpinfo();
                die("Run: parameter iterations should be number, but \"$num\" is not number\n");
            }
        } elsif (($word eq "-c") || ($word eq "-copies")) {
            my $num = shift(@words);
            if ($num =~ /^\d+$/) {
                if ($num > 1) {
                    if (!defined($params->{'copies'})) {
                        $params->{'copies'} = [ ];
                    }
                    push(@{$params->{'copies'}}, $num);
                }
            } else {
                helpinfo();
                die("Run: parameter copies should be number, but \"$num\" is not number\n");
            }
        } elsif (($word eq "-f") || ($word eq "-config")) {
            my $fname = shift(@words);
            my $bname = "";
            if ($fname !~ /\.lst$/) {
                $fname = $fname . ".lst"
            }
            $bname = getBaselineByConfigfile($fname);
            if ("$bname") {
                $INDEXBASE = $bname;
            }
            $params->{'testfile'} = $fname;
            $params->{'tests'} = readConfigFromFile($CONFDIR . "/" . $fname);
            $params->{'basefile'} = $INDEXBASE;
        } elsif (($word eq "-b") || ($word eq "-baseline")) {
            $bname_opt = shift(@words);
            if ($bname_opt !~ /\.base$/) {
                $bname_opt = $bname_opt . ".base";
            }
            my $fileExist = -e "./config/$bname_opt";
            if (!$fileExist) {
                helpinfo();
                die("Run: -b parameter <$bname_opt> can not be opened\n");
            }
        } elsif (($word eq "-t") || ($word eq "-threads")) {
            my $num = shift(@words);
            if ($num =~ /^\d+$/) {
                if ($num > 1) {
                    $ENV{'OMP_NUM_THREADS'} = $num;
                }
            } else {
                helpinfo();
                die("Run: parameter threads should be number, but \"$num\" is not number\n");
            }
        } elsif (($word eq "-C") || ($word eq "-clean")) {
            system("make clean");
            exit(0);
        } elsif (($word eq "-D") || ($word eq "-distclean")) {
            system("rm ${MAINDIR}/TestCases.pl");
            system("make spotless");
            exit(0);
        } elsif (($word eq "-p") || ($word eq "-peak")) {
            $isPeak = 1;
        } elsif (($word eq "-l") || ($word eq "-listcases")) {
            printTestCases();
            exit(0);
        } elsif (($word eq "-L") || ($word eq "-listcasesattr")) {
            printTestCasesAttr();
            exit(0);
        } elsif (($word eq "-B") || ($word eq "-onlybuild") ){
             # Check that the required files are in the proper places.
            $params->{'onlybuild'} = 1;
        } elsif (($word eq "-a") || ($word eq "-affinity")) {
            my $str = shift(@words);
            parseAffinities($str);
            $ENV{'GOMP_CPU_AFFINITY'} = $str;
        } elsif (($word eq "-h") || ($word eq "-help")) {
            helpinfo();
            exit(0);
        } elsif (($word eq "-V") || ($word eq "-version")) {
            print($brandInfo->{'bench_name'} ." version: " . $brandInfo->{'version'} ."\n");
            exit(0);
        } elsif (($word eq "-m") || ($word eq "-machine")) {
            my $num = @words;
            if (($num < 1) || ($num > 2)) {
                print("\nplease input brandModel and bios as below:");
                print("\n    Run -m \"brand model\" \"bios version\"\n");
                print("\nfor example:");
                print("\n    Run -m \"Huanghe Desktop x3214-A1\" \"BaiAo v1.0\"\n\n");
                exit(0);
            }
            my $brand = shift(@words);
            my $bios = "";
            if ($num == 2) {
                $bios = shift(@words);
            }
            system("rm -rf $ENVINFO $HARDINFO $SOFTINFO;
		    touch  $ENVINFO $HARDINFO $SOFTINFO;");
            system("sudo $BINDIR/machinfo \"$brand\" \"$bios\"");
            print("get machine info, brand=<$brand>, bios=<$bios>\n");
            exit(0);
        } else {
            helpinfo();
            die("Run: unknown option $word\n");
        }
    }

    # check config/machine.info
    system("$BINDIR/check_machinfo.sh");
    my $ret = $?;
    if ($ret != 0) {
        print("\nFirstly, please run comand as below:");
        print("\n    Run -m \"brand model\" \"bios version\"\n\n");
        exit(0);
    }

    # adjust $INDEXBASE
    if ("$bname_opt" ne "") {
        $INDEXBASE = $bname_opt;
    }
    $params->{'basefile'} = $INDEXBASE;
    print("\noptfile: bname_opt=<$bname_opt>, configfile=<$params->{'testfile'}>, basefile=<$params->{'basefile'}>.\n");

    # Always set OMP_STACKSIZE
    $ENV{'OMP_STACKSIZE'} = '120M';

    $params;
}

############################################################################
# CONFIG INPUT
############################################################################

# Read a set of benchmarking config informations from the given file.
sub readConfigFromFile {
    my ( $file ) = @_;

    print "config file name: $file\n";

    # Attempt to get the config data file; if we can't, die
    open(my $fd, "<", $file) || die("Run: open $file failed.\n");

    my $tests = [ ];
    while (<$fd>) {
        chomp;

        # Dump comments, ignore blank lines.
        s/#.*//;
        next if /^\s*$/;

        my ( $name, $ratio, $params) = split(/\|/);

        if (exists($TestCases::testCats->{$name})) {
            my $cat = $TestCases::testCats->{$name};
            $cat->{'ratio'} = $ratio;
            $catsRatioSum += $ratio;
        } else {
            $casesOpts->{$name}{'weight'} = $ratio;
            if (exists($TestCases::testParams->{$name})) {
                my $cat = $TestCases::testCats->{$TestCases::testParams->{$name}->{'cat'}};
                $cat->{'casesRatioSum'} += $ratio;
                if (defined($params)) {
                    $casesOpts->{$name}{'params'} = $params;
                } 
                push(@$tests, $name);
            } else {
                helpinfo();
                die("Run: unknown test \"$name\"\n");
            }
        }
    }

    close($fd);

    $tests;
}

############################################################################
# RESULTS INPUT / OUTPUT
############################################################################

# Read a set of benchmarking results from the given file.
# Returns results in the form returned by runTests(), but without the
# individual pass results.
sub readResultsFromFile {
    my ( $file ) = @_;

    # Attempt to get the baseline data file; if we can't, just return undef.
    open(my $fd, "<", $file) || return undef;

    my $results = { };
    while (<$fd>) {
        chomp;

        # Dump comments, ignore blank lines.
        s/#.*//;
        next if /^\s*$/;

        my ( $name, $time, $slab, $sum, $score, $iters ) = split(/\|/);
        my $bresult = { };
        $bresult->{'score'} = $score;
        $bresult->{'scorelabel'} = $slab;
        $bresult->{'time'} = $time;
        $bresult->{'iterations'} = $iters;

        $results->{$name} = $bresult;
    }

    close($fd);

    $results;
}


############################################################################
# RESULTS PROCESSING
############################################################################

# Process a set of results from a single test by averaging the individal
# pass results into a single final value.
# First, though, dump the worst 1/3 of the scores.  The logic is that a
# glitch in the system (background process waking up, for example) may
# make one or two runs go slow, so let's discard those.
#
# $bresult is a hashed array representing the results of a single test;
# $bresult->{'passes'} is an array of the output from the individual
# passes.
sub combinePassResults {
    my ( $bench, $tdata, $bresult, $logFile ) = @_;

    $bresult->{'bname'} = $bench;
    $bresult->{'cat'} = $tdata->{'cat'};

    # Computed results.
    my $iterations = 0;
    my $totalTime = 0;
    my $sum = 0;
    my $product = 0;
    my $label;
    my $countlist = [];

    my $pres = $bresult->{'passes'};

    # We're going to throw away the worst 1/3 of the pass results.
    # Figure out how many to keep.
    my $npasses = scalar(@$pres);
    my $ndump = int($npasses / 3);

    foreach my $presult (sort { $a->{'COUNT0'} <=> $b->{'COUNT0'} } @$pres) {
        my $count = $presult->{'COUNT0'};
        my $timebase = $presult->{'COUNT1'};
        $label = $presult->{'COUNT2'};
        my $time = $presult->{'TIME'} || $presult->{'elapsed'};

        # Skip this result if it's one of the worst ones.
        if ($ndump > 0) {
            printLog($logFile, "*Dump score: %12.4f\n", $count);
            --$ndump;
            next;
        }

        # Count this result.
        ++$iterations;
        push(@$countlist, $count);
        printLog($logFile, "Count score: %12.4f\n", $count);

        # If $timebase is 0 the figure is a rate; else compute
        # counts per $timebase.  $time is always seconds.
        if ($timebase > 0 && $time > 0) {
            $sum += $count / ($time / $timebase);
            $product += log($count) - log($time / $timebase) if ($count > 0);
        } else {
            $sum += $count;
            $product += log($count) if ($count > 0);
        }
        $totalTime += $time;
    }

    computedStdev($countlist, $iterations, $logFile);

    # Save the results for the benchmark.
    if ($iterations > 0) {
        $bresult->{'score'} = exp($product / $iterations);
        $bresult->{'scorelabel'} = $label;
        $bresult->{'time'} = $totalTime / $iterations;
        $bresult->{'iterations'} = $iterations;
    } else {
        $bresult->{'error'} = "No measured results";
    }
}

# Computed stdev
sub computedStdev {
    my ( $countlist, $iterations, $logFile ) = @_;
    my $ave = 0;
    my $stdev = 0;
    my $diff = 0;
    my $countsum = 0;

    if ($iterations > 1) {
        foreach my $clist (@$countlist) {
            $countsum += $clist;
        }
        $ave = $countsum / $iterations;
        foreach my $cl (@$countlist) {
            $diff += ($cl - $ave)**2;
        }
        $stdev = ($diff / ($iterations - 1))**(1/2);
        printLog($logFile, "\nstdev: %12.4f\n", $stdev);
    }
}

# Index the given full benchmark results against the baseline results.
# $results is a hashed array of test names to test results.
#
# Adds the following fields to each benchmark result:
#    iscore         The baseline score for this test
#    index          The index of this test against the baseline
# Adds the following fields to $results:
#    indexed        The number of tests for which index values were
#                   generated
#    fullindex      Non-0 if all the index tests were indexed
#    index          The computed overall index for the run
# Note that the index values are computed as
#    result / baseline * 10
# so an index of 523 indicates that a test ran 52.3 times faster than
# the baseline.
sub indexResults {
    my ( $results ) = @_;

    my $fscore = 1.0;
    my $catnum = 0;

    # Read in the baseline result data.  If we can't get it, just return
    # without making indexed results.
    #my $index = readResultsFromFile($CONFDIR . "/index.base");

    my $index = readResultsFromFile($CONFDIR . "/$INDEXBASE");
    # Count the number of results we have (indexed or not) in
    # each category.
    my $numCat = { };
    foreach my $bench (@{$results->{'list'}}) {
        my $bresult = $results->{$bench};
        ++$numCat->{$bresult->{'cat'}};
    }
    $results->{'numCat'} = $numCat;

    my $numIndex = { };
    my $indexed = { };
    my $sum = { };
    my $fsum = { };
    foreach my $bench (sort(keys(%$index))) {
        # Get the test data for this benchmark.
        my $tdata = $TestCases::testParams->{$bench};
        if (!defined($tdata)) {
            #abortRun("unknown benchmark \"$bench\" in $CONFDIR/index.base");
            abortRun("unknown benchmark \"$bench\" in $CONFDIR/$INDEXBASE");
        }

        # Get the test category.  Count the total tests in this cat.
        my $cat = $tdata->{'cat'};
        my $casesRatioSum = $TestCases::testCats->{$cat}{'casesRatioSum'};
        my $weight = $casesOpts->{$bench}{'weight'};

        ++$numIndex->{$cat};

        # If we don't have a result for this test, skip.
        next if (!defined($results->{$bench}));

        # Get the index and actual results.  Calcluate the score.
        my $iresult = $index->{$bench};
        my $bresult = $results->{$bench};
        my $ratio = $bresult->{'score'} / $iresult->{'score'};

        # Save the indexed score.
        $bresult->{'iscore'} = $iresult->{'score'};
        $bresult->{'index'} = $ratio * 10;
        if ($casesRatioSum != 0) {
            $bresult->{'findex'} = $bresult->{'index'} ** $weight;
        }

        # Sun the scores, and count this test for this category.
        $sum->{$cat} += log($ratio) if ($ratio > 0.000001);
        if ($casesRatioSum != 0) {
            $fsum->{$cat} += log($ratio ** $weight) if ($ratio > 0.000001);
        }
        ++$indexed->{$cat};
    }

    # Calculate the index scores per category.
    $results->{'indexed'} = $indexed;
    $results->{'numIndex'} = $numIndex;
    foreach my $c (keys(%$indexed)) {
        if ($indexed->{$c} > 0) {
            #printf "------c=%s, indexC=%f, sumC=%f, fsum=%f \n", $c, $indexed->{$c}, $sum->{$c}, $fsum->{$c};
            $results->{'index'}{$c} = exp($sum->{$c} / $indexed->{$c}) * 10;
            if (defined($fsum->{$c})) {
                my $casesRatioSum = $TestCases::testCats->{$c}{'casesRatioSum'};
                $results->{'findex'}{$c} = exp($fsum->{$c} / $casesRatioSum) * 10;
            } else {
                $results->{'findex'}{$c} = $results->{'index'}{$c};
            }
            if ($catsRatioSum > 0) {
                my $ffiscore = $results->{'findex'}{$c} ** $TestCases::testCats->{$c}{'ratio'};
                $results->{'ffiscore'}{$c} = $ffiscore;
                $fscore *= $ffiscore;
            } else {
                $catnum++;
                $fscore *= $results->{'index'}{$c};
            }
        }
    }

    if ($catsRatioSum > 0) {
        $results->{'gmean'} = $fscore ** (1.0 / $catsRatioSum);
    } else {
        if ($catnum > 0) {
            $results->{'gmean'} = $fscore ** (1.0 / $catnum);
        } else {
            $results->{'gmean'} = 0;
        }
    }
}


############################################################################
# TEST EXECUTION
############################################################################

# Exec the given command in a sub-process.
#
# In the child process, we run the command and store its standard output.
# We also time its execution, and catch its exit status.  We then write
# the command's output, plus lines containing the execution time and status,
# to a pipe.
#
# In the parent process, we immediately return an array containing the
# child PID and the filehandle to the pipe.  This allows the caller to
# kick off multiple commands in parallel, then gather their output.
sub commandBuffered {
    my ( $cmd ) = @_;

    # Create a pipe for parent-child communication.
    my $childReader;
    my $parentWriter;
    pipe($childReader, $parentWriter) || abortRun("pipe() failed");
    $parentWriter->autoflush(1);

    # Fork off the child process.
    my $pid = fork();
    if (!defined($pid)) {
        abortRun("fork() failed (undef)");
    } elsif ($pid == 0) {
        # Close the other end of the pipe.
        close $childReader;

        # Start the clock and spawn the command.
        my $benchStart = Time::HiRes::time();
        my ( $cmdPid, $cmdFd ) = command($cmd);

        # Read and buffer all the command's output.
        my $output = [ ];
        while (<$cmdFd>) {
            push(@$output, $_);
        }

        # Stop the clock and save the time.
        my $benchEnd = Time::HiRes::time();
        my $elTime = $benchEnd - $benchStart;
        push(@$output, sprintf "elapsed|%f\n", $elTime);

        # Wait for the child to die so we can get its status.
        # close($cmdFd);  Doesn't work???
        waitpid($cmdPid, 0);
        my $status = $?;
        push(@$output, sprintf "status|%d\n", $status);
	push(@$output, sprintf "timeS|%d(%s)\n", $benchStart, strftime("%Y-%m-%d,%H:%M:%S", localtime($benchStart)));
	push(@$output, sprintf "timeE|%d(%s)\n", $benchEnd,   strftime("%Y-%m-%d,%H:%M:%S", localtime($benchEnd)));

        # Now that we've got the time, play back all the output to the pipe.
        # The parent can read this at its leisure.
        foreach my $line (@$output) {
            print $parentWriter $line;
        }

        # Terminate this child.
        close $parentWriter;
        exit(0);
    }

    # Close the other end of the pipe.
    close $parentWriter;

    return ( $pid, $childReader );
}


# Read the results of a benchmark execution from a child process, given
# its process ID and its filehandle.  Create a results hash structure
# containing the fields returned by the child, plus:
#    pid            The child's process ID
#    status         The child's exit status
#    ERROR          Any stderr output from the child that isn't result data
# Note that ay result fields with ultiple values are split; so eg.
#    COUNT|x|y|x
# becomes
#    COUNT0 = x
#    COUNT1 = y
#    COUNT2 = z
sub readResults {
    my ( $pid, $fd ) = @_;

    my $presult = { 'pid' => $pid };

    # Read all the result lines from the child.
    while (<$fd>) {
        chomp;

        my ( $field, @params ) = split(/\|/);
        if (scalar(@params) == 0) {            # Error message.
            $presult->{'ERROR'} .= "\n" if ($presult->{'ERROR'});
            $presult->{'ERROR'} .= $field;
        } elsif (scalar(@params) == 1) {       # Simple data.
            $presult->{$field} = $params[0];
        } else {                               # Compound data.
            # Store the values in separate fields, named "FIELD$i".
            for (my $x = 0; $x < scalar(@params); ++$x) {
                $presult->{$field . $x} = $params[$x];
            }
        }
    }

    # If the command had an error, make an appropriate message if we
    # don't have one.
    if ($presult->{'status'} != 0 && !defined($presult->{'ERROR'})) {
        $presult->{'ERROR'} = "command returned status " . $presult->{'status'};
    }

    # Wait for the child to die.
    close($fd);
    waitpid($pid, 0);

    $presult;
}


# Execute a benchmark command.  We set off a given number of copies in
# parallel to exercise multiple CPUs.
#
# We return an array of results hashes, one per copy; each one is as
# returned by readResults().
sub executeBenchmark {
    my ( $command, $copies, $params, $logFile ) = @_;
    my $pres = [ ];

    # Prepare before run multiple copies.
    # If not define 'prepare' in CaseConfig.pl, prepare nothing.
    my $precmd = $params->{'prepare'};
    if(defined($precmd)) {
        # if call exec(), it will start new process.
        # so call system(), it will block main process.
        system("$precmd" . " >> $logFile");
        my $ret=$?;
        if ($ret != 0) {
            die("\nError: prepare fail<$ret>");
        }
    }

    # Array of contexts for all the copies we're running.
    my $ctxt = [ ];

    # Kick off all the commands at once.
    for (my $i = 0; $i < $copies; ++$i) {
        my $cmd;

        if (($ENV{'OMP_NUM_THREADS'} == 1) && exists($affinities->[$i])) {
            $cmd = sprintf "taskset -c %s %s", $affinities->[$i], $command;
        } else {
            $cmd = $command;
        }
        my ( $cmdPid, $cmdFd ) = commandBuffered($cmd);
        $ctxt->[$i] = {
            'pid'     => $cmdPid,
            'fd'      => $cmdFd,
        };
    }

    # Now, we can simply read back the command results in order.  Because
    # the child processes read and buffer the results and time the commands,
    # there's no need to use select() to read the results as they appear.
    my $pres = [ ];
    for (my $i = 0; $i < $copies; ++$i) {
        my $presult = readResults($ctxt->[$i]{'pid'}, $ctxt->[$i]{'fd'});
        push(@$pres, $presult);
    }

    $pres;
}


# Run one iteration of a benchmark, as specified by the given
# benchmark parameters.  We run multiple parallel copies as
# specified by $copies.
sub runOnePass {
    my ( $params, $verbose, $logFile, $copies, $reportFds ) = @_;

    # Get the command to run.
    my $command = $params->{'command'};
    if ($verbose > 1) {
        printf "\n";
        printf "COMMAND: \"%s\"\n", $command;
        printf "COPIES: \"%d\"\n", $copies;
    }

    # Remember where we are, and move to the data directory for test.
    my $pwd = `pwd`;
    chdir($DATADIR);

    # Execute N copies of the benchmark in parallel.
    my $copyResults = executeBenchmark($command, $copies, $params, $logFile);
    printLog($logFile, "\n");

    # Move back home.
    chdir($pwd);

    # Sum up the scores of the copies.
    my $count = 0;
    my $time = 0;
    my $elap = 0;
    foreach my $res (@$copyResults) {
        # Log the result data for each copy.
        foreach my $k (sort(keys(%$res))) {
            printLog($logFile, "# %s: %s\n", $k, $res->{$k});
        }
        printLog($logFile, "\n");

        # If it failed, bomb out.
        if (defined($res->{'ERROR'})) {
            my $key = $params->{'key'};
            my $name = $params->{'logmsg'};

            if(!$isSkipFail){
                summarizeNullJson($reportFds->{'json'});
                 abortRun("$key: \"$name\": ". $res->{'ERROR'});
            }else{
                 $isFail = 1;
                 setFailBench($key);
                 skipFailBench("$key: \"$name\": ". $res->{'ERROR'});
                 return;
            }
        }

        # Count up the score.
        $count += $res->{'COUNT0'};
        $time += $res->{'TIME'} || $res->{'elapsed'};
        $elap += $res->{'elapsed'};
    }

    # Make up a combined result.
    my $passResult = $copyResults->[0];
    $passResult->{'COUNT0'} = $count;
    $passResult->{'TIME'} = $time / $copies;
    $passResult->{'elapsed'} = $elap / $copies;

    $passResult;
}


sub runBenchmark {
    my ( $bench, $tparams, $params, $verbose, $logFile, $copies, $reportFds ) = @_;

    # Make up the command string based on the parameters.
    my $prog = $params->{'prog'} || $BINDIR . "/" . $bench;
    my $command = sprintf "\"%s\" %s", $prog, $params->{'options'};
    $command .= " < \"" . $params->{'stdin'} . "\"" if ($params->{'stdin'});
    $command .= " 2>&1";
    $command .= $params->{'stdout'} ? (" >> \"" . $logFile . "\"") : " > /dev/null";
    $params->{'command'} = $command;

    # Set up the benchmark results structure.
    my $bresult = { 'name' => $bench, 'msg' => $params->{'logmsg'} };

    if ($verbose > 0) {
        printf "\n%d x %s: %s,", $copies, $bench, $params->{'logmsg'};
    }

    printLog($logFile,
             "\n########################################################\n");
    printLog($logFile, "%s:%s -- %s\n",
             $bench, $params->{'logmsg'}, number($copies, "copy", "copies"));
    printLog($logFile, "==> %s\n\n", $command);

    # Run the test iterations, as given by the "repeat" parameter.
    my $repeats = $shortIterCount;
    $repeats = $longIterCount if $params->{'repeat'} eq 'long';
    $repeats = 1 if $params->{'repeat'} eq 'single';
    my $pres = [ ];
    for (my $i = 1; $i <= $repeats; ++$i) {
        printLog($logFile, "#### Pass %d\n\n", $i);

        # make an attempt to flush buffers
        system("sync; sleep 1; sync; sleep 2");
        # display heartbeat
        if ($verbose > 0) {
            printf " %d", $i;
        }

        # Execute one pass of the benchmark.
        my $presult = runOnePass($params, $verbose, $logFile, $copies, $reportFds);
        if ($isFail) {
            return;
        }
        push(@$pres, $presult);
    }
    $bresult->{'passes'} = $pres;

    # Calculate the averaged results for this benchmark.
    combinePassResults($bench, $tparams, $bresult, $logFile);

    # Log the results.
    if ($copies == 1) {
        printLog($logFile, "\n>>>> Results of 1 copy\n");
    } else {
        printLog($logFile, "\n>>>> Sum of %d copies\n", $copies);
    }
    foreach my $k ( 'score', 'time', 'iterations' ) {
        printLog($logFile, ">>>> %s: %s\n", $k, $bresult->{$k});
    }
    printLog($logFile, "\n");

    # Some specific cleanup routines.
    #if ($bench eq "C") {
        #unlink(${DATADIR} . "/cctest.o");
        #unlink(${DATADIR} . "/a.out");
    #}

    if ($verbose > 0) {
        printf "\n";
    }

    $bresult;
}

require "$FRAMEDIR/output/Console.pl";
require "$FRAMEDIR/output/Text.pl";
require "$FRAMEDIR/output/Html.pl";
require "$FRAMEDIR/output/Pdf.pl";
require "$FRAMEDIR/output/CSV.pl";
require "$FRAMEDIR/output/Json.pl";
require "$FRAMEDIR/output/Sendfile.pl";

# Run the named benchmarks.
sub runTestsWithReport {
    my ( $tests, $verbose, $logFile, $copies,
         $systemInfo, $reportFds, $inparams) = @_;
    if ($verbose > 1) {
        printf "Run with %s\n", number($copies, "copy", "copies");
    }

    # Run all the requested tests and gather the results.
    my $results = { 'start' => time(), 'copies' => $copies };
     
    foreach my $bench (@$tests) {
        # Get the parameters for this benchmark.
        $isFail = 0;
        my $tparams = $TestCases::testParams->{$bench};
        if (!defined($tparams)) {     
            abortRun("unknow benchmark \"$bench\"");
        }

        # Make up the actual benchmark parameters.
        my $params = mergeParams($TestCases::baseParams, $tparams);
        if (defined $casesOpts->{$bench}{'params'}) {
            $params->{'options'} = $casesOpts->{$bench}{'params'};
        }
       
        # If the benchmark doesn't want to run with this many copies, skip it.
        my $cat = $tparams->{'cat'};
        my $maxCopiesCat = $TestCases::testCats->{$cat}{'maxCopies'};
        my $maxCopies = $params->{'maxCopies'};
        if (($maxCopies > 0 && $copies > $maxCopies) ||
            ($maxCopiesCat > 0 && $copies > $maxCopiesCat)) {
            setSingleCoreBench($bench);
            next;
        }

        # Add 'key' member for params, just for convenience to pass $bench to subfunction.
        $params->{'key'} = $bench;

        my $maxThreads = $params->{'maxThreads'};
        next if ($maxThreads > 0 && $ENV{'OMP_NUM_THREADS'} > $maxThreads);
        # Run the benchmark.
        my $bresult = runBenchmark($bench, $tparams, $params, $verbose, $logFile, $copies, $reportFds);
        if (!$isFail){
            $results->{$bench} = $bresult;
        }
    }
     
    $results->{'end'} = time();
    $results->{'totalCases'} = $totalCasenum - @singleCoreBenchList;
    $results->{'failCases'} = @failBenchList;
    $results->{'validCases'} = $totalCasenum - @singleCoreBenchList - @failBenchList;
    $results->{'reportStatus'} = (!$results->{'failCases'})? "SUCCESS" : "INVALID";
    $results->{'failList'} = join(" ", @failBenchList);
    $results->{'baseFile'} = $INDEXBASE;

    # Generate a sorted list of benchmarks for which we have results.
    my @benches = grep {
        ref($results->{$_}) eq "HASH" && defined($results->{$_}{'msg'})
    } keys(%$results);
    @benches = sort {
        $results->{$a}{'bname'} cmp $results->{$b}{'bname'}
    } @benches;
    $results->{'list'} = \@benches;

    # Generate index scores for the results relative to the baseline data. 
    indexResults($results);
    summarizeRun($systemInfo, $results, $verbose, $reportFds->{'txt'});
    summarizeRunHtml($systemInfo, $results, $verbose, $reportFds->{'html'});
    summarizeRunPdf($systemInfo, $results, $verbose, $reportFds->{'pdf'});
    summarizeRunCsv($systemInfo, $results, $verbose, $reportFds->{'csv'});
    summarizeRunJson($systemInfo, $results, $verbose, $inparams, $reportFds->{'json'});

    printLog($logFile, "================================================================\n");
    my $staStr = sprintf ("REPORT STATUS: %s\n    Copies: %d\n    TotalCases: %d\n    SuccessCases: %d\n    FailList: %d\n    BaseFile: %s\n",
        $results->{'reportStatus'}, $results->{'copies'}, $results->{'totalCases'}, $results->{'validCases'}, $results->{'failCases'}, $INDEXBASE);
    my $failstr = "";
    if ($results->{'failCases'}) {
        $failstr = sprintf("    FailList:     %s\n\n",, $results->{'failList'});
    }
    printLog ($logFile, "$staStr$failstr");
}

sub setFailBench{
    #my $errcase = pop(@_);
    my ($failBench) = @_;
    push(@failBenchList,$failBench);
    return @failBenchList;
}

# Set cases that only support single-core operation
sub setSingleCoreBench{
    my ($singleCoreBench) = @_;
    push(@singleCoreBenchList, $singleCoreBench);
    return @singleCoreBenchList;
}

# Print cases that only support single-core operation
sub printSingleCoreBench{
    my ($logFile) = @_;
    my $num=@singleCoreBenchList;
    print "\n======== The following $num cases only support single-core operation: ========\n";
    foreach my $n (@singleCoreBenchList){
        print "$n ";
    }
    print "\n\n\n";

    # Output the cases that only support single-core operation to .log file
    $" = "\n";
    open(my $fd, ">>", $logFile) || die("Run: can't write to $logFile\n");
    printf $fd "\n======== The following $num cases only support single-core operation: ========\n";
    #printf $fd "@singleCoreBenchList";
    foreach my $n (@singleCoreBenchList){
        printf $fd "$n ";
    }
    printf $fd "\n\n\n";
    close($fd);
}

############################################################################
# MAIN
############################################################################

sub main {
    my @args = @_;

    getIndexList();
    getGindexList();
    my $onlybuild = 0;
    my $params = parseArgs(@args);
    $onlybuild = $params->{'onlybuild'};
    my $cmdargs = $params->{'cmdargs'};
    my $strcmdargs = join(' ', @$cmdargs);
    my $inparams;
    $inparams->{'cmdargs'} = $strcmdargs;
    
    my $verbose = $params->{'verbose'} || 1;
    if ($params->{'iterations'}) {
        $inparams->{'iterations'} = $params->{'iterations'};
        $longIterCount = $params->{'iterations'};
        $shortIterCount = int(($params->{'iterations'} + 1) / 3);
        $shortIterCount = 1 if ($shortIterCount < 1);
    }

    if ($params->{'mark'}) {
        $isSkipFail = 1;
    }


    # If no benchmark units have be specified, do "index".
    my $tests = $params->{'tests'};
    if ($#$tests < 0) {
         #$tests = $TestDefines::gindex;
         helpinfo();
         die("Run: unknown test, please input -f parameters\n");
    }

    my $systemInfo = getSystemInfo();
    my $fileName = logFile($systemInfo);
    my $reportDir = "$RESULTDIR/$fileName";
    my $reportFile = "$reportDir/$fileName";

    # Create directories.
    my @creatingDirectories = ( ${TMPDIR}, "$reportDir" );
    createDirrectoriesIfNotExists(@creatingDirectories);

    system("cp -f $ENVINFO  ${reportDir}/");
    system("cp -f $HARDINFO ${reportDir}/");
    system("cp -f $SOFTINFO ${reportDir}/");

    my $makeLogFile = $reportFile . ".mlog";
    preChecks($tests, $systemInfo, $makeLogFile, $onlybuild);

    # Display the program banner.
    print "------------------------------------------------------------------------------\n\n";
    system("cat \"${DATADIR}/benchmark.logo\"");
    print "\n   Current version: $brandInfo->{'version'}\n\n";
    print "------------------------------------------------------------------------------\n\n";

    # Show caselist, if not in quiet mode.
    $totalCasenum = $#$tests + 1;
    if ($verbose > 0) {
        printf "Caselist to run : <%s>, casenum=%d\n\n", join(", ", @$tests), $totalCasenum;
        printf "Output directory: %s\n\n", $reportDir;
    }

    # Generate unique file names for the report and log file.
    my $reportTxt = $reportFile . ".txt";
    my $reportHtml = $reportFile . ".html";
    my $reportPdf = $reportFile . ".pdf";
    my $reportJson = $reportFile . ".json";
    my $reportCsv = $reportFile . ".csv";
    my $logFile = $reportFile . ".log";
    printLog($logFile, "basedir=<$MAINDIR>, paras=<@args>");
    printLog($logFile, "caselist=<@$tests>, casenum=$totalCasenum");

    # If defined "UB_OUTPUT_CSV" on Environment, output csv file.
    my $ubOutputCsv = $ENV{"UB_OUTPUT_CSV"};
    # If write CSV, header needs only once.
    my $is_csv_header_written = 0;

    my $reportFds = { };
    # Open the log file for writing.
    open($reportFds->{'txt'}, ">", $reportTxt) ||
                            die("Run: can't write to $reportTxt\n");
    open($reportFds->{'html'}, ">", $reportHtml) ||
                            die("Run: can't write to $reportHtml\n");
    open($reportFds->{'csv'}, ">", $reportCsv) ||
                            die("Run: can't write to $reportCsv\n");
    $reportFds->{'pdf'} = openPdf($systemInfo, $reportPdf, $brandInfo,
                               "${DATADIR}/logo.png", "${DATADIR}/chinese.ttf");
    open($reportFds->{'json'}, ">", $reportJson) ||
                            die("Run: can't write to $reportJson\n");                             

    runHeader($systemInfo, $reportFds->{'txt'}, $brandInfo);
    runHeaderHtml($systemInfo, $reportFds->{'html'}, $brandInfo);
    runHeaderJson($systemInfo, $reportFds->{'json'}, $brandInfo);

    # Dump information about the system under test.
    displaySystem($systemInfo, $reportFds->{'txt'});
    displaySystemHtml($systemInfo, $reportFds->{'html'});
    # it is called internally
    #displaySystemPdf($systemInfo, $reportFds->{'pdf'});
    displaySystemJson($systemInfo, $reportFds->{'json'});

    if($params->{'testfile'}){
        $inparams->{'testfile'}  = $params->{'testfile'};
    }

    if($params->{'basefile'}){
        $inparams->{'basefile'}  = $params->{'basefile'};
    }

    my $copies = $params->{'copies'};
    if ($copies && (scalar(@$copies) != 0)) {
        $inparams->{'copies'} = scalar(@$copies);
        if ($ENV{'OMP_NUM_THREADS'} != 1) {
            runTestsWithReport($tests, $verbose, $logFile, 1, $systemInfo, $reportFds, $inparams);
            $ENV{'OMP_NUM_THREADS'} = 1;
        }
        foreach my $c (@$copies) {
            runTestsWithReport($tests, $verbose, $logFile, $c, $systemInfo, $reportFds, $inparams);
        }
    } else {
        runTestsWithReport($tests, $verbose, $logFile, 1, $systemInfo, $reportFds, $inparams);
    }

    runFooterHtml($reportFds->{'html'});
    closePdf($reportFds->{'pdf'});

    # Finish the report.
    close($reportFds->{'txt'});
    close($reportFds->{'html'});
    close($reportFds->{'csv'});
    close($reportFds->{'json'});

    # Display the report, if not in quiet mode.
    if ($verbose > 0) {
        printf "\n";
        printf  "========================================================================\n";
        system("cat \"$reportTxt\"");
    }
    if (@singleCoreBenchList) {
        printSingleCoreBench($logFile);
    }

    if($params->{'sendfile'})
    {
        sendResultFile($reportJson);
        sendResultFile($reportPdf);
        sendResultFile($reportTxt);
        sendResultFile($reportCsv);
        sendResultFile($logFile);
        sendResultFile($makeLogFile);
    }

    0;
}


exit(main(@ARGV));

