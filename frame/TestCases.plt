#!/usr/bin/perl -w

package TestCases;

use strict;

use POSIX qw(strftime);
use Time::HiRes;
use IO::Handle;
use File::Path;
use FindBin;

# Directory where the test programs live.
my $BINDIR = ::getDir("UB_BINDIR", $FindBin::Bin . "/pgms");

# Temp directory, for temp files.
my $TMPDIR = ::getDir("UB_TMPDIR", $FindBin::Bin . "/frame/tmp");

# Directory to put results in.
my $RESULTDIR = ::getDir("UB_RESULTDIR", $FindBin::Bin . "/results");

# C compiler to use in compilation tests.
my $cCompiler = "gcc";

my $WKLDPATH = ::getDir("UB_WKLDPATH", $FindBin::Bin . "/../workloads");

#######################################################################
# Configure the categories to which tests can belong.
#######################################################################

our $testCats = {
    "CPU"       => { "name" => "CPU and cache",    "maxCopies" => 0, "ratio" => 0, "casesRatioSum" => 0 },
    "MEMORY"    => { "name" => "Memory",           "maxCopies" => 0, "ratio" => 0, "casesRatioSum" => 0 },
    "SYSTEM"    => { "name" => "Operating System", "maxCopies" => 0, "ratio" => 0, "casesRatioSum" => 0 },
    "FILEIO"    => { "name" => "File system and storage", "maxCopies" => 0, "ratio" => 0, "casesRatioSum" => 0 },
    "NETIO"     => { "name" => "Network",                 "maxCopies" => 0, "ratio" => 0, "casesRatioSum" => 0 },
    "GRAPHIO"   => { "name" => "Graphic 2D and 3D",       "maxCopies" => 1, "ratio" => 0, "casesRatioSum" => 0 },
    "COMPLEX"   => { "name" => "Typical applications",    "maxCopies" => 0, "ratio" => 0, "casesRatioSum" => 0 },
    "RUNTIME"   => { "name" => "C/C++/Java/Python",       "maxCopies" => 0, "ratio" => 0, "casesRatioSum" => 0 },
    "LAB"       => { "name" => "Experiment",              "maxCopies" => 0, "ratio" => 0, "casesRatioSum" => 0 },
};

#######################################################################
# Default parameters which will be merged with the benchmark parameters
#######################################################################

# Default parameters for benchmarks.  Note that if "prog" is used,
# it must contain just the program name, as it will be quoted (this
# is necessary if BINDIR contains spaces).  Put any options in "options".
our $baseParams = {
    "prog" => undef,
    "options" => "",
    "repeat" => "short",
    "stdout" => 1,                  # Non-0 to keep stdout.
    "stdin" => "",
    "logmsg" => "",
    "buildep" => "",
    "maxCopies" => 0,               # 0 means unlimited
    "maxThreads" => 1,              # 0 means unlimited
    "checkself" => 0,               # if none-zero, the test program will check the result correction by itself.
    "workloaddir" => undef,         # tell the framework where is the workload dir for this case.
    "prepare" => "",                # prepare for bench before run the bench.
};

#######################################################################
# Individual parameters for all benchmarks.
#######################################################################

our $testParams = {
