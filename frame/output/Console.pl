#!/usr/bin/perl

use Term::ANSIColor qw(:constants);

use strict;

my $avoid_failure = {};

############################################################################
# CONSOLE REPORTS
############################################################################

sub printTestCases {

    my $cases = $TestCases::testParams;

    printf "\n\n";
    printf "%-25s %-40s %s\n", "TestCases", "Descriptions", "Original parameters";
    printf "------------------------------------------------------------------------------------------------------------------\n";

    for my $c (sort(keys(%$cases))) {
        printf "%-25s %-40s %s\n", $c, $cases->{$c}{'logmsg'}, $cases->{$c}{'options'};
    }
    printf "\n\n";
}

sub printTestCasesAttr {

    my $cases = $TestCases::testParams;

    printf "\n\n";
    printf "%-25s   %15s %15s %15s %15s %6s\n", "TestCases", "Max copies", "Max threads", "Self-check", "Repeat";
    printf "-------------------------------------------------------------------------------------------\n";

    for my $c (sort(keys(%$cases))) {
        my $case = mergeParams($TestCases::baseParams, $cases->{$c});
        my $tmpcat = $case->{'cat'};
        my $catMaxCopies = $TestCases::testCats->{$tmpcat}->{'maxCopies'};

        printf "%-25s ", $c;
        if ($case->{'maxCopies'} != 0) {
            printf "\e[1;31m%15d \e[0m", $case->{'maxCopies'};
        } elsif ($catMaxCopies != 0) {
            printf "\e[1;31m%15d \e[0m", $catMaxCopies;
        } else {
            printf "%15d ", $case->{'maxCopies'};
        }

        if ($case->{'maxThreads'} != 0) {
            printf "\e[1;31m%15d \e[0m", $case->{'maxThreads'};
        } else {
            printf "%15d ", $case->{'maxThreads'};
        }

        if ($case->{'checkself'} == 1) {
            printf "\e[1;31m%15d \e[0m", $case->{'checkself'};
        } elsif ($case->{'checkself'} == 10) {
            printf "\e[1;33m%15d \e[0m", $case->{'checkself'};
        } else {
            printf "%15d ", $case->{'checkself'};
        }
        printf "  %15s", $case->{'repeat'};
        printf "\n";
    }
    printf "\n\n";
}
