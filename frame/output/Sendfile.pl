#!/usr/bin/perl

use strict;
use IO::Socket;
use Getopt::Long;
use IO::File;

my $PLCONFDIR = Cwd::realpath(getDir('UB_CONFDIR', $FindBin::Bin . "/../../config"));


sub sendResultFile{
	my ( $filejson ) = @_;
	#print "\n*****************$filejson******************\n";

	my($result, $udp_client_socket, $DESTIP, $PORTNO, $PROTO, $MAXSZ, $killserver, $outgoing, $ServerRunning, $ack, $ack_str, $checksum);


	if(!($DESTIP))
	{
	        my $DESTIPADDR=`cat "${PLCONFDIR}/log_server_ip"`;
		$DESTIP = $DESTIPADDR;
		#print "Target IP argument missing. Assuming $DESTIP...\n";
	}

	if(!($PORTNO))
	{
		$PORTNO = 5152;
		#print "Target Port argument missing. Assuming $PORTNO...\n";

	}

	if(!($MAXSZ))
	{
		$MAXSZ = 1024;
		#print "Message size argument missing. Assuming $MAXSZ bytes...\n";
	}

	if(($killserver ne "YES") and ($killserver ne "NO"))
	{
		$killserver = 1;
		#print "Server shutdown argument missing or invalid. Assuming YES...\n";
	}

	$PROTO = 'udp';

	my $fileName = $filejson;

	if($fileName eq ""){
		die("please chose a file!\n");
	}

    my @filenamesz = split('/', $filejson);
    my $lentmp = @filenamesz;
    my $filenamesub = @filenamesz[$lentmp - 1];
    $filenamesub = $filenamesub."_._._";

	my $fileLength;
	my $readSize = 1048576;

	my $fh = new IO::File;
	$fh->open("<$fileName");
	$fileLength = (stat($fh))[7] + length($filenamesub);
	my $extLength;
	if($readSize >= $fileLength){
		$readSize = $fileLength;
		$extLength = 1;
		}else{
		$extLength = int($fileLength/$readSize);
		if(($extLength * $readSize) < $fileLength){
			$extLength = $extLength + 1;
		}
	}

	my $filenameonly = @filenamesz[$lentmp - 1];
	my $fileContentLen = (stat($fh))[7];
	print "Starting $PROTO client on port $PORTNO: $filenameonly, size=$fileContentLen\n";

	$udp_client_socket = new IO::Socket::INET(PeerAddr => $DESTIP, PeerPort => $PORTNO, Proto => $PROTO)
	or die "Couldn't create a $PROTO client on port $PORTNO : $@\n";

	#print "Ready to sent $PROTO messages to $DESTIP port $PORTNO\n";

	$ServerRunning = 0;
	my $timeout = 0;
	while(!$ServerRunning)
	{
		$outgoing = "hello";
		$udp_client_socket->send($outgoing);
		
		if($udp_client_socket->recv($ack, $MAXSZ))
		{
			my($ack_str, $checksum) = split(/_/, $ack);
			
			if($ack_str eq "ACK")
			{
				print ("Server is alive!\n");
				$ServerRunning = 1;
			}
			else
			{
				die ("Server handshake did not complete successfully. Exiting...\n");
			}	
		}
		else
		{
            $timeout++;
            if($timeout == 1){
                print ("Waiting for server...\n");
            }
            
            if($timeout >= 5){
                last;
            }
			sleep(2);
		}	
	}

	#send filecontent
	my $fileContent;
	my $line;
	my $i = 1;
	my $isLast = 0;
	my $complated = 0;
    my $wordtmp;
	while(($i * $readSize) <= $fileLength || $isLast == 1){
		if(read($fh, $line, $readSize)){
            if($complated == 0){
                    $complated = $complated + length($line) + length($filenamesub);
                    $wordtmp = $filenamesub.$line;
            }
            else{
                $complated = $complated + length($line);
                $wordtmp = $line;
            }
			
			$udp_client_socket->send($wordtmp);
			#print "sending file,the size of this piece of cake is:".length($line).",complated:$complated bytes,total:$fileLength bytes,complated:".  ($complated / $fileLength * 100)."%\n";
		}
			$i = $i + 1;
			if($i * $readSize > $fileLength && ($i - 1) * $readSize <= $fileLength){
			$isLast = 1;
		}else{
			$isLast = 0;
		}
	}
	#send filecontent end
	if($timeout > 1){
		print "transmitted failed!\n";
	}
	else{
		print "transmitted successed!\n";
	}
	
	if($killserver eq "YES")
	{
		$outgoing = "killmenow";

		$udp_client_socket->send($outgoing);
		$udp_client_socket->recv($ack, $MAXSZ);

		sleep(2);
	}

	$fh->close;
	$udp_client_socket->close();
}
