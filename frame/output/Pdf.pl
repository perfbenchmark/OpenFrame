#!/usr/bin/perl

use strict;
use PDF::API2;
use PDF::API2::Page;
use PDF::API2::Content;
use PDF::API2::Annotation;
use PDF::API2::NamedDestination;

my $avoid_failure = {};

my $TAB_WIDTH = 16;
my $BORDER_BEGIN = 14;
my $BORDER_END = 597;
my $BORDER_END_CHARS = 101;
my $COL_BEGIN = 21;
my $COL_WIDTH = 100;
my $COL_TAB1 = $COL_BEGIN + $TAB_WIDTH;
my $COL_TAB2 = $COL_TAB1 + $TAB_WIDTH;
my $COL_COL1 = $COL_BEGIN + $COL_WIDTH * 13 / 8;
my $LINE_BEGIN = 760;
my $LINE_END = 30;
my $HEIGHT_FONTB = 30;
my $HEIGHT_FONT = 25;
my $PAGE_WIDTH = 580;

my $lineCur = $LINE_BEGIN;

my $DRAW_CENTER = -1;

my $TEST_STATUS = "full";

############################################################################
# PDF API2 Wrapper
############################################################################

sub pdfDrawHLineExt {
    my ( $pdf, $y, $count, $col ) = @_;
    my $page = $pdf->{'cpage'};
    my $text = $pdf->{'text'};
    my $str = "_";

    for (my $i = 1; $i < $count; $i++) {
        $str .= "_";
    }

    $text->font($pdf->{'font'}, 10);
    $text->translate($col, $y);
    $text->text($str);
}

sub pdfDrawHLine {
    my ( $pdf, $y, $count ) = @_;
    pdfDrawHLineExt($pdf, $y, $count, $COL_BEGIN);
}

sub pdfDrawHLineFull {
    my ( $pdf, $y ) = @_;
    my $page = $pdf->{'cpage'};
    my $text = $pdf->{'text'};

    $text->font($pdf->{'font'}, 10);
    $text->translate($BORDER_BEGIN - 1, $y);
    $text->text("_________________________________________________________________________________________________________");
}

sub pdfDrawHLineB {
    my ( $pdf, $y ) = @_;
    my $page = $pdf->{'cpage'};
    my $textt = $pdf->{'text'};

    $textt->font($pdf->{'fontb'}, 12);
    $textt->translate($BORDER_BEGIN, $y);
    $textt->text("_______________________________________________________________________________________");
}

sub pdfDrawVLineB {
    my ( $pdf, $x ) = @_;
    my $textt = $pdf->{'text'};
    my $y;

    $textt->font($pdf->{'fontb'}, 12);

    $x -= 3;
    for ($y = $LINE_END; $y < $LINE_BEGIN; $y += 10) {
        $textt->translate($x, $y);
        $textt->text("|");
    }
}

sub pdfDrawVLine {
    my ( $pdf, $x, $yb, $ye ) = @_;
    my $text = $pdf->{'text'};
    my $y;

    $text->font($pdf->{'font'}, 10);

    $x -= 3;
    #for ($y = $ye; $y < $b; $y += 10) {
    for ($y = $ye - 5; $y < $yb - 5; $y += 1) {
        $text->translate($x, $y);
        $text->text("|");
    }
}

sub normalFontSize {
    my ( $pdf ) = @_;
    my $text = $pdf->{'text'};
    my $fsize;

    $text->font($pdf->{'font'}, 10);
    $fsize = $text->advancewidth("x");

    $fsize;
}

sub pdfDrawText {
    my ( $ways, $x, $y, $str, $pdf ) = @_;
    my $textc = $pdf->{'text'};

    if ($ways eq "text") {
        $textc->font($pdf->{'font'}, 10);
    } elsif ($ways eq "textt") {
        $textc->font($pdf->{'fontb'}, 12);
    } elsif ($ways eq "texttb") {
        $textc->font($pdf->{'fontb'}, 14);
    } elsif ($ways eq "texttc") {
        $textc->font($pdf->{'fontc'}, 20);
    } elsif ($ways eq "textl") {
        $textc->font($pdf->{'fontb'}, 11);
    } elsif ($ways eq "textbb") {
        $textc->font($pdf->{'fontb'}, 10);
    } elsif ($ways eq "textsc") {
        $textc->font($pdf->{'fontc'}, 10);
    }

    if ($x == $DRAW_CENTER) {
        $x = ($PAGE_WIDTH - $textc->advancewidth($str)) / 2,
    }

    $textc->translate($x, $y);
    $textc->text($str);
}

sub pdfDrawTextWrapAlign {
    my ( $ways, $x, $ex, $str, $start, $gap, $pdf ) = @_;
    my $textc = $pdf->{'text'};

    if ($ways eq "text") {
        $textc->font($pdf->{'font'}, 10);
    } elsif ($ways eq "textt") {
        $textc->font($pdf->{'fontb'}, 12);
    } elsif ($ways eq "textbb") {
        $textc->font($pdf->{'fontb'}, 10);
    }

    for (my $i = $start; $i < length($str); $i++) {
        my $tmp = substr($str, 0, $i);
        if ($textc->advancewidth($tmp) > ($ex - $x)) {
            my $left = substr($str, $i);
            $i--;
            $lineCur += $gap / 3;
            pdfDrawText($ways, $x, $lineCur, $tmp, $pdf);
            $lineCur -= $gap * 7 / 12;
            pdfDrawText($ways, $x, $lineCur, $left, $pdf);
            $lineCur += $gap / 4;
            return;
        }
    }
    pdfDrawText($ways, $x, $lineCur, $str, $pdf);
}

sub pdfDrawTextWrapAlignCommon {
    my ( $ways, $x, $ex, $str, $gap, $pdf ) = @_;
    my $textc = $pdf->{'text'};
    my $continue = 0;
    my $maxw;
    my $tmp;
    my $i;

    if ($ways eq "text") {
        $textc->font($pdf->{'font'}, 10);
    } elsif ($ways eq "textt") {
        $textc->font($pdf->{'fontb'}, 12);
    } elsif ($ways eq "textbb") {
        $textc->font($pdf->{'fontb'}, 10);
    }
    $maxw= $ex - $x - $textc->advancewidth('X');

    for ($i = 0; $i <= length($str); $i++) {
        $tmp = substr($str, $continue, $i - $continue);
        if ($textc->advancewidth($tmp) >= $maxw) {
            pdfDrawText($ways, $x, $lineCur, $tmp, $pdf);
            $pdf = stepWithPageAdjust($pdf, $gap);
            $continue = $i;
        }
    }

    # Draw the left string, and skip the last $i++
    if (($i - $continue) > 1) {
        pdfDrawText($ways, $x, $lineCur, $tmp, $pdf);
    }

    $pdf;
}

sub displayImage {
    my ( $page, $img, $x, $y, $width, $height ) = @_;
    $page->gfx->image($img, $x, $y, $width, $height);
}

sub newPdfPage {
    my ( $pdf) = @_;
    my $fpdf = $pdf->{'pdf'};
    my $bench_name = $pdf->{'bench_name'};

    my $page = $fpdf->page();
    my $text = $page->text();

    $page->mediabox('Letter');

    # Set page background
    $text->font($pdf->{'fontb'}, 250);
    $text->fillcolor('#F0F0F0');
    $text->transform(-translate=>[135, 50], -rotate=>50);
    $text->text($bench_name);
    $text->fillcolor('#000000');

    $pdf->{'idx'}++;
    $pdf->{'cpage'} = $page;
    $pdf->{'text'} = $text;

    pdfDrawHLineB($pdf, $LINE_BEGIN);
    pdfDrawHLineB($pdf, $LINE_END);
    pdfDrawVLineB($pdf, $BORDER_BEGIN);
    pdfDrawVLineB($pdf, $BORDER_END);
    $lineCur = $LINE_BEGIN;

    displayImage($page, $pdf->{'imglogo'}, 230, 0, 135, 27);
}

sub addEndInfo {
    my ( $pdf, $results ) = @_;
    my $fpdf = $pdf->{'pdf'};
    my $idx = $pdf->{'idx'};

    for (my $i = 1; $i <= $idx; $i++) {
        my $page = $fpdf->openpage($i);
        my $text = $page->text();
        my $ttime = sprintf "%s",
                     strftime("%Y-%m-%d %H:%M:%S", localtime($results->{'start'}));

        # Set page header text
        $text->font($pdf->{'font'}, 10);
        $text->translate($COL_BEGIN, 770);
        $text->text("$pdf->{'bench_desc'} $pdf->{'version'} report, $ttime");
        # Set page footer text
        $text->font($pdf->{'font'}, 12);
        $text->translate(520, 10);
        $text->text("page $i of $idx");
    }
}

sub stepWithPageAdjust {
    my ( $pdf, $height ) = @_;

    $lineCur -= $height;
    if ($lineCur < $LINE_END + $height) {
        newPdfPage($pdf);
        $lineCur -= $height;
    }
    $pdf;
}

############################################################################
# PDF REPORTS
############################################################################

sub displayTitle {
    my ( $pdf, $info ) = @_;
    my $title0 = sprintf "%s %s", $pdf->{'bench_name'}, $pdf->{'version'};
    my $title1 = sprintf "Report Date: %s", strftime("%Y-%m-%d", localtime());
    my $title2 = sprintf "%s", $info->{'minfo'}{'brandmodel'};
    my $page = $pdf->{'cpage'};
    my $text = $page->text();
    my $col0 = 360;
    my $off = 2;

    pdfDrawVLine($pdf, $col0, $lineCur - $off, $lineCur - $HEIGHT_FONTB * 3);
    $lineCur -= $HEIGHT_FONTB * 3 / 2;
    $text->font($pdf->{'fontb'}, 14);
    pdfDrawText("texttb", ($PAGE_WIDTH + $col0 - $text->advancewidth($title0)) / 2,
                $lineCur, $title0, $pdf);
    $lineCur -= $HEIGHT_FONTB * 1 / 3;
    pdfDrawText("texttc", ($col0 - $text->advancewidth($title2)) / 2,
                $lineCur, $title2, $pdf);
    $lineCur -= $HEIGHT_FONTB * 1 / 3;
    $text->font($pdf->{'fontb'}, 11);
    pdfDrawText("textl", ($PAGE_WIDTH + $col0 - $text->advancewidth($title1)) / 2,
                $lineCur, $title1, $pdf);
    $lineCur -= $HEIGHT_FONTB;
    pdfDrawHLineFull($pdf, $lineCur);
    $lineCur -= $HEIGHT_FONTB * 2 / 3;
}

sub addResultAtFront {
    my ( $results, $pdf ) = @_;
    my $fpdf = $pdf->{'pdf'};
    my $page = $fpdf->openpage(1);
    my $text = $page->text();
    my $col = 165;
    my $title = sprintf "Composite Score ( %s ):",
                        number($results->{'copies'}, "copy", "copies");
    my $str = sprintf "%8.4f", $results->{'gmean'};
    if ($results->{'reportStatus'} ne "SUCCESS") {
	    $str = $str . " (INVALID)";
    }
    $pdf->{'text'} = $text;
    $text->font($pdf->{'fontb'}, 14);
    $lineCur = $LINE_BEGIN - $HEIGHT_FONTB * 4;
    $text->translate($col, $lineCur);
    $text->text($title);
    pdfDrawText("texttb", $col + $text->advancewidth($title) + 10,
                          $lineCur, $str, $pdf);
}

# Dump the given run results into the given report file.
sub openPdf {
    my ( $info, $filename, $brandInfo, $logofilename, $fontname ) = @_;
    my $pdf = {};

    my $fpdf = PDF::API2->new();
    my $imglogo = $fpdf->image_png($logofilename);
    my $font = $fpdf->corefont('Helvetica');
    my $fontb = $fpdf->corefont('Helvetica-Bold');
    my $fontc = $fpdf->ttfont("$fontname", -encode => 'utf-8');
    my $bench_name = $brandInfo->{'bench_name'};
    my $version = $brandInfo->{'version'};

    $pdf->{'pdf'} = $fpdf;
    $pdf->{'imglogo'} = $imglogo;
    $pdf->{'font'} = $font;
    $pdf->{'fontb'} = $fontb;
    $pdf->{'fontc'} = $fontc;
    $pdf->{'fname'} = $filename;
    $pdf->{'bench_name'} = $bench_name;
    $pdf->{'bench_desc'} = $brandInfo->{'bench_desc'};
    $pdf->{'version'} = $version;
    $pdf->{'idx'} = 0;

    newPdfPage($pdf);

    displayTitle($pdf, $info, $version, $bench_name);

    $pdf;
}


sub displaySoftwarePdf {
    my ( $info, $pdf ) = @_;
    my $str;
    my $lineGap = $HEIGHT_FONT * 2 / 3;

    #if ($lineCur < $LINE_END + 3 * $HEIGHT_FONTB) {
    #    newPdfPage($pdf);
    #} else {
    #    $lineCur -= $HEIGHT_FONTB * 2 / 3;
    #    pdfDrawHLineFull($pdf, $lineCur);
    #}
    #$lineCur -= $HEIGHT_FONTB * 2 / 3;
    #pdfDrawText("textt", $DRAW_CENTER, $lineCur, "Software", $pdf);
    #$pdf = stepWithPageAdjust($pdf, $HEIGHT_FONTB);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "OS Release:", $pdf);
    $str = sprintf "%s", $info->{'minfo'}{'osrelease'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "OS Kernel:", $pdf);
    $str = sprintf "%s -- %s -- %s",
               $info->{'os'}, $info->{'osRel'}, $info->{'osVer'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    $pdf;
}

sub displayHardwarePdf {
    my ( $info, $pdf ) = @_;
    my $str;
    my $lineGap = $HEIGHT_FONT * 2 / 3;
    my $cpus = $info->{'cpus'};
    my $minfo = $info->{'minfo'};

    if ($lineCur < $LINE_END + 3 * $HEIGHT_FONTB) {
        newPdfPage($pdf);
    } else {
        $lineCur -= $HEIGHT_FONTB * 2 / 3;
        pdfDrawHLineFull($pdf, $lineCur);
    }
    #$lineCur -= $HEIGHT_FONTB * 2 / 3;
    #pdfDrawText("textt", $DRAW_CENTER, $lineCur, "Hardware", $pdf);
    $lineCur -= $HEIGHT_FONTB;

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "CPU Information:", $pdf);
    if (!defined($cpus)) {
        pdfDrawText("text", $COL_COL1, $lineCur, "no deatils available", $pdf);
    } else {
        if (!defined($cpus->[0]{'flags'})) {
            $cpus->[0]{'flags'} = "";
        }
        $str = sprintf "%s (%.4f bogomips)",
                    $cpus->[0]{'model'}, $cpus->[0]{'bogo'};
        pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
        $pdf = stepWithPageAdjust($pdf, $lineGap);
        $str = sprintf "%s", $cpus->[0]{'flags'};
        pdfDrawTextWrapAlign("text", $COL_COL1, $PAGE_WIDTH, $str, 50, $lineGap, $pdf);
    }
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "CPU Sockets/Cores/Threads:", $pdf);
    $str = $minfo->{'cpusocks'} . " / " . $minfo->{'cpucores'} . " / " . $minfo->{'cputhrs'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Memory Manufacturer:", $pdf);
    pdfDrawTextWrapAlign("text", $COL_COL1, $PAGE_WIDTH, $minfo->{'memmanu'}, 50, $lineGap, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Memory Information:", $pdf);
    $str = sprintf "%s MB, %s", $info->{'memTotal'}, $minfo->{'memtype'};
    pdfDrawTextWrapAlign("text", $COL_COL1, $PAGE_WIDTH, $str, 50, $lineGap, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    $pdf;
}

sub fontAutoCheckUTF8Chinese {
    my ( $str ) = @_;
    my $i;

    for ($i = 0; $i < length($str); $i++) {
        if (ord(substr($str, $i, 1)) >= 0x80) {
            return "textsc";
        }
    }
    "text";
}

sub displaySystemPdfFull {
    my ( $info, $pdf ) = @_;
    my $str;
    my $lineGap = $HEIGHT_FONT * 2 / 3;

    my $cpus = $info->{'cpus'};
    my $minfo = $info->{'minfo'};
    my @items0;
    my @items1;
    my @items2;
    my @items3;
    my @items4;
    my @items5;
    my $count;
    my $i;


    if ($lineCur < $LINE_END + 3 * $HEIGHT_FONTB) {
        newPdfPage($pdf);
    } else {
        $lineCur -= $HEIGHT_FONTB * 2 / 3;
        pdfDrawHLineFull($pdf, $lineCur);
    }
    $lineCur -= $HEIGHT_FONTB * 2 / 3;
    pdfDrawText("textt", $COL_BEGIN, $lineCur, "System Full Information", $pdf);
    $lineCur -= $HEIGHT_FONTB;

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Brand:", $pdf);
    $str = sprintf "%s", $minfo->{'brandmodel'};
    pdfDrawText(fontAutoCheckUTF8Chinese($str), $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Bios:", $pdf);
    $str = sprintf "%s", $minfo->{'bios'};
    pdfDrawText(fontAutoCheckUTF8Chinese($str), $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Arch:", $pdf);
    $str = sprintf "%s %s", $info->{'mach'}, $info->{'platform'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    $str = sprintf "CPUs(%d):", $#$cpus + 1;
    pdfDrawText("textbb", $COL_TAB1, $lineCur, $str, $pdf);
    if (!defined($cpus)) {
        pdfDrawText("text", $COL_COL1, $lineCur, "no deatils available", $pdf);
    } else {
        if (!defined($cpus->[$i]{'flags'})) {
            $cpus->[0]{'flags'} = "";
        }
        $str = sprintf "%s (%.4f bogomips)",
                    $cpus->[$i]{'model'}, $cpus->[$i]{'bogo'};
        pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
        $pdf = stepWithPageAdjust($pdf, $lineGap);
        $str = sprintf "%s", $cpus->[$i]{'flags'};
        pdfDrawTextWrapAlign("text", $COL_COL1, $PAGE_WIDTH, $str, 50, $lineGap, $pdf);
    }
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "CPU Sockets/Cores/Threads:", $pdf);
    $str = $minfo->{'cpusocks'} . " / " . $minfo->{'cpucores'} . " / " . $minfo->{'cputhrs'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Caches:", $pdf);
    $pdf = stepWithPageAdjust($pdf, $HEIGHT_FONT / 2);
    @items2 = @{$minfo->{'caches'}};
    $count = @items2;
    for ($i = 0; $i < $count; $i++) {
        pdfDrawText("text", $COL_COL1, $lineCur, @items2[$i], $pdf);
        $pdf = stepWithPageAdjust($pdf, $lineGap);
    }

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Numa Topology:", $pdf);
    $str = sprintf "%s", $minfo->{'numacount'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Total Memory:", $pdf);
    $str = sprintf "%s MB ", $info->{'memTotal'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Memory Count:", $pdf);
    $str = sprintf "%s", $minfo->{'memcount'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Memory Manu:", $pdf);
    $str = sprintf "%s", $minfo->{'memmanu'};
    pdfDrawTextWrapAlign("text", $COL_COL1, $PAGE_WIDTH, $str, 50, $lineGap, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Memory Type:", $pdf);
    $str = sprintf "%s", $minfo->{'memtype'};
    pdfDrawTextWrapAlign("text", $COL_COL1, $PAGE_WIDTH, $str, 50, $lineGap, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Memory Speed:", $pdf);
    $str = sprintf "%s", $minfo->{'memspeed'};
    pdfDrawTextWrapAlign("text", $COL_COL1, $PAGE_WIDTH, $str, 50, $lineGap, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Disks:", $pdf);
    $lineCur -= $HEIGHT_FONT/ 2;
    @items0 = @{$minfo->{'harddisks'}};
    $count = @items0;
    for ($i = 0; $i < $count; $i++) {
        if ($lineCur < $LINE_END + $HEIGHT_FONT) {
            newPdfPage($pdf);
            $lineCur -= $HEIGHT_FONT;
        }
        pdfDrawText("text", $COL_COL1, $lineCur, @items0[$i], $pdf);
        $lineCur -= $lineGap;
    }

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Netcards:", $pdf);
    $lineCur -= $HEIGHT_FONT/ 2;
    @items1 = @{$minfo->{'ethcards'}};
    $count = @items1;
    for ($i = 0; $i < $count; $i++) {
        if ($lineCur < $LINE_END + $HEIGHT_FONT) {
            newPdfPage($pdf);
            $lineCur -= $HEIGHT_FONT;
        }
        pdfDrawText("text", $COL_COL1, $lineCur, @items1[$i], $pdf);
        $lineCur -= $lineGap;
    }

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Display Cards:", $pdf);
    $lineCur -= $HEIGHT_FONT/ 2;
    @items3 = @{$minfo->{'dispcards'}};
    $count = @items3;
    for ($i = 0; $i < $count; $i++) {
        if ($lineCur < $LINE_END + $HEIGHT_FONT) {
            newPdfPage($pdf);
            $lineCur -= $HEIGHT_FONT;
        }
        pdfDrawTextWrapAlign("text", $COL_COL1, $PAGE_WIDTH, @items3[$i],
                                                50, $HEIGHT_FONT * 2 / 3, $pdf);
        $lineCur -= $lineGap;
    }

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "System:", $pdf);
    $str = sprintf "%s: %s", $info->{'name'}, $info->{'system'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "OS Release:", $pdf);
    $str = sprintf "%s", $minfo->{'osrelease'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "OS Kernel:", $pdf);
    $str = sprintf "%s -- %s -- %s",
               $info->{'os'}, $info->{'osRel'}, $info->{'osVer'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Language:", $pdf);
    $str = sprintf "%s", $info->{'language'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Bash:", $pdf);
    $str = sprintf "%s", $minfo->{'bash'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "GCC:", $pdf);
    $str = sprintf "%s", $minfo->{'gcc'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "G++:", $pdf);
    $str = sprintf "%s", $minfo->{'g++'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Binutils:", $pdf);
    $str = sprintf "%s", $minfo->{'binutils'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Python2:", $pdf);
    $str = sprintf "%s", $minfo->{'python2'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Python3:", $pdf);
    $str = sprintf "%s", $minfo->{'python3'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Java:", $pdf);
    $str = sprintf "%s", $minfo->{'java'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "GlibC:", $pdf);
    $str = sprintf "%s", $minfo->{'glibc'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Glib:", $pdf);
    $str = sprintf "%s", $minfo->{'glib'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "StdC++:", $pdf);
    $str = sprintf "%s", $minfo->{'stdc++'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "QT:", $pdf);
    $str = sprintf "%s", $minfo->{'qt'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "File System:", $pdf);
    $str = sprintf "%s", $minfo->{'fs'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Base Opts:", $pdf);
    $lineCur -= $HEIGHT_FONT/ 2;
    @items4 = @{$info->{'baseopts'}};
    $count = @items4;
    for ($i = 0; $i < $count; $i++) {
        if ($lineCur < $LINE_END + $HEIGHT_FONT) {
            newPdfPage($pdf);
            $lineCur -= $HEIGHT_FONT;
        }
        pdfDrawTextWrapAlign("text", $COL_COL1, $PAGE_WIDTH, @items4[$i],
                                                50, $HEIGHT_FONT * 2 / 3, $pdf);
        $lineCur -= $lineGap;
    }
    if ($info->{'ispeak'} != 0) {
        @items5 = @{$info->{'peakopts'}};
        $count = @items5;
        pdfDrawText("textbb", $COL_TAB1, $lineCur, "Peak Opts:", $pdf);
        $lineCur -= $HEIGHT_FONT/ 2;
        for ($i = 0; $i < $count; $i++) {
            if ($lineCur < $LINE_END + $HEIGHT_FONT) {
                newPdfPage($pdf);
                $lineCur -= $HEIGHT_FONT;
            }
            pdfDrawTextWrapAlign("text", $COL_COL1, $PAGE_WIDTH, @items5[$i],
                                                50, $HEIGHT_FONT * 2 / 3, $pdf);
            $lineCur -= $lineGap;
        }
    }

    pdfDrawText("textbb", $COL_TAB1, $lineCur, "Runlevel:", $pdf);
    $str = sprintf "%s", $info->{'runlevel'};
    pdfDrawText("text", $COL_COL1, $lineCur, $str, $pdf);
    $pdf = stepWithPageAdjust($pdf, $lineGap);

    $pdf;
}



sub displaySystemPdf {
    my ( $info, $pdf ) = @_;
    $pdf = displayHardwarePdf($info, $pdf);
    $pdf = displaySoftwarePdf($info, $pdf);
    $pdf;
}

# Display the test scores from the given set of test results.
sub logResultsPdf {
    my ( $results, $pdf ) = @_;
    my $text = $pdf->{'text'};
    my $fsize = normalFontSize($pdf);
    my $col0 = $COL_BEGIN;
    my $col1 = $col0 + 30 * $fsize;
    my $col2 = $col1 + 42 * $fsize;
    my $col3 = $col2 + 17 * $fsize;
    my $col4 = $col3 + 6 * $fsize;

    if ($lineCur < $LINE_END + 3 * $HEIGHT_FONTB) {
        newPdfPage($pdf);
        $text = $pdf->{'text'};
    }
    $lineCur -= $HEIGHT_FONT;

    foreach my $bench (@{$results->{'list'}}) {
        my $bresult = $results->{$bench};
        my $tmp = sprintf "%-25s", $bench;

        $lineCur -= $HEIGHT_FONT;
        if ($lineCur < $LINE_END + $HEIGHT_FONT) {
            newPdfPage($pdf);
            $lineCur -= $HEIGHT_FONT;
            $text = $pdf->{'text'};
        }
        pdfDrawText("textbb", $col0, $lineCur, $tmp, $pdf);
        $tmp = sprintf "%-40s", $bresult->{'msg'};
        pdfDrawText("text", $col1, $lineCur, $tmp, $pdf);
        $tmp = sprintf "%12.4f", $bresult->{'score'};
        pdfDrawText("text", $col2, $lineCur, $tmp, $pdf);
        $tmp = sprintf "%-5s", $bresult->{'scorelabel'};
        pdfDrawText("text", $col3, $lineCur, $tmp, $pdf);
        $tmp = sprintf "(%.1f s, %d samples)",
                       $bresult->{'time'}, $bresult->{'iterations'};
        pdfDrawText("text", $col4, $lineCur, $tmp, $pdf);
    }
    $lineCur -= $HEIGHT_FONT;

    $pdf;
}


# Display the test scores from the given set of test results
# for a given category of tests.
sub logIndexCatPdf {
    my ( $info, $results, $cat, $pdf ) = @_;
    my $text = $pdf->{'text'};

    my $catsRatioSum = $info->{'catsRatioSum'};
    my $total = $results->{'numIndex'}{$cat};
    my $indexed = $results->{'indexed'}{$cat};
    my $iscore = $results->{'index'}{$cat};
    my $fiscore = $results->{'findex'}{$cat};
    my $full = defined($indexed) && $indexed == $total;
    my $head = $cat;
    my $warn = "";
    my $str;

    my $fsize = normalFontSize($pdf);
    my $col0 = $COL_BEGIN;
    my $col1 = $col0 + 28 * $fsize;
    my $col2 = $col1 + 21 * $fsize;
    my $col3 = $col2 + 16 * $fsize;
    my $col4 = $col3 + 18 * $fsize;
    my $col5 = $col4 + 11 * $fsize;
    my $col6 = $col5 + 9 * $fsize;


    # If there are no results in this category, just ignore it.
    if (!defined($results->{'numCat'}{$cat}) ||
                            $results->{'numCat'}{$cat} == 0) {
        return;
    }

    if (!$full) {
        $TEST_STATUS = "partial";
    }

    if ($lineCur < $LINE_END + 3 * $HEIGHT_FONTB) {
        newPdfPage($pdf);
        $lineCur -= $HEIGHT_FONTB;
        $text = $pdf->{'text'};
    } else {
        $lineCur -= $HEIGHT_FONTB;
        $lineCur -= $HEIGHT_FONTB;
    }

    pdfDrawText("textbb", $col0, $lineCur, "TestCases", $pdf);
    pdfDrawText("textbb", $col1, $lineCur, $head, $pdf);
    pdfDrawText("textbb", $col2, $lineCur, "  BASELINE", $pdf);
    pdfDrawText("textbb", $col3, $lineCur, "    RESULT", $pdf);
    pdfDrawText("textbb", $col4, $lineCur, " INDEX", $pdf);
    if ($catsRatioSum > 0) {
        pdfDrawText("textbb", $col5, $lineCur, "WEIGHT", $pdf);
        pdfDrawText("textbb", $col6, $lineCur, "FINDEX", $pdf);
    }
    $lineCur -= $HEIGHT_FONT / 2;
    pdfDrawHLine($pdf, $lineCur, $catsRatioSum > 0 ? $BORDER_END_CHARS : 84);

    foreach my $bench (@{$results->{'list'}}) {
        my $bresult = $results->{$bench};
        next if $bresult->{'cat'} ne $cat;

        if (defined($bresult->{'iscore'}) && defined($bresult->{'index'})) {
            $lineCur -= $HEIGHT_FONT;
            if ($lineCur < $LINE_END + $HEIGHT_FONT) {
                newPdfPage($pdf);
                $lineCur -= $HEIGHT_FONT;
                $text = $pdf->{'text'};
            }

            pdfDrawText("textbb", $col0, $lineCur, $bench, $pdf);
            pdfDrawTextWrapAlign("text", $col1, $col2 - 6,
                                 $bresult->{'msg'}, 16, $HEIGHT_FONT * 2 / 3, $pdf);
            $str = sprintf "%12.4f", $bresult->{'iscore'};
            pdfDrawText("text", $col2, $lineCur, $str, $pdf);
            $str = sprintf "%12.4f", $bresult->{'score'};
            pdfDrawText("text", $col3, $lineCur, $str, $pdf);
            $str = sprintf "%8.4f", $bresult->{'index'};
            pdfDrawText("text", $col4, $lineCur, $str, $pdf);
            if ($catsRatioSum > 0) {
                $str = sprintf "%6s",
                               "$info->{'casesOpts'}->{$bench}{'weight'}";
                pdfDrawText("text", $col5, $lineCur, $str, $pdf);
                $str = sprintf "%1.2e", $bresult->{'findex'};
                pdfDrawText("text", $col6, $lineCur, $str, $pdf);
            }
        }
    }

    $lineCur -= $HEIGHT_FONT;
    if ($lineCur < $LINE_END + $HEIGHT_FONT) {
        newPdfPage($pdf);
        $lineCur -= $HEIGHT_FONT;
        $text = $pdf->{'text'};
    }
    pdfDrawText("text", $col4, $lineCur, "========", $pdf);
    if ($catsRatioSum > 0) {
        pdfDrawText("text", $col5, $lineCur, "=======  ========", $pdf);
    }

    $lineCur -= $HEIGHT_FONT;
    $str = sprintf "%s", $cat . " Index Score";
    pdfDrawText("textbb", $col1, $lineCur, $str, $pdf);
    $str = sprintf "%8.4f", $iscore;
    pdfDrawText("textbb", $col4, $lineCur, $str, $pdf);
    if ($catsRatioSum > 0) {
        $str = sprintf "%6s",
                       "$TestCases::testCats->{$cat}{'casesRatioSum'}";
        pdfDrawText("textbb", $col5, $lineCur, $str, $pdf);
        $str = sprintf "%8.4f", $fiscore;
        pdfDrawText("textbb", $col6, $lineCur, $str, $pdf);
    }

    $pdf;
}


# Display index scores, if any, for the given run results.
sub logIndexPdf {
    my ( $info, $results, $pdf ) = @_;
    my $count = $results->{'indexed'};

    foreach my $cat (sort(keys(%$count))) {
        $pdf = logIndexCatPdf($info, $results, $cat, $pdf);
    }

    $pdf;
}

sub logFinalPdf {
    my ( $info, $results, $pdf ) = @_;
    my $count = $results->{'indexed'};
    my $cscore = { };
    my $catsRatioSum = $info->{'catsRatioSum'};
    my $str;
    my $score = sprintf "%8.4f", $results->{'gmean'};

    my $text = $pdf->{'text'};
    $text->font($pdf->{'font'}, 10);

    my $fsize = $text->advancewidth("x");
    my $col = $COL_TAB1;
    my $col0 = $COL_COL1;
    my $col1 = $col0 + 20 * $fsize;
    my $col2 = $col1 + 22 * $fsize;
    my $col2e = $col2 + 12 * $fsize;
    my $col3 = $col2 + 17 * $fsize;
    my $col4 = $col3 + 12 * $fsize;

    if ($lineCur < $LINE_END + 3 * $HEIGHT_FONTB) {
        newPdfPage($pdf);
    } else {
        #pdfDrawHLineFull($pdf, $lineCur);
        $lineCur -= $HEIGHT_FONT;
    }

    pdfDrawText("textl", $col, $lineCur - $HEIGHT_FONTB / 2, "Result Description:", $pdf);
    pdfDrawText("textbb", $col0, $lineCur, "Category", $pdf);
    pdfDrawText("textbb", $col1, $lineCur, "Description", $pdf);
    if ($catsRatioSum > 0) {
        pdfDrawText("textbb", $col2, $lineCur, "  Orig score", $pdf);
        pdfDrawText("textbb", $col3, $lineCur, "Weight", $pdf);
        pdfDrawText("textbb", $col4, $lineCur, " Score", $pdf);
    } else {
        pdfDrawText("textbb", $col2, $lineCur, "       Score", $pdf);
    }
    $lineCur -= $HEIGHT_FONT / 2;
    pdfDrawHLineExt($pdf, $lineCur, $catsRatioSum > 0 ? 72 : 50, $col0);

    foreach my $cat (sort(keys(%$count))) {
        $lineCur -= $HEIGHT_FONT;
        if ($lineCur < $LINE_END + $HEIGHT_FONT) {
            newPdfPage($pdf);
            $lineCur -= $HEIGHT_FONT;
            $text = $pdf->{'text'};
        }
        pdfDrawText("textbb", $col0, $lineCur, $cat, $pdf);
        pdfDrawText("text", $col1, $lineCur,
                                  $TestCases::testCats->{$cat}{'name'}, $pdf);
        if ($catsRatioSum > 0) {
            $str = sprintf "%8.4f", $results->{'findex'}{$cat};
            pdfDrawText("textbb", $col2e - $text->advancewidth($str),
                                  $lineCur, $str, $pdf);
            $str = sprintf "    %s", "$TestCases::testCats->{$cat}{'ratio'}";
            pdfDrawText("textbb", $col3, $lineCur, $str, $pdf);
            $str = sprintf "%1.2e", $results->{'ffiscore'}{$cat};
            pdfDrawText("textbb", $col4, $lineCur, $str, $pdf);
        } else {
            $str = sprintf "%8.4f", $results->{'index'}{$cat};
            pdfDrawText("textbb", $col2e - $text->advancewidth($str),
                                  $lineCur, $str, $pdf);
        }
    }

    $lineCur -= $HEIGHT_FONT;
    if ($lineCur < $LINE_END + $HEIGHT_FONT) {
        newPdfPage($pdf);
        $lineCur -= $HEIGHT_FONT;
        $text = $pdf->{'text'};
    }
    if ($catsRatioSum > 0) {
        pdfDrawText("textbb", $col3, $lineCur, "========", $pdf);
        pdfDrawText("textbb", $col4, $lineCur, "========", $pdf);
        $lineCur -= $HEIGHT_FONT;
        #pdfDrawText("textbb", $col1, $lineCur, "               FINAL TOTAL RESULT :", $pdf);
        $str = sprintf "    %s", "$catsRatioSum";
        pdfDrawText("textbb", $col3, $lineCur, $str, $pdf);
        pdfDrawText("textbb", $col4, $lineCur, $score, $pdf);
    } else {
        pdfDrawText("textbb", $col2, $lineCur, "===========", $pdf);
        $lineCur -= $HEIGHT_FONT;
        #pdfDrawText("textbb", $col1, $lineCur, "                  Composite Score :", $pdf);
        pdfDrawText("textbb", $col2e - $text->advancewidth($score), $lineCur, $score, $pdf);
    }

    $lineCur -= $HEIGHT_FONTB / 2;
    $str = sprintf("%s (Total:%d, Success:%d, Failed:%d)",
        $results->{'reportStatus'}, $results->{'totalCases'}, $results->{'validCases'},
        $results->{'failCases'});
    pdfDrawText("textl", $col, $lineCur, "Result Status:", $pdf);
    pdfDrawText("textbb", $col0, $lineCur, $str, $pdf);

    if ($results->{'reportStatus'} ne "SUCCESS") {
        $lineCur -= $HEIGHT_FONT;
        pdfDrawText("textl", $col, $lineCur, "Failed Cases:", $pdf);
        $pdf = pdfDrawTextWrapAlignCommon("text", $col0, $PAGE_WIDTH,
                                                  $results->{'failList'},
                                                  $HEIGHT_FONTB / 2, $pdf);
    }

    $pdf;
}


# Dump the given run results into the given report file.
sub summarizeRunPdfCore {
    my ( $info, $results, $verbose, $pdf) = @_;

    my $title = sprintf "Benchmark Run: %s; %s with %s",
           number($info->{'numCpus'}, "CPU"),
           number($results->{'copies'}, "parallel process", "parallel processes"),
           number($ENV{'OMP_NUM_THREADS'}, "thread");
    my $time = $results->{'end'} - $results->{'start'};
    my $ttime = sprintf "Time: %s - %s, %dh%02dm%02ds",
                     strftime("%H:%M:%S", localtime($results->{'start'})),
                     strftime("%H:%M:%S", localtime($results->{'end'})),
                     int($time / 3600), int($time / 60) % 60, $time % 60;

    if ($lineCur < $LINE_END + 5 * $HEIGHT_FONTB) {
        newPdfPage($pdf);
    } else {
        $lineCur -= $HEIGHT_FONT;
        pdfDrawHLineFull($pdf, $lineCur);
    }

    $lineCur -= $HEIGHT_FONTB * 2 / 3;
    pdfDrawText("textt", $COL_BEGIN, $lineCur, $title, $pdf);
    $lineCur -= $HEIGHT_FONT;
    pdfDrawText("textbb", $COL_BEGIN, $lineCur, $ttime, $pdf);

    # Display the indexed scores, if any.
    $pdf = logIndexPdf($info, $results, $pdf);

    # Display the run scores.
    $pdf = logResultsPdf($results, $pdf);

    $pdf;
}

sub summarizeRunPdf {
    my ( $info, $results, $verbose, $pdf) = @_;

    $pdf = displaySystemPdf($info, $pdf);
    $pdf = logFinalPdf($info, $results, $pdf);
    $pdf = summarizeRunPdfCore($info, $results, $verbose, $pdf);
    $pdf = displaySystemPdfFull($info, $pdf);

    addResultAtFront($results, $pdf);
    addEndInfo($pdf, $results);
}

sub closePdf {
    my ( $pdf ) = @_;
    my $fpdf = $pdf->{'pdf'};

    $fpdf->saveas($pdf->{'fname'});
    $fpdf->release();
}
