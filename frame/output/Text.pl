#!/usr/bin/perl

use strict;

my $avoid_failure = {};

############################################################################
# TEXT REPORTS
############################################################################

sub printUsingDirectoriesToFile {

    my ( $info, $fd) = @_;

    printf $fd "------------------------------------------------------------------------------\n";
    printf $fd "   Use directories for:\n";
    printf $fd "      * Results                      = $info->{'resultdir'}\n";
    printf $fd "------------------------------------------------------------------------------\n";
    printf $fd "\n";
}

sub runHeader {
    my ( $systemInfo, $reportFd, $brandInfo) = @_;
    printf $reportFd "   %s (Version %s)\n\n", $brandInfo->{'bench_name'}, $brandInfo->{'version'};

}

# Display a banner indicating the configuration of the system under test
# to the given file desc.
sub displaySystem {
    my ( $info, $fd ) = @_;
    my @items0;
    my @items1;
    my @items2;
    my @items3;
    my @items4;
    my @items5;
    my $count;
    my $i;

    # Display basic system info.
    printf $fd "   Brand:         %s\n", $info->{'minfo'}{'brandmodel'};
    printf $fd "   Bios:          %s\n", $info->{'minfo'}{'bios'};
    printf $fd "   Arch:          %s (%s)\n", $info->{'mach'}, $info->{'platform'};

    # Get and display details on the CPUs, if possible.
    my $cpus = $info->{'cpus'};
    printf $fd "   CPUs(%s):\n", $#$cpus + 1;
    if (!defined($cpus)) {
        printf $fd "              no details available\n";
    } else {
        if (!defined($cpus->[$0]{'flags'})) {
            $cpus->[0]{'flags'} = '';
        }
        printf $fd "                  %s (%.1f bogomips)\n",
                                  $cpus->[0]{'model'}, $cpus->[0]{'bogo'};
        printf $fd "                  %s\n", $cpus->[0]{'flags'};
    }

    @items2 = @{$info->{'minfo'}{'caches'}};
    $count = @items2;
    printf $fd "   Caches:\n";
    for ($i = 0; $i < $count; $i++) {
        printf $fd "                  %s\n", @items2[$i];
    }
    printf $fd "\n";

    printf $fd "   Numa Topology: %s\n", $info->{'minfo'}{'numacount'};
    printf $fd "   Total Memory:  %s MB \n", $info->{'memTotal'};
    printf $fd "   Memory Count:  %s\n", $info->{'minfo'}{'memcount'};
    printf $fd "   Memory Manu:   %s\n", $info->{'minfo'}{'memmanu'};
    printf $fd "   Memory Type:   %s\n", $info->{'minfo'}{'memtype'};
    printf $fd "   Memory Speed:  %s\n\n", $info->{'minfo'}{'memspeed'};

    printf $fd "   System:        %s: %s\n", $info->{'name'}, $info->{'system'};
    printf $fd "   OS Release:    %s\n", $info->{'minfo'}{'osrelease'};
    printf $fd "   OS Kernel:     %s -- %s -- %s\n",
                        $info->{'os'}, $info->{'osRel'}, $info->{'osVer'};
    printf $fd "   Language:      %s\n", $info->{'language'};
    printf $fd "   Bash:          %s\n", $info->{'minfo'}{'bash'};
    printf $fd "   GCC:           %s\n", $info->{'minfo'}{'gcc'};
    printf $fd "   G++:           %s\n", $info->{'minfo'}{'g++'};
    printf $fd "   Binutils:      %s\n", $info->{'minfo'}{'binutils'};
    printf $fd "   Python2:       %s\n", $info->{'minfo'}{'python2'};
    printf $fd "   Python3:       %s\n", $info->{'minfo'}{'python3'};
    printf $fd "   Java:          %s\n", $info->{'minfo'}{'java'};
    printf $fd "   GlibC:         %s\n", $info->{'minfo'}{'glibc'};
    printf $fd "   Glib:          %s\n", $info->{'minfo'}{'glib'};
    printf $fd "   StdC++:        %s\n", $info->{'minfo'}{'stdc++'};
    printf $fd "   QT:            %s\n", $info->{'minfo'}{'qt'};
    printf $fd "   File System:   %s\n", $info->{'minfo'}{'fs'};
    printf $fd "\n\n";

    ## Display system load and usage info.
    #printf $fd "   %s; runlevel %s\n\n", $info->{'load'}, $info->{'runlevel'};

    #printUsingDirectoriesToFile($info, $fd);
    #printf $fd "Total disk layout\n%s\n", $info->{'diskinfo'};

    @items0 = @{$info->{'minfo'}{'harddisks'}};
    $count = @items0;
    printf $fd "   Disks:\n";
    for ($i = 0; $i < $count; $i++) {
        printf $fd "                 %s\n", @items0[$i];
    }
    @items1 = @{$info->{'minfo'}{'ethcards'}};
    $count = @items1;
    printf $fd "   Netcards:\n";
    for ($i = 0; $i < $count; $i++) {
        printf $fd "                 %s\n", @items1[$i];
    }
    if ($info->{'graphics'}) {
        printf $fd "   Graphics:     %s\n", $info->{'graphics'};
    }
    @items3 = @{$info->{'minfo'}{'dispcards'}};
    $count = @items3;
    printf $fd "   Display Cards:\n";
    for ($i = 0; $i < $count; $i++) {
        printf $fd "                 %s\n", @items3[$i];
    }
    printf $fd "\n";

    printf $fd "   Base Opts:\n\n";
    @items4 = @{$info->{'baseopts'}};
    $count = @items4;
    for ($i = 0; $i < $count; $i++) {
        printf $fd "                 %s\n", @items4[$i];
    }
    printf $fd "\n";
    if ($info->{'ispeak'} != 0) {
        printf $fd "   Peak Opts:\n\n";
        @items5 = @{$info->{'peakopts'}};
        $count = @items5;
        for ($i = 0; $i < $count; $i++) {
            printf $fd "                 %s\n", @items5[$i];
        }
        printf $fd "\n";
    }
    printf $fd "   Runlevel:      %s\n", $info->{'runlevel'};
    printf $fd "\n";
}

# Display the test scores from the given set of test results.
sub logResults {
    my ( $results, $outFd ) = @_;

    # Display the individual test scores.
    foreach my $bench (@{$results->{'list'}}) {
        my $bresult = $results->{$bench};

        printf $outFd "%-25s %-40s %12.4f %-5s (%.1f s, %d samples)\n",
                      $bench,
                      $bresult->{'msg'},
                      $bresult->{'score'},
                      $bresult->{'scorelabel'},
                      $bresult->{'time'},
                      $bresult->{'iterations'};
    }

    printf $outFd "\n";
    printf $outFd "\n";
    printf $outFd "\n";
}


# Display index scores, if any, for the given run results.
sub logIndexCat {
    my ( $info, $results, $cat, $outFd) = @_;

    my $catsRatioSum = $info->{'catsRatioSum'};
    my $total = $results->{'numIndex'}{$cat};
    my $indexed = $results->{'indexed'}{$cat};
    my $iscore = $results->{'index'}{$cat};
    my $fiscore = $results->{'findex'}{$cat};
    my $full = defined($indexed) && $indexed == $total;

    # If there are no indexed scores, just say so.
    if (!defined($indexed) || $indexed == 0) {
        printf $outFd "No index results available for %s\n\n",
                      $TestCases::testCats->{$cat}{'name'};
        return;
    }

    # Display the header, depending on whether we have a full set of index
    # scores, or a partial set.
    my $head = $cat . ($full ? " Index Values" : " Partial Index");
    printf $outFd "%-25s %-40s %12s %12s %8s ",
                  "TestCases", $head, "BASELINE", "RESULT", "INDEX";
    if ($catsRatioSum > 0) {
        printf $outFd " %12s %8s", "WEIGHT", "FINDEX";
    }
    printf $outFd "\n";
    if ($catsRatioSum > 0) {
        printf $outFd "%s\n",
                  "----------------------------------------------------------------------------------------------------------------------------";
    } else {
        printf $outFd "%s\n",
                  "-----------------------------------------------------------------------------------------------------";
    }

    # Display the individual test scores.
    foreach my $bench (@{$results->{'list'}}) {
        my $bresult = $results->{$bench};
        next if $bresult->{'cat'} ne $cat;

        if (defined($bresult->{'iscore'}) && defined($bresult->{'index'})) {
            printf $outFd "%-25s %-40s %12.4f %12.4f %8.4f ",
                      $bench, $bresult->{'msg'}, $bresult->{'iscore'},
                      $bresult->{'score'}, $bresult->{'index'};
            if ($catsRatioSum > 0) {
                printf $outFd " %12s %1.2e",
                              "$info->{'casesOpts'}->{$bench}{'weight'}",
                              $bresult->{'findex'};
            }
        } else {
            printf $outFd "%-25s %-40s %12s %12.4f %8s",
                      $bench, $bresult->{'msg'}, "---",
                      $bresult->{'score'}, "---";
            if ($catsRatioSum > 0) {
                printf $outFd " %12s %8s", "----", "---";
            }
        }
        printf $outFd "\n";
    }

    # Display the overall score.
    my $title = $cat . " Index Score";
    if (!$full) {
        $title .= " (Partial Only)";
    }
    printf $outFd "%-25s %-40s %12s %12s %8s %14s", "", "", "", "", "========", "";
    if ($catsRatioSum > 0) {
        printf $outFd " %8s", "", "========";
    }
    printf $outFd "\n";
    printf $outFd "%-92s %8.4f", $title, $iscore;

    if ($catsRatioSum > 0) {
        printf $outFd "  %12s %8.4f", "$TestCases::testCats->{$cat}{'casesRatioSum'}", $fiscore;
    }

    printf $outFd "\n\n\n";
}


# Display index scores, if any, for the given run results.
sub logIndex {
    my ( $info, $results, $outFd) = @_;

    my $count = $results->{'indexed'};
    my $cscore = { };

    foreach my $cat (sort(keys(%$count))) {
        $cscore->{$cat} = $results->{'index'}{$cat};
        logIndexCat($info, $results, $cat, $outFd);
    }

    if ($info->{'catsRatioSum'} > 0) {
        printf $outFd "%-12s %-25s %8s %12s %8s\n",
                      "CAT NAME", "CAT DESC", "FINDEX", "WEIGHT", "FFINDEX";
        printf $outFd "%s\n",
                      "---------------------------------------------------------------------";
        foreach my $cat (keys(%$count)) {
            printf $outFd "%-12s %-25s %8.4f %12s %1.2e\n",
                      $cat,
                      $TestCases::testCats->{$cat}{'name'},
                      $results->{'findex'}{$cat},
                      "$TestCases::testCats->{$cat}{'ratio'}",
                      $results->{'ffiscore'}{$cat};
        }
        printf $outFd "%-12s %-25s %8s %12s %8s\n",
                      "", "", "", "", "========";
        printf $outFd "%-12s %-25s %8s %12s %8.4f\n",
                      "---", "FINAL TOTAL RESULT:", "---",
                      "$info->{'catsRatioSum'}", $results->{'gmean'};
        printf $outFd "\n\n\n";
    } else {
        printf $outFd "%-16s %8s\n", "CAT NAME", "INDEX";
        printf $outFd "-------------------------\n";
        foreach my $cat (sort(keys(%$count))) {
            printf $outFd "%-16s %8.4f\n", $cat, $results->{'index'}{$cat};
        }
        printf $outFd "%-16s %8s\n", "", "========";
        printf $outFd "%-16s %8.4f\n\n\n", "Composite Score", $results->{'gmean'};
    }
}

# Dump the given run results into the given report file.
sub summarizeRun {
    my ( $info, $results, $verbose, $reportFd ) = @_;

    my $tStart=timelocal(localtime($results->{'start'}));
    my $tEnd=timelocal(localtime($results->{'end'}));
    my $tDiff=$tEnd - $tStart;
    my $tMinute=int($tDiff / 60);
    my $tSecond=$tDiff % 60;
    my $tHour=int($tDiff / 3600);
    $tMinute=$tMinute % 60;

    # Display information about this test run.
    printf $reportFd "------------------------------------------------------------------------\n";
    printf $reportFd "Benchmark Run: %s %s - %s (%sh%sm%ss)\n",
           strftime("%Y-%m-%d", localtime($results->{'start'})),
           strftime("%H:%M:%S", localtime($results->{'start'})),
           strftime("%H:%M:%S", localtime($results->{'end'})),
           $tHour, $tMinute, $tSecond;
    printf $reportFd "%s in system; running %s of tests with %s\n",
           number($info->{'numCpus'}, "CPU"),
           number($results->{'copies'}, "parallel copy", "parallel copies"),
           number($ENV{'OMP_NUM_THREADS'}, "thread");
    printf $reportFd "\n";

    # Display the run scores.
    logResults($results, $reportFd);

    # Display the indexed scores, if any.
    logIndex($info, $results, $reportFd);

    printf $reportFd "------------------------------------------------------------------------\n";
    my $tmpstr = sprintf ("REPORT STATUS: %s\n    Copies:       %d\n    TotalCases:   %d\n    SuccessCases: %d\n    FailCases:    %d\n    Basefile:     %s\n",
        $results->{'reportStatus'},
        $results->{'copies'},
        $results->{'totalCases'}, $results->{'validCases'},
        $results->{'failCases'}, $results->{'baseFile'});
    my $failstr = "";
    if ($results->{'failCases'}) {
        $failstr = sprintf("    FailList:     %s\n\n",, $results->{'failList'});
    }
    printf $reportFd "%s%s", $tmpstr, $failstr;
}
