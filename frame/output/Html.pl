#!/usr/bin/perl

use strict;

my $avoid_failure = {};

############################################################################
# HTML REPORTS
############################################################################

# Dump the given run results into the given report file.
sub runHeaderHtml {
    my ( $systemInfo, $reportFd, $brandInfo) = @_;

    # Display information about this test run.
    my $title = sprintf "Benchmark of %s / %s on %s",
                     $systemInfo->{'name'}, $systemInfo->{'system'},
                     strftime("%Y-%m-%d", localtime());

    print $reportFd <<EOF;
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="keywords" content="linux, benchmarks, benchmarking">
  <title>$title</title>
  <style type="text/css">
    table {
      margin: 1em 1em 1em 0;
      background: #f9f9f9;
      border: 1px #aaaaaa solid;
      border-collapse: collapse;
    }

    table th, table td {
      border: 1px #aaaaaa solid;
      padding: 0.2em;
    }

    table th {
      background: #f2f2f2;
      text-align: center;
    }
  </style>
</head>
<body>
EOF

    # Display information about this test run.
    printf $reportFd "<h2>%s</h2>\n", $title;
    printf $reportFd "<p><b>%s (Version %s)</b></p>\n\n",
                      $brandInfo->{'bench_name'}, $brandInfo->{'version'};
}


# Display a banner indicating the configuration of the system under test
# to the given file desc.
sub displaySystemHtml {
    my ( $info, $fd ) = @_;
    my @items0;
    my @items1;
    my @items2;
    my @items3;
    my @items4;
    my @items5;
    my $count;
    my $i;

    printf $fd "<h3>Test System Information</h3>\n";
    printf $fd "<p><table>\n";

    printf $fd "   <td><b>Brand:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'minfo'}{'brandmodel'};
    printf $fd "</tr><tr>\n";

    printf $fd "   <td><b>Bios:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'minfo'}{'bios'};
    printf $fd "</tr><tr>\n";

    printf $fd "   <td><b>Arch:</b></td>\n";
    printf $fd "   <td colspan=2>%s: %s</td>\n",
               $info->{'mach'}, $info->{'platform'};
    printf $fd "</tr><tr>\n";

    my $cpus = $info->{'cpus'};
    printf $fd "<tr>\n";
    printf $fd "   <td><b>CPUs(%d):</b></td>\n", $#$cpus + 1;
    if (!defined($cpus)) {
        printf $fd "   <td colspan=2>no details available</td>\n";
    } else {
        if (!defined($cpus->[0]{'flags'})) {
            $cpus->[0]{'flags'} = "";
        }
        printf $fd "   <td>%s (%.4f bogomips)<br/>\n",
                    $cpus->[0]{'model'}, $cpus->[0]{'bogo'};
        printf $fd "       %s</td>\n", $cpus->[0]{'flags'};
    }
    printf $fd "</tr>\n";

    @items2 = @{$info->{'minfo'}{'caches'}};
    $count = @items2;
    for ($i = 0; $i < $count; $i++) {
            printf $fd "<tr>\n";
            if ($i == 0) {
                printf $fd "   <td rowspan=%d><b>Caches:</b></td>\n", $count;
            }
            printf $fd "   <td>%s</td>\n", @items2[$i];
            printf $fd "</tr>\n";
    }

    printf $fd "<tr>\n";
    printf $fd "   <td><b>Numa Topology:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'minfo'}{'numacount'};
    printf $fd "</tr>\n";

    printf $fd "<tr>\n";
    printf $fd "   <td><b>Total Memory:</b></td>\n";
    printf $fd "   <td colspan=2>%s MB</td>\n", $info->{'memTotal'};
    printf $fd "</tr>\n";

    printf $fd "<tr>\n";
    printf $fd "   <td><b>Memory Count:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'minfo'}{'memcount'};
    printf $fd "</tr>\n";

    printf $fd "<tr>\n";
    printf $fd "   <td><b>Memory Manu:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'minfo'}{'memmanu'};
    printf $fd "</tr>\n";

    printf $fd "<tr>\n";
    printf $fd "   <td><b>Memory Type:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'minfo'}{'memtype'};
    printf $fd "</tr>\n";

    printf $fd "<tr>\n";
    printf $fd "   <td><b>Memory Speed:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'minfo'}{'memspeed'};
    printf $fd "</tr>\n";

    printf $fd "<tr>\n";
    printf $fd "   <td><b>System:</b></td>\n";
    printf $fd "   <td colspan=2>%s: %s</td>\n",
               $info->{'name'}, $info->{'system'};
    printf $fd "</tr><tr>\n";

    printf $fd "   <td><b>OS Release:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'minfo'}{'osrelease'};
    printf $fd "</tr><tr>\n";

    printf $fd "   <td><b>OS Kernel:</b></td>\n";
    printf $fd "   <td colspan=2>%s -- %s -- %s</td>\n",
               $info->{'os'}, $info->{'osRel'}, $info->{'osVer'};
    printf $fd "</tr><tr>\n";

    printf $fd "   <td><b>Language:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'language'};
    printf $fd "</tr>\n";

    printf $fd "   <td><b>Bash:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'minfo'}{'bash'};
    printf $fd "</tr><tr>\n";

    printf $fd "   <td><b>GCC:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'minfo'}{'gcc'};
    printf $fd "</tr><tr>\n";

    printf $fd "   <td><b>G++:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'minfo'}{'g++'};
    printf $fd "</tr><tr>\n";

    printf $fd "   <td><b>Binutils:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'minfo'}{'binutils'};
    printf $fd "</tr><tr>\n";

    printf $fd "   <td><b>Python2:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'minfo'}{'python2'};
    printf $fd "</tr><tr>\n";

    printf $fd "   <td><b>Python3:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'minfo'}{'python3'};
    printf $fd "</tr><tr>\n";

    printf $fd "   <td><b>Java:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'minfo'}{'java'};
    printf $fd "</tr><tr>\n";

    printf $fd "   <td><b>GlibC:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'minfo'}{'glibc'};
    printf $fd "</tr><tr>\n";

    printf $fd "   <td><b>Glib:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'minfo'}{'glib'};
    printf $fd "</tr><tr>\n";

    printf $fd "   <td><b>StdC++:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'minfo'}{'stdc++'};
    printf $fd "</tr><tr>\n";

    printf $fd "   <td><b>QT:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'minfo'}{'qt'};
    printf $fd "</tr><tr>\n";

    printf $fd "   <td><b>File System:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'minfo'}{'fs'};
    printf $fd "</tr><tr>\n";

    @items0 = @{$info->{'minfo'}{'harddisks'}};
    $count = @items0;
    for ($i = 0; $i < $count; $i++) {
        printf $fd "<tr>\n";
        if ($i == 0) {
            printf $fd "   <td rowspan=%d><b>Disks:</b></td>\n", $count;
        }
        printf $fd "   <td>%s</td>\n", @items0[$i];
        printf $fd "</tr>\n";
    }

    @items1 = @{$info->{'minfo'}{'ethcards'}};
    $count = @items1;
    for ($i = 0; $i < $count; $i++) {
        printf $fd "<tr>\n";
        if ($i == 0) {
            printf $fd "   <td rowspan=%d><b>Netcards:</b></td>\n", $count;
        }
        printf $fd "   <td>%s</td>\n", @items1[$i];
        printf $fd "</tr>\n";
    }

    @items3 = @{$info->{'minfo'}{'dispcards'}};
    $count = @items3;
    for ($i = 0; $i < $count; $i++) {
        printf $fd "<tr>\n";
        if ($i == 0) {
            printf $fd "   <td rowspan=%d><b>Display Cards:</b></td>\n", $count;
        }
        printf $fd "   <td>%s</td>\n", @items3[$i];
        printf $fd "</tr>\n";
    }

    if ($info->{'graphics'}) {
        printf $fd "<tr>\n";
        printf $fd "   <td><b>Graphics:</b></td>\n";
        printf $fd "   <td colspan=2>%s</td>\n", $info->{'graphics'};
        printf $fd "</tr>\n";
    }

    @items4 = @{$info->{'baseopts'}};
    $count = @items4;
    for ($i = 0; $i < $count; $i++) {
        printf $fd "<tr>\n";
        if ($i == 0) {
            printf $fd "   <td rowspan=%d><b>Base Opt:</b></td>\n", $count;
        }
        printf $fd "   <td>%s</td>\n", @items4[$i];
        printf $fd "</tr>\n";
    }

    if ($info->{'ispeak'} != 0) {
        @items5 = @{$info->{'peakopts'}};
        $count = @items5;
        for ($i = 0; $i < $count; $i++) {
            printf $fd "<tr>\n";
            if ($i == 0) {
                printf $fd "   <td rowspan=%d><b>Peak Opt:</b></td>\n", $count;
            }
            printf $fd "   <td>%s</td>\n", @items5[$i];
            printf $fd "</tr>\n";
        }
    }

    # Display system runlevel.
    printf $fd "<tr>\n";
    printf $fd "   <td><b>Runlevel:</b></td>\n";
    printf $fd "   <td colspan=2>%s</td>\n", $info->{'runlevel'};
    printf $fd "</tr>\n";

    printf $fd "</table></p>\n\n";
}

# Display the test scores from the given set of test results.
sub logResultsHtml {
    my ( $results, $fd ) = @_;

    printf $fd "<p><table width=\"100%%\">\n";

    #printf $fd "<tr>\n";
    #printf $fd "   <th align=left>Testcase</th>\n";
    #printf $fd "   <th align=left>Description</th>\n";
    #printf $fd "   <th align=right>Score</th>\n";
    #printf $fd "   <th align=left>Unit</th>\n";
    #printf $fd "   <th align=right>Time</th>\n";
    #printf $fd "   <th align=right>Iters.</th>\n";
    #printf $fd "</tr>\n";

    # Display the individual test scores.
    foreach my $bench (@{$results->{'list'}}) {
        my $bresult = $results->{$bench};

        printf $fd "<tr>\n";

        printf $fd "   <td><b>%s</b></td>\n", $bench;
        printf $fd "   <td><b>%s</b></td>\n", $bresult->{'msg'};
        printf $fd "   <td align=right><tt>%.4f</tt></td>\n",
                   $bresult->{'score'};
        printf $fd "   <td align=left><tt>%s</tt></td>\n",
                   $bresult->{'scorelabel'};
        printf $fd "   <td align=right><tt>%.4f s</tt></td>\n",
                   $bresult->{'time'};
        printf $fd "   <td align=right><tt>%d samples</tt></td>\n",
                   $bresult->{'iterations'};

        printf $fd "</tr>\n";
    }

    printf $fd "</table></p>\n\n\n";
}


# Display the test scores from the given set of test results
# for a given category of tests.
sub logIndexCatHtml {
    my ( $info, $results, $cat, $fd ) = @_;

    my $catsRatioSum = $info->{'catsRatioSum'};
    my $total = $results->{'numIndex'}{$cat};
    my $indexed = $results->{'indexed'}{$cat};
    my $iscore = $results->{'index'}{$cat};
    my $fiscore = $results->{'findex'}{$cat};
    my $full = defined($indexed) && $indexed == $total;
    my $head = $cat . ($full ? " Index Values" : " Partial Index");

    # If there are no results in this category, just ignore it.
    if (!defined($results->{'numCat'}{$cat}) ||
                            $results->{'numCat'}{$cat} == 0) {
        return;
    }

    # Say the category.  If there are no indexed scores, just say so.
    my $warn = "";
    if (!defined($indexed) || $indexed == 0) {
        $warn = " - no index results available";
    } elsif (!$full) {
        $warn = " - not all index tests were run;" .
                " only a partial index score is available";
    }
    printf $fd "<h4>%s%s</h4>\n", $TestCases::testCats->{$cat}{'name'}, $warn;

    printf $fd "<p><table width=\"100%%\">\n";

    printf $fd "<tr>\n";
    printf $fd "   <th align=left>Testcase</th>\n";
    printf $fd "   <th align=left>%s</th>\n", $head;
    printf $fd "   <th align=right>Baseline</th>\n";
    printf $fd "   <th align=right>Result</th>\n";
    printf $fd "   <th align=right>Index</th>\n";
    if ($catsRatioSum > 0) {
        printf $fd "   <th align=right>Weight</th>\n";
        printf $fd "   <th align=right>FIndex</th>\n";
    }
    printf $fd "</tr>\n";

    # Display the individual test scores.
    foreach my $bench (@{$results->{'list'}}) {
        my $bresult = $results->{$bench};
        next if $bresult->{'cat'} ne $cat;

        printf $fd "<tr>\n";
        printf $fd "   <td><b>%s</b></td>\n", $bench;
        printf $fd "   <td><b>%s</b></td>\n", $bresult->{'msg'};
        if (defined($bresult->{'iscore'}) && defined($bresult->{'index'})) {
            printf $fd "   <td align=right><tt>%.4f</tt></td>\n",
                       $bresult->{'iscore'};
            printf $fd "   <td align=right><tt>%.4f</tt></td>\n",
                       $bresult->{'score'};
            printf $fd "   <td align=right><tt>%.4f</tt></td>\n",
                       $bresult->{'index'};
            if ($catsRatioSum > 0) {
                printf $fd "   <td align=right><b>%s</b></td>\n",
                              "$info->{'casesOpts'}->{$bench}{'weight'}";
                printf $fd "   <td align=right><tt>%1.2e</tt></td>\n",
                              $bresult->{'findex'};
            }
        } else {
            printf $fd "   <td align=right>---</td>\n";
            printf $fd "   <td align=right><tt>%.4f</tt></td>\n",
                       $bresult->{'score'};
            printf $fd "   <td align=right>---</td>\n";
            if ($catsRatioSum > 0) {
                printf $fd "   <td align=right>---</td>\n";
                printf $fd "   <td align=right>---</td>\n";
            }
        }
        printf $fd "</tr>\n";
    }

    # Display the overall score.
    if (defined($indexed) && $indexed > 0) {
        my $title = $TestCases::testCats->{$cat}{'name'} . " Index Score";
        if (!$full) {
            $title .= " (Partial Only)";
        }
        printf $fd "<tr>\n";
        printf $fd "   <td colspan=4><b>%s:</b></td>\n", $title;
        printf $fd "   <td align=right><b><tt>%.4f</tt></b></td>\n", $iscore;
        if ($catsRatioSum > 0) {
            printf $fd "   <td align=right><b>%s</b></td>\n",
                           "$TestCases::testCats->{$cat}{'casesRatioSum'}";
            printf $fd "   <td align=right><b><tt>%.4f</tt></b></td>\n",
                           $fiscore;
        }
        printf $fd "</tr>\n";
    }

    printf $fd "</table></p>\n\n";
}


# Display index scores, if any, for the given run results.
sub logIndexHtml {
    my ( $info, $results, $fd ) = @_;
    my $count = $results->{'indexed'};
    my $cscore = { };

    foreach my $cat (sort(keys(%$count))) {
        $cscore->{$cat} = $results->{'index'}{$cat};
        logIndexCatHtml($info, $results, $cat, $fd);
    }

    printf $fd "<h4></h4>\n";
    printf $fd "<h4>Total:</h4>\n";
    printf $fd "<p><table width=\"70%\">\n";

    if ($info->{'catsRatioSum'} > 0) {
        printf $fd "<tr>\n";
        printf $fd "   <th align=left>CATA NAME</th>\n";
        printf $fd "   <th align=left>CAT DESC</th>\n";
        printf $fd "   <th align=right>FINDEX</th>\n";
        printf $fd "   <th align=right>WEIGHT</th>\n";
        printf $fd "   <th align=right>FFINDEX</th>\n";
        printf $fd "</tr>\n";

        foreach my $cat (sort(keys(%$count))) {
            printf $fd "<tr>\n";
            printf $fd "   <td><b>%s</b></td>\n", $cat;
            printf $fd "   <td><b>%s</b></td>\n",
                               $TestCases::testCats->{$cat}{'name'};
            printf $fd "   <td align=right><tt>%8.4f</tt></td>\n",
                               $results->{'findex'}{$cat};
            printf $fd "   <td align=right><b>%s</b></td>\n",
                               "$TestCases::testCats->{$cat}{'ratio'}";
            printf $fd "   <td align=right><tt>%1.2e</tt></td>\n",
                               $results->{'ffiscore'}{$cat};
            printf $fd "</tr>\n";
        }

        printf $fd "<tr>\n";
        printf $fd "   <td colspan=3 align=right><b>%s       </b></td>\n",
                               "FINAL TOTAL RESULT :";
        printf $fd "   <td align=right><b>%s</b></td>\n",
                               "$info->{'catsRatioSum'}";
        printf $fd "   <td align=right><b><tt>%.4f</tt></b></td>\n",
                               $results->{'gmean'};
        printf $fd "</tr>\n";
    } else {
        printf $fd "<tr>\n";
        printf $fd "   <th align=left>CATA NAME</th>\n";
        printf $fd "   <th align=right>INDEX</th>\n";
        printf $fd "</tr>\n";
        foreach my $cat (sort(keys(%$count))) {
            printf $fd "<tr>\n";
            printf $fd "   <td><b>%s</b></td>\n", $cat;
            printf $fd "   <td align=right><tt>%8.4f</tt></td>\n",
                               $results->{'index'}{$cat};
            printf $fd "</tr>\n";
        }
        printf $fd "<tr>\n";
        printf $fd "   <td><b>%s</b></td>\n", "Composite Score";
        printf $fd "   <td align=right><tt>%8.4f</tt></td>\n",
                               $results->{'gmean'};
        printf $fd "</tr>\n";
    }

    printf $fd "</table></p>\n\n";
}


# Dump the given run results into the given report file.
sub summarizeRunHtml {
    my ( $info, $results, $verbose, $reportFd ) = @_;

    # Display information about this test run.
    my $time = $results->{'end'} - $results->{'start'};
    printf $reportFd "<h3>Benchmark Run: %s; %s with %s</h3>\n",
           number($info->{'numCpus'}, "CPU"),
           number($results->{'copies'}, "parallel process", "parallel processes"),
           number($ENV{'OMP_NUM_THREADS'}, "thread");
    printf $reportFd "<p>Time: %s - %s; %dm %02ds</p>\n",
                     strftime("%H:%M:%S", localtime($results->{'start'})),
                     strftime("%H:%M:%S", localtime($results->{'end'})),
                     int($time / 60), $time % 60;
    printf $reportFd "\n";

    # Display the run scores.
    logResultsHtml($results, $reportFd);

    # Display the indexed scores, if any.
    logIndexHtml($info, $results, $reportFd);

    printf $reportFd "<p><hr/></p>\n";
    printf $reportFd "<h3>REPORT STATUS: %s</h3>\n", $results->{'reportStatus'};
    printf $reportFd "<p>    TotalCases: %d</p>    <p>ValidCases: %d</p>    <p>FailCases: %d</p>    <p>BaseFile: %s</p>\n",
        $results->{'totalCases'}, $results->{'validCases'},
        $results->{'failCases'}, $results->{'baseFile'};
    printf $reportFd "<p>    FailList: %s</p>\n", $results->{'failList'};
}


sub runFooterHtml {
    my ( $reportFd ) = @_;

    print $reportFd <<EOF;
<p><hr/></p>
<div><b>No Warranties:</b> This information is provided free of charge and "as
is" without any warranty, condition, or representation of any kind,
either express or implied, including but not limited to, any warranty
respecting non-infringement, and the implied warranties of conditions
of merchantability and fitness for a particular purpose. All logos or
trademarks on this site are the property of their respective owner. In
no event shall the author be liable for any
direct, indirect, special, incidental, consequential or other damages
howsoever caused whether arising in contract, tort, or otherwise,
arising out of or in connection with the use or performance of the
information contained on this web site.</div>
</body>
</html>
EOF
}
