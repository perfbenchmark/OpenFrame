#!/usr/bin/perl

use strict;

my $avoid_failure = {};

############################################################################
# JSON REPORTS
############################################################################

sub runHeaderJson {
    my ( $systemInfo, $reportFd, $brandInfo) = @_;
    printf $reportFd "{\n";
    printf $reportFd "   \"ENVinfo\":[\n";
    printf $reportFd "   {\"compent\":\"%s_version\", \"version\":\"%s\"},\n", $brandInfo->{'bench_name'}, $brandInfo->{'version'};

}

# Display a banner indicating the configuration of the system under test
# to the given file desc.
sub displaySystemJson {
    my ( $info, $fd ) = @_;
    my @items0;
    my @items1;
    my $count;
    my $i;

    # Display basic system info.
    printf $fd "   {\"compent\":\"hid\",     \"description\":\"%s\"},\n", $info->{'minfo'}{'hid'};
    printf $fd "   {\"compent\":\"sid\",     \"description\":\"%s\"},\n", $info->{'minfo'}{'sid'};
    printf $fd "   {\"compent\":\"bios\",     \"description\":\"%s\"},\n", $info->{'minfo'}{'bios'};
    printf $fd "   {\"compent\":\"hostname\",     \"description\":\"\%s: \%s\"},\n", $info->{'name'}, $info->{'system'};
    printf $fd "   {\"compent\":\"baseboard\", \"manufacturer\":\"%s\", \"serialnumber\":\"%s\"},\n", $info->{'minfo'}{'manufacturer'},$info->{'minfo'}{'serialnumber'};
    printf $fd "   {\"compent\":\"kernel_version\",         \"description\":\"%s -- %s\"},\n",
                        $info->{'os'}, $info->{'osRel'};
    printf $fd "   {\"compent\":\"os_version\", \"description\":\"%s\"},\n", $info->{'minfo'}{'osrelease'};
    printf $fd "   {\"compent\":\"bash\",     \"version\":\"%s\"},\n", $info->{'minfo'}{'bash'};
    printf $fd "   {\"compent\":\"gcc\",     \"version\":\"%s\"},\n", $info->{'minfo'}{'gcc'};
    printf $fd "   {\"compent\":\"g++\",     \"version\":\"%s\"},\n", $info->{'minfo'}{'g++'};
    printf $fd "   {\"compent\":\"binutils\",     \"version\":\"%s\"},\n", $info->{'minfo'}{'binutils'};
    printf $fd "   {\"compent\":\"python2\",     \"version\":\"%s\"},\n", $info->{'minfo'}{'python2'};
    printf $fd "   {\"compent\":\"python3\",     \"version\":\"%s\"},\n", $info->{'minfo'}{'python3'};
    printf $fd "   {\"compent\":\"java\",     \"version\":\"%s\"},\n", $info->{'minfo'}{'java'};
    printf $fd "   {\"compent\":\"glibc\",     \"version\":\"%s\"},\n", $info->{'minfo'}{'glibc'};
    printf $fd "   {\"compent\":\"glib\",     \"version\":\"%s\"},\n", $info->{'minfo'}{'glib'};
    printf $fd "   {\"compent\":\"stdc++\",     \"version\":\"%s\"},\n", $info->{'minfo'}{'stdc++'};
    printf $fd "   {\"compent\":\"qt\",     \"version\":\"%s\"},\n", $info->{'minfo'}{'qt'};
    printf $fd "   {\"compent\":\"file_system\",     \"version\":\"%s\"},\n", $info->{'minfo'}{'fs'};
    #printf $fd "   {\"compent\":\"Machine\",    \"description\":\"%s\"},\n", $info->{'mach'};
    printf $fd "   {\"compent\":\"brand\",      \"description\":\"%s\"},\n", $info->{'minfo'}{'brandmodel'};
    printf $fd "   {\"compent\":\"runlevel\",      \"description\":\"%s\"},\n", $info->{'runlevel'};
    my $lange = $info->{'language'};
    $lange=~s/"//g;
    printf $fd "   {\"compent\":\"language\",   \"description\":\"%s\"},\n", $lange;

    # Get and display details on the CPUs, if possible.
    my $cpus = $info->{'cpus'};
    my @caches;
    @caches = @{$info->{'minfo'}{'caches'}};
    if (!defined($cpus)) {
        printf $fd "   {\"tips\":\"CPU: no details available\"},\n";
    } else {
        my $cpunum = $#$cpus+1;
        for (my $i = 0; $i <= $#$cpus; ++$i) {
            if (!defined($cpus->[$i]{'flags'})) {
                $cpus->[$i]{'flags'} = '';
            }
            printf $fd "   {\"compent\":\"cpu\", \"description\":\"%s\", \"bogomips\":%.1f, \"cpuid\":\"%s\", \"corenum\":%d, \"arch\":\"%s\"",
                       $cpus->[$i]{'model'}, $cpus->[$i]{'bogo'}, $info->{'minfo'}{'cpuid'}, $cpunum, $info->{'mach'};
            my $inum = @caches;
            for(my $j = 0; $j < $inum; $j++)
            {
                my $strcache = @caches[$j];
                my @arraycache = split(':', $strcache);
                printf $fd ", \"%s\":\"%s\"", @arraycache[0], @arraycache[1];
            }
            printf $fd "},\n";
            last;
            #printf $fd "          {\"description\":\"%s\"},\n", $cpus->[$i]{'flags'};
        }
    }

    my @dispcards;
    @dispcards = @{$info->{'minfo'}{'dispcards'}};
    $count = @dispcards;
    if($count == 0)
    {
       printf $fd "   {\"compent\":\"display_card\", \"description\":\" \"},\n";
    }
    else
    {
       for ($i = 0; $i < $count; $i++) {
        printf $fd "   {\"compent\":\"display_card\",     \"description\":\"%s\"},\n", @dispcards[$i];
        }
    }
    
    chomp($info->{'minfo'}{'memmanu'}),
    chomp($info->{'minfo'}{'memtype'}),
    chomp($info->{'minfo'}{'memspeed'});
    printf $fd "\n   {\"compent\":\"memory\", \"numa_topology\":\"%s\", \"total\":\"%s MB\", \"count\":\"%s\", \"manufacturer\":\"%s\", \"type\":\"%s\", \"speed\":\"%s\"},\n\n",
    $info->{'minfo'}{'numacount'},
    $info->{'memTotal'},
    $info->{'minfo'}{'memcount'},
    $info->{'minfo'}{'memmanu'},
    $info->{'minfo'}{'memtype'},
    $info->{'minfo'}{'memspeed'};

    if ($info->{'graphics'}) {
        #printf $fd "   {\"compent\":\"Graphics:\", \"description\":\"%s\"},\n", $info->{'graphics'};
    }

    @items0 = @{$info->{'minfo'}{'harddisks'}};
    $count = @items0;
    if($count == 0)
    {
       printf $fd "   {\"compent\":\"disks\", \"description\":\" \"},\n";
    }
    else
    {
       for ($i = 0; $i < $count; $i++) {
           if($i == 0)
           {
               printf $fd "   {\"compent\":\"disks\"";
           }
        printf $fd ", \"disks%d\":\"%s\"", $i+1, @items0[$i];
            if($i == $count-1)
            {
                printf $fd "},\n";
            }
       }
    }
    
    @items1 = @{$info->{'minfo'}{'ethcards'}};
    $count = @items1;
     if($count == 0)
     {
        printf $fd "   {\"compent\":\"netcards\", \"description\":\" \"}\n";
     }
     else{
       for ($i = 0; $i < $count; $i++) {
           if($i == 0)
           {
               printf $fd "   {\"compent\":\"netcards\", ";
           }

        printf $fd "\"card%d\":\"%s\"", $i+1, @items1[$i];
        if($i < $count - 1)
        {
            printf $fd ", ";
        }
        if($i == $count-1)
        {
            printf $fd ", \"mac1\":\"%s\"}\n", $info->{'minfo'}{'mac1'};
        }
       }
     }
    printf $fd "   ],\n";
    printf $fd "\n";
}

# Display the test scores from the given set of test results.
sub logResultsJson {
    my ( $results, $outFd ) = @_;

    # Display the individual test scores.
    printf $outFd "   \"CASETime\":[\n";
    my $i = 0;
    my $len = scalar(@{$results->{'list'}}); 
    foreach my $bench (@{$results->{'list'}}) {
        my $bresult = $results->{$bench};
        printf $outFd "{\"id\":\"%-25s\", \"logmsg\":\"%-40s\", \"score\":\"%12.4f %-5s \",  \"scorelabel\":\"(%.1f s, %d samples)\"}",
                      $bench,
                      $bresult->{'msg'},
                      $bresult->{'score'},
                      $bresult->{'scorelabel'},
                      $bresult->{'time'},
                      $bresult->{'iterations'};
        if($i < $len - 1){
            printf $outFd ",";
        }
        printf $outFd "\n";
        $i++;
    }
    printf $outFd "   ],\n";
}


# Display index scores, if any, for the given run results.
sub logIndexCatJson {
    my ( $info, $results, $cat, $outFd, $isumtmp, $inumtmp) = @_;

    my $catsRatioSum = $info->{'catsRatioSum'};
    my $total = $results->{'numIndex'}{$cat};
    my $indexed = $results->{'indexed'}{$cat};
    my $iscore = $results->{'index'}{$cat};
    my $fiscore = $results->{'findex'}{$cat};
    my $full = defined($indexed) && $indexed == $total;

    # If there are no indexed scores, just say so.
    if (!defined($indexed) || $indexed == 0) {
        printf $outFd "{\"tips\":\"No index results available for %s\"},\n\n",
                      $TestCases::testCats->{$cat}{'name'};
        return;
    }

    # Display the header, depending on whether we have a full set of index
    # scores, or a partial set.

    if ($catsRatioSum > 0) {
        #printf $outFd "{\"value\":\"%s\"},\n",
                 # "----------------------------------------------------------------------------------------------------------------------------";
    } else {
        #printf $outFd "{\"value\":\"%s\"},\n",
                  #"-----------------------------------------------------------------------------------------------------";
    }

    # Display the individual test scores.
    my $subinum = 0;
    my $subisum = 0;
    foreach my $bench (@{$results->{'list'}})
    {
       my $bresult = $results->{$bench};
        next if $bresult->{'cat'} ne $cat;
        if (defined($bresult->{'iscore'}) && defined($bresult->{'index'})) {
           $subisum++;
        }
    }

    foreach my $bench (@{$results->{'list'}}) {
        my $bresult = $results->{$bench};
        next if $bresult->{'cat'} ne $cat;

        if (defined($bresult->{'iscore'}) && defined($bresult->{'index'})) {
            printf $outFd "   {\"id\":\"%s\",  \"logmsg\":\"%s\", \"cat\":\"%s\", \"result\":%.4f, 
            \"units\":\"%s\", \"elapsedTimeInSec\":%.1f, \"samples\":%d, \"baseline\":%.4f, \"score\":%.4f}",
                      $bench, $bresult->{'msg'}, 
                      $cat,
                      $bresult->{'score'},
                      $bresult->{'scorelabel'},
                      $bresult->{'time'},
                      $bresult->{'iterations'},
                      $bresult->{'iscore'},
                      $bresult->{'index'};

           if($subinum < $subisum-1){
              printf $outFd ",\n";
           }

           #printf $outFd "\n========%d,======%d====,\n", $subinum, $subisum;
            if ($catsRatioSum > 0) {
                #printf $outFd " {\"id\":\"%12s\", \"findex\":\"%1.2e\"},",
                              #"$info->{'casesOpts'}->{$bench}{'weight'}",
                             # $bresult->{'findex'};
            }
            $subinum++;
        } else {
            #printf $outFd "{\"id\":\"%-25s\", \"logmsg\":\"%-40s %12s\",  \"score\":\"%12.4f %8s\"},",
                      #$bench, $bresult->{'msg'}, "---",
                      #$bresult->{'score'}, "---";
            if ($catsRatioSum > 0) {
                #printf $outFd " {\"id\":\"%12s %8s\"}", "----", "---";
            }
        }
        #printf $outFd "\n";
    }

    # Display the overall score.
    my $title = $TestCases::testCats->{$cat}{'name'} . " Index Score";
    if (!$full) {
        $title .= " (Partial Only)";
    }
    #printf $outFd "{\"id\":\"%-25s %-40s %12s %12s %8s %14s\"},", "", "", "", "", "========", "";
    if ($catsRatioSum > 0) {
        #printf $outFd " {\"id\":\"%8s\"},", "", "========";
    }
    #printf $outFd "\n";
    #printf $outFd "{\"id\":\"%-92s\", \"iscore\":\"%8.4f\"}", $title, $iscore;
     
    if($inumtmp < $isumtmp-1)
    {
        printf $outFd ",\n";
    }

    #printf $outFd "\n-------%d,---%d---,\n", $inumtmp, $isumtmp;

    if ($catsRatioSum > 0) {
        #printf $outFd "  {\"id\":\"%12s\",  \"iscore\":\"%8.4f\"},", "$TestCases::testCats->{$cat}{'casesRatioSum'}", $fiscore;
    }
}


# Display index scores, if any, for the given run results.
sub logIndexJson {
    my ( $info, $results, $inparams, $outFd) = @_;

    my $count = $results->{'indexed'};
    my $cscore = { };

    printf $outFd "   \"CASE\":[\n";
    my $isumtmp = scalar(keys(%$count));
    my $inumtmp = 0;
    foreach my $cat (sort(keys(%$count))) {
        $cscore->{$cat} = $results->{'index'}{$cat};
        logIndexCatJson($info, $results, $cat, $outFd, $isumtmp, $inumtmp);
        $inumtmp++;
    }
    printf $outFd "\n";
    printf $outFd "   ],\n";
    printf $outFd "\n";

    printf $outFd "   \"CAT\":[\n";
    my $inumcat = 0;
    my $isumcat = scalar(keys(%$count));
    foreach my $cat (sort(keys(%$count))) {
        printf $outFd "   {\"id\":\"%s\", \"name\":\"%s\", \"maxCopies\":\"%s\", \"ratio\":\"%s\", \"casesRatioSum\":\"%s\", \"score\":%.4f}", 
                $cat, $TestCases::testCats->{$cat}{'name'}, 
                $TestCases::testCats->{$cat}{'maxCopies'},
                $TestCases::testCats->{$cat}{'ratio'},
                $TestCases::testCats->{$cat}{'casesRatioSum'},
                $results->{'index'}{$cat};
                if($inumcat < $isumcat-1)
                {
                    printf $outFd ",\n";
                }
                $inumcat++;
    }
    printf $outFd "\n";
    printf $outFd "   ],\n";
    printf $outFd "\n";

    printf $outFd "   \"ENTIRE\":[\n";

    my $tStart=timelocal(localtime($results->{'start'}));
    my $tEnd=timelocal(localtime($results->{'end'}));
    my $tDiff=$tEnd - $tStart;
    my $tMinute=int($tDiff / 60);
    my $tSecond=$tDiff % 60;
    my $tHour=int($tDiff / 3600);
    $tMinute=$tMinute % 60;

    printf $outFd "   {\"id\":\"%s\", \"copies\":%d, \"threads\":%d, \"iterations\":%d, \"testfile\":\"%s\", \"basefile\":\"%s\", \"cmdargs\":\"%s\"},\n", 
            "input", 
            number($results->{'copies'}, "parallel copy", "parallel copies"),
            number($ENV{'OMP_NUM_THREADS'}, "thread"),
            $inparams->{'iterations'},
            $inparams->{'testfile'},
            $inparams->{'basefile'},
            $inparams->{'cmdargs'};

    printf $outFd "   {\"id\":\"%s\", \"begindate\":\"%s\", \"begintime\":\"%s\", \"endtime\":\"%s\", \"elapsedtime\":\"%sh%sm%ss\",\"score\":%.4f,
            \"totalcases\":%d, \"validcases\":%d, \"failcases\":%d, \"faillist\":\"%s\",\"reportstatus\":\"%s\"}\n",
            "output", 
            strftime("%Y-%m-%d", localtime($results->{'start'})),
            strftime("%H:%M:%S", localtime($results->{'start'})),
            strftime("%H:%M:%S", localtime($results->{'end'})),
            $tHour, $tMinute, $tSecond,
            $results->{'gmean'},
	    $results->{'totalCases'},
	    $results->{'validCases'},
	    $results->{'failCases'},
	    $results->{'failList'},
	    $results->{'reportStatus'};
    printf $outFd "   ]\n";
   
    printf $outFd "}\n";
    printf $outFd "\n";
}

# Dump the given run results into the given report file.
sub summarizeRunJson {
    my ( $info, $results, $verbose, $inparams, $reportFd ) = @_;

    # Display the indexed scores, if any.
    logIndexJson($info, $results, $inparams, $reportFd);
}

sub summarizeNullJson{
    my ( $reportFd ) = @_;
    printf $reportFd "   \"ENTIRE\":[\n";
    printf $reportFd "   ]\n";
   
    printf $reportFd "}\n";
    printf $reportFd "\n";
}
