#!/usr/bin/perl

use strict;
my $avoid_failure = {};

############################################################################
# CSV REPORTS
############################################################################

# Display the test scores from the given set of test results.
sub logResultsCsv {
    my ( $results, $outFd ) = @_;

    # Display the individual test scores.
    foreach my $bench (@{$results->{'list'}}) {
        my $bresult = $results->{$bench};

        printf $outFd "%s,%s,%.4f,%s,%.1f s, %d samples\n",
                      $bench,
                      $bresult->{'msg'},
                      $bresult->{'score'},
                      $bresult->{'scorelabel'},
                      $bresult->{'time'},
                      $bresult->{'iterations'};
    }

    printf $outFd "\n";
    printf $outFd "\n";
    printf $outFd "\n";
}


# Display index scores, if any, for the given run results.
sub logIndexCatCsv {
    my ( $info, $results, $cat, $outFd) = @_;

    my $catsRatioSum = $info->{'catsRatioSum'};
    my $total = $results->{'numIndex'}{$cat};
    my $indexed = $results->{'indexed'}{$cat};
    my $iscore = $results->{'index'}{$cat};
    my $fiscore = $results->{'findex'}{$cat};
    my $full = $total == $indexed;

    # If there are no indexed scores, just say so.
    if (!defined($indexed) || $indexed == 0) {
        printf $outFd "No index results available for %s\n\n",
                      $TestCases::testCats->{$cat}{'name'};
        return;
    }

    # Display the header, depending on whether we have a full set of index
    # scores, or a partial set.
    my $head = $cat . ($full ? " Index Values" : " Partial Index");
    printf $outFd "%s,%s,%s,%s,%s,",
                  "TestCases", $head, "BASELINE", "RESULT", "INDEX";
    if ($catsRatioSum > 0) {
        printf $outFd " %s,%s", "WEIGHT", "FINDEX";
    }
    printf $outFd "\n";

    # Display the individual test scores.
    foreach my $bench (@{$results->{'list'}}) {
        my $bresult = $results->{$bench};
        next if $bresult->{'cat'} ne $cat;

        if (defined($bresult->{'iscore'}) && defined($bresult->{'index'})) {
            printf $outFd "%s,%s,%.4f,%.4f,%.4f",
                      $bench, $bresult->{'msg'}, $bresult->{'iscore'},
                      $bresult->{'score'}, $bresult->{'index'};
            if ($catsRatioSum > 0) {
                printf $outFd ",%s,%1.2e",
                              "$info->{'casesOpts'}->{$bench}{'weight'}",
                              $bresult->{'findex'};
            }
        } else {
            printf $outFd "%s,%s,,%.4f,,",
                      $bench, $bresult->{'msg'},
                      $bresult->{'score'};
            if ($catsRatioSum > 0) {
                printf $outFd "%s,%.4f", "----", "---";
            }
        }
        printf $outFd "\n";
    }

    # Display the overall score.
    my $title = $TestCases::testCats->{$cat}{'name'} . " Index Score";
    if (!$full) {
        $title .= " (Partial Only)";
    }
    printf $outFd "%s,,,,%0.4f", $title, $iscore;

    if ($catsRatioSum > 0) {
        printf $outFd ",%s,%.4f", "$TestCases::testCats->{$cat}{'casesRatioSum'}", $fiscore;
    }

    printf $outFd "\n\n\n";
}


# Display index scores, if any, for the given run results.
sub logIndexCsv {
    my ( $info, $results, $outFd) = @_;

    my $count = $results->{'indexed'};
    my $cscore = { };

    foreach my $cat (sort(keys(%$count))) {
        $cscore->{$cat} = $results->{'index'}{$cat};
        logIndexCatCsv($info, $results, $cat, $outFd);
    }

    if ($info->{'catsRatioSum'} > 0) {
        printf $outFd "%s,%s,%s,%s,%s\n",
                      "CAT NAME", "CAT DESC", "FINDEX", "WEIGHT", "FFINDEX";
        foreach my $cat (sort(keys(%$count))) {
            printf $outFd "%s,%s,%.4f,%s,%.2e\n",
                      $cat,
                      $TestCases::testCats->{$cat}{'name'},
                      $results->{'findex'}{$cat},
                      "$TestCases::testCats->{$cat}{'ratio'}",
                      $results->{'ffiscore'}{$cat};
        }
        printf $outFd ",%s,,%s,%.4f\n", "FINAL TOTAL RESULT:",
                      "$info->{'catsRatioSum'}", $results->{'gmean'};
        printf $outFd "\n\n\n";
    } else {
        printf $outFd "%s,%s\n", "CAT NAME", "INDEX";
        foreach my $cat (sort(keys(%$count))) {
            printf $outFd "%s,%.4f\n", $cat, $results->{'index'}{$cat};
        }
        printf $outFd "%s,%.4f\n\n\n", "Composite Score", $results->{'gmean'};
    }
}

# Dump the given run results into the given report file.
sub summarizeRunCsv{
    my ( $info, $results, $verbose, $reportFd ) = @_;
    my $tStart=timelocal(localtime($results->{'start'}));
    my $tEnd=timelocal(localtime($results->{'end'}));
    my $tDiff=$tEnd - $tStart;
    my $tMinute=int($tDiff / 60);
    my $tSecond=$tDiff % 60;
    my $tHour=int($tDiff / 3600);
    $tMinute=$tMinute % 60;

    # Display information about this test run.
    printf $reportFd "Benchmark Run: %s %s - %s (%sh%sm%ss)\n",
           strftime("%Y-%m-%d", localtime($results->{'start'})),
           strftime("%H:%M:%S", localtime($results->{'start'})),
           strftime("%H:%M:%S", localtime($results->{'end'})),
           $tHour, $tMinute, $tSecond;
    printf $reportFd "%s in system; running %s of tests with %s;\n\nTotal Memory : %s MB\n",
           number($info->{'numCpus'}, "CPU"),
           number($results->{'copies'}, "parallel copy", "parallel copies"),
           number($ENV{'OMP_NUM_THREADS'}, "thread"),
           $info->{'memTotal'};
    printf $reportFd "\n";

    # Display the run scores.
    logResultsCsv($results, $reportFd);

    # Display the indexed scores, if any.
    logIndexCsv($info, $results, $reportFd);

    printf $reportFd "\n";
    my $tmpstr = sprintf ("REPORT STATUS, %s\nTotalCases, %d\nSuccessCases, %d\nFailCases, %d\nBasefile, %s\n",
        $results->{'reportStatus'}, $results->{'totalCases'}, $results->{'validCases'},
        $results->{'failCases'}, $results->{'baseFile'});
    my $failstr = "";
    if ($results->{'failCases'}) {
        $failstr = sprintf("FailList, %s\n\n",, $results->{'failList'});
    }
    printf $reportFd "%s%s", $tmpstr, $failstr;
}
