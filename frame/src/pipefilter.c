#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main(int argc, char *argv[])
{
	int ret, idx, len;
	char buf[0x200];

	while (true) {
		len = read(0, buf, sizeof(buf));
		if (len == 0) {
			break;
		} else if (len < 0) {
			fprintf(stderr, "pipe read failed.\n");
			return len;
		} else {
			for (idx = 0; idx < len; idx++) {
				if (buf[idx] == '\0')
					/* Use stdout to notify the warning, so that the caller easy known about it */
					fprintf(stdout, "!!!!!pipefilter catch invalid parameter!!!!!!!!!!!!!!\n");
			}
			for (idx = 0; idx < len; idx += ret) {
				ret = write(1, buf + idx, len - idx);
				if (ret < 0) {
					fprintf(stderr, "pipe write failed.\n");
					return ret;
				}
			}
		}
	}
	return 0;
}
