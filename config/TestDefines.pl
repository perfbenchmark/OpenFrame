#!/usr/bin/perl

package TestDefines;

use strict;

#######################################################################
# The testcase names which are compatible with the original UnixBench.
#######################################################################

our $whets = [
#    "whetstone-double"
];

# The tests which constitute the official index.
our $index = [
#   "whetstone-double",
#   "grep",
];

#######################################################################
# The testcase names for cppperf.
#######################################################################


# List of all supported test names.
our $testList = {
    # Compatible
#    "whets"         => $whets,
};

