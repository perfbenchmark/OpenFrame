#!/bin/bash
#!/bin/sh
BINDIR=$(cd "$(dirname "$0")";pwd)

#
# If we want to use the frame tools, we can do like blow.
#
# These tools are only optional.
#BINRUN="./main"
BINRUN="./coremark" 

WORKDIR=${BINDIR}/exec
#cp src/lmp_serial examples/melt/
cd "${WORKDIR}"

"$BINRUN" $@ > ./result$$.txt

sed -i -e "s/:.*//" ./result$$.txt

diff result$$.txt ../chk.txt
RET=$?
rm -rf result$$.txt
exit $RET
