#!/bin/bash
#!/bin/sh
BINDIR=$(cd "$(dirname "$0")";pwd)

#
# If we want to use the frame tools, we can do like blow.
#
# These tools are only optional.
#BINRUN="./main"
BINRUN="./scimark" 

WORKDIR=${BINDIR}/exec
#cp src/lmp_serial examples/melt/
cd "${WORKDIR}"
"$BINRUN" $@
exit $?
