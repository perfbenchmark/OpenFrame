#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "LU.h"
#include "FFT.h"
#include "SOR.h"
#include "MonteCarlo.h"
#include "Random.h" 
#include "Stopwatch.h"  
#include "SparseCompRow.h"
#include "array.h"
#include "kernel.h"

    void kernel_measureFFT(unsigned int N, double mintime, Random R,
      double *res, double *sum_, unsigned long *num_cycles) 
    {
        /* initialize FFT data as complex (N real/img pairs) */

        int twiceN = 2*N;
        long size = sizeof(double)*twiceN;
        double *x = RandomVector(twiceN, R);
        double *chk = malloc(size);
        unsigned long cycles = 1;
        Stopwatch Q = new_Stopwatch();
        int i=0;
        double result = 0.0;
        double sum = 0.0;

        memcpy(chk, x, size);
        while(1)
        {
            Stopwatch_start(Q);
            for (i=0; i<cycles; i++)
            {
                FFT_transform(twiceN, x);     /* forward transform */
                FFT_inverse(twiceN, x);       /* backward transform */
            }
            Stopwatch_stop(Q);
            for (i = 0; i < twiceN; i++)
            {
                long chk0 = (long)(x[i] * 1000);
                long chk1 = (long)(chk[i] * 1000);
                if (chk0 != chk1)
                {
                    fprintf(stderr, "FFT transform and revert are different for %d (%f, %f)",
                                    i, x[i], chk[i]);
                    exit(-1);
                }
            }
            if (Stopwatch_read(Q) >= mintime)
                break;

            cycles *= 2;

        }
        /* approx Mflops */

        result = FFT_num_flops(N)*cycles/ Stopwatch_read(Q) * 1.0e-6;
        Stopwatch_delete(Q);

        for (i=0; i<twiceN; i++)
        {
            sum += x[i];
        }
        sum /= twiceN;

        free(chk);
        free(x);

        *sum_ = sum;
        *res = result;
        *num_cycles = cycles;

    }

    void kernel_measureSOR(unsigned int N, double min_time, Random R,
            double *res, double *sum_, unsigned long *num_cycles)
    {
        double **G = RandomMatrix(N, N, R);
        double result = 0.0;
        double sum = 0.0, *data, chk = 0.0;
        unsigned int i=0, j=0;
        struct {
            int cyc;
            double chk;
        } chkdata[] = {
            {1, 49.853012},
            {2, 100.402227},
            {4, 151.557352},
            {8, 203.114752},
            {16, 254.875946},
            {32, 306.729432},
            {64, 358.637314},
            {128, 410.583459},
            {256, 462.554587},
            {512, 514.538782},
            {1024, 566.527252},
            {2048, 618.516438},
            {4096, 670.505647},
            {8192, 722.494856},
            {16384, 774.484065}
        };

        Stopwatch Q = new_Stopwatch();
        int cycles=1;
        while(1)
        {
            Stopwatch_start(Q);
            data = SOR_execute(N, N, 1.25, G, cycles);
            Stopwatch_stop(Q);
            for (i=0; i<N-1; i++)
            {
                chk += data[i];
            }
            for (i=0; i < sizeof(chkdata)/sizeof(chkdata[0]); i++)
            {
                if (cycles == chkdata[i].cyc)
                {
                    double diff = chk - chkdata[i].chk;
                    if ((diff > 0.001) || (diff < -0.001))
                    {
                        fprintf(stderr, "expect {%d, %f}, but real {%d, %f},\n",
                                        chkdata[i].cyc, chkdata[i].chk, cycles, chk);
                        exit (-1);
                    }
                }
            }

            if (Stopwatch_read(Q) >= min_time) break;

            cycles *= 2;
        }
        /* approx Mflops */

        result = SOR_num_flops(N, N, cycles) / Stopwatch_read(Q) * 1.0e-6;

        for (i=0; i<N; i++)
          for (j=0; j<N; j++)
              sum += G[i][j];
        sum /= (N*N);

        Array2D_double_delete(N, N, G);
        *res = result;
        *sum_ = sum;
        *num_cycles = cycles;

        Stopwatch_delete(Q);

    }



    void kernel_measureMonteCarlo(double min_time, Random R,
            double *res, double *sum_, unsigned long *num_cycles)
    {
        double result = 0.0;
        double sum = 0.0, ret;
        Stopwatch Q = new_Stopwatch();

        unsigned long cycles=1;
        while(1)
        {
            Stopwatch_start(Q);
            ret = MonteCarlo_integrate(cycles);
            Stopwatch_stop(Q);
            sum += ret;
            ret -= 3.14;
            if (cycles >= 0x400000)
            {
                if ((ret > 0.05) || (ret < -0.05))
                {
                    fprintf(stderr, "ret: %f with cycles: %lx, but expect: 3.14.\n", ret, cycles);
                    exit(-1);
                }
            }
            else if (cycles >= 0x1000)
            {
                if ((ret > 0.5) || (ret < -0.5))
                {
                    fprintf(stderr, "ret: %f with cycles: %lx, but expect: 3.14.\n", ret, cycles);
                    exit(-1);
                }
            }
            if (Stopwatch_read(Q) >= min_time) break;

            cycles *= 2;
        }
        /* approx Mflops */
        result = MonteCarlo_num_flops(cycles) / Stopwatch_read(Q) * 1.0e-6;
        Stopwatch_delete(Q);
        *res = result;
        *sum_ = sum;
        *num_cycles = cycles;
    }


    void kernel_measureSparseMatMult(unsigned int N, unsigned int nz, 
            double min_time, Random R,
            double *res, double *sum_, unsigned long *num_cycles)
    {
        /* initialize vector multipliers and storage for result */
        /* y = A*y;  */

        double *x = RandomVector(N, R);
        double *y = (double*) malloc(sizeof(double)*N);


        double result = 0.0;
        double sum = 0.0;

#if 0
        // initialize square sparse matrix
        //
        // for this test, we create a sparse matrix with M/nz nonzeros
        // per row, with spaced-out evenly between the begining of the
        // row to the main diagonal.  Thus, the resulting pattern looks
        // like
        //             +-----------------+
        //             +*                +
        //             +***              +
        //             +* * *            +
        //             +** *  *          +
        //             +**  *   *        +
        //             +* *   *   *      +
        //             +*  *   *    *    +
        //             +*   *    *    *  + 
        //             +-----------------+
        //
        // (as best reproducible with integer artihmetic)
        // Note that the first nr rows will have elements past
        // the diagonal.
#endif

        int nr = nz/N;      /* average number of nonzeros per row  */
        int anz = nr *N;    /* _actual_ number of nonzeros         */

            
        double *val = RandomVector(anz, R);
        int *col = (int*) malloc(sizeof(int)*nz);
        int *row = (int*) malloc(sizeof(int)*(N+1));
        int r=0;
        int cycles=1;
        int i=0;
        double chk;
        struct {
            int cyc;
            double chk;
        } chkdata[] = {
            {1, 1190.647239},
            {2, 3571.941716},
            {4, 8334.530670},
            {8, 17859.708579},
            {16, 36910.064397},
            {32, 75010.776032},
            {64, 151212.199302},
            {128, 303615.045843},
            {256, 608420.738924},
            {512, 1218032.125086},
            {1024, 2437254.897411},
            {2048, 4875700.442061},
            {4096, 9752591.531361},
            {8192, 19506373.709961},
            {16384, 39013938.067160},
            {32768, 78029066.781559},
            {65536, 156059324.210356},
            {131072, 312119839.067960},
            {262144, 624240868.783056},
        };

        Stopwatch Q = new_Stopwatch();

        for (i=0; i<N; i++)
            y[i] = 0.0;

        row[0] = 0; 
        for (r=0; r<N; r++)
        {
            /* initialize elements for row r */

            int rowr = row[r];
            int step = r/ nr;
            int i=0;

            row[r+1] = rowr + nr;
            if (step < 1) step = 1;   /* take at least unit steps */


            for (i=0; i<nr; i++)
                col[rowr+i] = i*step;
                
        }


        while(1)
        {
            Stopwatch_start(Q);
            SparseCompRow_matmult(N, y, val, row, col, x, cycles);
            Stopwatch_stop(Q);
            sum = 0;
            for (i=0; i<N; i++)
                sum += y[i];
            for (i=0; i<sizeof(chkdata)/sizeof(chkdata[0]); i++)
            {
                if (cycles==chkdata[i].cyc)
                {
                    chk = sum - chkdata[i].chk;
                    if ((chk > 0.0001) || (chk < -0.0001))
                    {
                        fprintf(stderr, "cur {%d, %f}, but expect {%d, %f}\n",
                                        cycles, sum, chkdata[i].cyc, chkdata[i].chk);
                        exit(-1);
                    }
                }
            }
            if (Stopwatch_read(Q) >= min_time) break;

            cycles *= 2;
        }
        /* approx Mflops */
        result = SparseCompRow_num_flops(N, nz, cycles) / 
                        Stopwatch_read(Q) * 1.0e-6;

        sum = 0;
        for (i=0; i<N; i++)
            sum += y[i];
        sum /= N;

        Stopwatch_delete(Q);
        free(row);
        free(col);
        free(val);
        free(y);
        free(x);

        *res = result;
        *sum_ = sum;
        *num_cycles = cycles;
    }

    void solveLU(unsigned int N, double **LU, int *pvt, double *b)
    {
        int ii = 0, i, j;

        for (i = 0; i < N; i++) {
            int ip = pvt[i];
            double sum = b[ip];

            b[ip] = b[i];
            if (ii == 0)
                for (j = 0; j < i; j++)
                    sum -= LU[i][j] * b[j];
            else if (sum == 0.0)
                ii = i;
            b[i] = sum;
        }

        for (i = N - 1; i >= 0; i--) {
            double sum = b[i];
            for (j = i + 1; j < N; j++)
                sum -= LU[i][j] * b[j];
            b[i] = sum / LU[i][i];
        }
    }

    void check_resultLU(unsigned int N, double **lu, int *pivot, double **A, Random R)
    {
        double *b = RandomVector(N, R);
        double *x = malloc(N * sizeof(double));
        double *y = malloc(N * sizeof(double));
        double chk = 0.0;
        int i, j;

        if (!b || !x || !y)
            exit(1);

        for (i = 0; i < N; i++)
            x[i] = b[i];

        solveLU(N, lu, pivot, x);

        for (i = 0; i < N; i++) {
            y[i] = 0.0;
            for (j = 0; j < N; j++)
                y[i] += A[i][j] * x[j];
        }

        for (i = 0; i < N; i++)
            chk += fabs(b[i] - y[i]);
        if ((chk > 0.000001) || (chk < -0.000001)) {
            fprintf(stderr, "chk: %f, but expect 0.0.\n", chk);
            exit(-1);
        }
    }


    void kernel_measureLU(unsigned int N, double min_time, Random R,
        double *res, double *sum_, unsigned long *num_cycles)
    {

        double **A = NULL;
        double **lu = NULL; 
        int *pivot = NULL;

    

        Stopwatch Q = new_Stopwatch();
        double result = 0.0;
        double sum = 0.0;
        int i=0, j=0;
        int cycles=1;
        int N2 = N/2;

        if ((A = RandomMatrix(N, N,  R)) == NULL) exit(1);
        if ((lu = new_Array2D_double(N, N)) == NULL) exit(1);
        if ((pivot = (int *) malloc(N * sizeof(int))) == NULL) exit(1);

        for (i=0; i<N; i++)
          for (j=0; j<N; j++)
              lu[i][j] = 0.0;

        /* make sure A is diagonally dominant, to avoid singularity */
        /* set diagonal to be 4 times the absolute value of its row sum */
        for (i=0; i<N; i++)
        {
            double row_sum = 0.0;
            /* compute row sum of absoluate values  */
            for (j=0; j<N; j++)
                row_sum +=  fabs( A[i][j] );
           A[i][i] = 4 * row_sum;
        }

        while(1)
        {
            Stopwatch_start(Q);
            for (i=0; i<cycles; i++)
            {
                double lu_center = fabs(lu[N2][N2]);
                Array2D_double_copy(N, N, lu, A);

                /* add modification to A, based on previous LU */
                /* to avoid being optimized out. */
                /*   lu_center = max( A_center, old_lu_center) */

                lu[N2][N2]= (A[N2][N2] > lu_center ? A[N2][N2] : lu_center);

                LU_factor(N, N, lu, pivot);
            }
            Stopwatch_stop(Q);
            check_resultLU(N, lu, pivot, A, R);
            if (Stopwatch_read(Q) >= min_time) break;
            cycles *= 2;
        }

        /* approx Mflops */
        result = LU_num_flops(N) * cycles / Stopwatch_read(Q) * 1.0e-6;

        Stopwatch_delete(Q);
        free(pivot); 

        for (i=0; i<N; i++)
          for (j=0; j<N; j++)
            sum += lu[i][j];
        sum /= (N*N);

        Array2D_double_delete(N, N, lu); 
        Array2D_double_delete(N, N, A);

        *res = result;
        *sum_ = sum;
        *num_cycles = cycles;

    }

