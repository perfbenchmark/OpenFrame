#!/bin/bash
#
# $1: minimize port
# $2: maximize port

if [ $# == 4 ]; then
echo "Usage: $0 weight_in_kilos length_in_centimeters"
UB_TMPDIR=$4
fi

MINPORT=$1
MAXPORT=$2
RETFILE=$3
COUNT=$(($MAXPORT - $MINPORT))
PORT=$MINPORT
i=0
BINDIR=$(cd "$(dirname "$0")";pwd)

TMPFILE="${UB_TMPDIR}/cur_port"

cd "${BINDIR}"

get_a_port() {
	if [ -f "${TMPFILE}" ]
	then
		PORT=`cat "${TMPFILE}"`
		PORT="$(($PORT+1))"
		if [ $PORT -ge $MAXPORT ] || [ $PORT -lt $MINPORT ]
		then
			PORT=$MINPORT
		fi
	fi
	echo "try: " $PORT " between " $MAXPORT " and " $MINPORT
	echo $PORT > "${TMPFILE}"
}

while ((i < $COUNT))
do
	get_a_port
	if [ $(netstat -atu | grep -v "tcp6\|udp6" | awk '{print $4}' | grep $PORT | wc -l) = "0" ]
	then
		echo $PORT > "${RETFILE}"
		exit 0
	else
		i=$(($i+1))
	fi
done

exit -1;
