#!/bin/bash

BINDIR=$(cd "$(dirname "$0")";pwd)
BINNAME=$2
cd "${BINDIR}/../workloads/$1"
shift 2
./$BINNAME "$@"
exit $?
