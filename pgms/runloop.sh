#!/bin/bash
cd "$(dirname "$0")";pwd
BINDIR=$(cd "$(dirname "$0")"; pwd)

#echo "paras=<$@>"

# it is too big for the log of cppperf, so redirect it to /dev/null
$BINDIR/loopcalc $@ 1>/dev/null

