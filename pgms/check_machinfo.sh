#!/bin/bash

basedir=$(cd "$(dirname "$0")"; pwd)

infofile="$basedir/../config/machine.info"

echo -e "\nCheck $infofile:"
if [ ! -e $infofile ]; then
  echo "    Error: cannot find config/machine.info"
  exit 1
fi 

val=$(grep "brandmodel=" $infofile | awk -F= '{print $2}' | sed -s 's/ //g')
if [ "x$val" == "x" ]; then
  echo "    Error: value of brandmodel is empty in config/machine.info"
  exit 2
fi

while read line
do
  echo $line | grep -q "^#"
  if [ $? -eq 0 ]; then
    continue
  fi

  key=$(echo $line | awk -F= '{print $1}')
  value=$(echo $line | awk -F= '{print $2}')
  if [ -z "$value" ]; then
    echo "    $key has no value"
  fi
done < $infofile

