#!/bin/bash

curdir=$(cd "$(dirname "$0")"; pwd);
rootdir="${curdir}/.."

jnum=$1
makelog=$2

echo "begin to build: concurrent num=${jnum}, makelog=${makelog}."
cd ${rootdir}

timeS=$(date '+%Y-%m-%d %H:%M:%S')

make -j${jnum} 2>&1 | tee -a ${makelog}

# get make's return code
ret=${PIPESTATUS[0]}

timeE=$(date '+%Y-%m-%d %H:%M:%S')

echo -e "\nMakeTime Begin: $timeS" | tee -a ${makelog}
echo -e "MakeTime End  : $timeE"   | tee -a ${makelog}
echo -e "make return <$ret>\n\n"   | tee -a ${makelog}

exit $ret

