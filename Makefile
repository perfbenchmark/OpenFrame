#
# Toolchains
#
export SHELL = /bin/sh
export CC=gcc
export CXX=g++
export JAVAC=javac
export JAVA=java
export FORTRAN=gfortran
export STDCXXV=c++11

#
# Environments
#
TOPDIR = $(shell pwd)
CONFDIR = ./config
PROGDIR = ./pgms
FRMDIR = ./frame
SRCDIR = $(FRMDIR)/src
RESULTDIR = ./results
TMPDIR = $(FRMDIR)/tmp
WLDIR = ./workloads


#
# Customize the building items.
#

WKLDCFG = workloads.cfg

ifneq ($(wildcard $(WKLDCFG)),)
  include $(WKLDCFG)
else
  DIRS = $(shell ls $(WLDIR) | grep -v Makefile | xargs)
endif

# It is a tool which may be used by workloads.
BINS += $(PROGDIR)/loopcalc $(PROGDIR)/pipefilter

#
# Default optimization flags for c, c++, java, fortran, linking.
#
# *OPTFLAGS0: it has the best level, the most features, and the least limits of performance in *OPTFLAGS?.
# *OPTFLAGS1: it shouldn't have better level, or more features, or less limits of performance than *OPTFLAGS0
# *OPTFLAGS2: it shouldn't have better level, or more features, or less limits of performance than *OPTFLAGS1
#  ...
#  ...
# *OPTFLAGSn: it shouldn't have better level, or more features, or less limits of performance than *OPTFLAGS{n-1}
#
COPTFLAGS0 = -O2
COPTFLAGS1 = -O1
COPTFLAGS2 = -O0
CXXOPTFLAGS0 = -O2
CXXOPTFLAGS1 = -O1
CXXOPTFLAGS2 = -O0
FOPTFLAGS0 =
FOPTFLAGS1 =
FOPTFLAGS2 =
JOPTFLAGS0 =
JOPTFLAGS1 =
JOPTFLAGS2 =
LDOPTFLAGS0 =
LDOPTFLAGS1 =
LDOPTFLAGS2 =

BASEOPTFILELIST = ./listbaseopt.mk

#
# Base Optimization Configuration
#
ifeq ($(BASEOPTCFG),)
  BASEOPTCFG = $(CONFDIR)/baseopt.mk
endif
ifneq ($(wildcard $(BASEOPTCFG)),)
  include $(BASEOPTCFG)
endif

#
# Testcases Peak Optimization Configuration for the each workload programs
#
ifeq ($(PEAKOPTCFG),)
  PEAKOPTCFG = peakopt.mk
endif
ifneq ($(wildcard $(PEAKOPTCFG)),)
  include $(PEAKOPTCFG)
endif

#
# Main working flow
#

.PHONY: all clean spotless

all: $(BINS) $(DIRS)
	@if  test ! -d  $(TMPDIR) \
        ; then  \
           mkdir $(TMPDIR) \
        ; fi
	@if  test ! -d  $(RESULTDIR) \
        ; then  \
           mkdir $(RESULTDIR) \
        ; fi
	@echo "COPTFLAGS0 = $(COPTFLAGS0)" > $(BASEOPTFILELIST)
	@echo "COPTFLAGS1 = $(COPTFLAGS1)" >> $(BASEOPTFILELIST)
	@echo "COPTFLAGS2 = $(COPTFLAGS2)" >> $(BASEOPTFILELIST)
	@echo "CXXOPTFLAGS0 = $(CXXOPTFLAGS0)" >> $(BASEOPTFILELIST)
	@echo "CXXOPTFLAGS1 = $(CXXOPTFLAGS1)" >> $(BASEOPTFILELIST)
	@echo "CXXOPTFLAGS2 = $(CXXOPTFLAGS2)" >> $(BASEOPTFILELIST)
	@echo "FOPTFLAGS0 = $(FOPTFLAGS0)" >> $(BASEOPTFILELIST)
	@echo "FOPTFLAGS1 = $(FOPTFLAGS1)" >> $(BASEOPTFILELIST)
	@echo "FOPTFLAGS2 = $(FOPTFLAGS2)" >> $(BASEOPTFILELIST)
	@echo "JOPTFLAGS0 = $(JOPTFLAGS0)" >> $(BASEOPTFILELIST)
	@echo "JOPTFLAGS1 = $(JOPTFLAGS1)" >> $(BASEOPTFILELIST)
	@echo "JOPTFLAGS2 = $(JOPTFLAGS2)" >> $(BASEOPTFILELIST)
	@echo "LDOPTFLAGS0 = $(LDOPTFLAGS0)" >> $(BASEOPTFILELIST)
	@echo "LDOPTFLAGS1 = $(LDOPTFLAGS1)" >> $(BASEOPTFILELIST)
	@echo "LDOPTFLAGS2 = $(LDOPTFLAGS2)" >> $(BASEOPTFILELIST)

$(DIRS):
	$(eval $@_COPTFLAGS0 ?= $(COPTFLAGS0))
	$(eval $@_COPTFLAGS1 ?= $(COPTFLAGS1))
	$(eval $@_COPTFLAGS2 ?= $(COPTFLAGS2))
	$(eval $@_CXXOPTFLAGS0 ?= $(CXXOPTFLAGS0))
	$(eval $@_CXXOPTFLAGS1 ?= $(CXXOPTFLAGS1))
	$(eval $@_CXXOPTFLAGS2 ?= $(CXXOPTFLAGS2))
	$(eval $@_FOPTFLAGS0 ?= $(FOPTFLAGS0))
	$(eval $@_FOPTFLAGS1 ?= $(FOPTFLAGS1))
	$(eval $@_FOPTFLAGS2 ?= $(FOPTFLAGS2))
	$(eval $@_JOPTFLAGS0 ?= $(JOPTFLAGS0))
	$(eval $@_JOPTFLAGS1 ?= $(JOPTFLAGS1))
	$(eval $@_JOPTFLAGS2 ?= $(JOPTFLAGS2))
	$(eval $@_LDOPTFLAGS0 ?= $(LDOPTFLAGS0))
	$(eval $@_LDOPTFLAGS1 ?= $(LDOPTFLAGS1))
	$(eval $@_LDOPTFLAGS2 ?= $(LDOPTFLAGS2))
	$(eval OPTFLAGS0 = CFLAGS_OPT0="$($@_COPTFLAGS0)" CXXFLAGS_OPT0="$($@_CXXOPTFLAGS0)" \
			FFLAGS_OPT0="$($@_FOPTFLAGS0)" JFLAGS_OPT0="$($@_JOPTFLAGS0)" LDFLAGS_OPT0="$($@_LDOPTFLAGS0)")
	$(eval OPTFLAGS1 = CFLAGS_OPT1="$($@_COPTFLAGS1)" CXXFLAGS_OPT1="$($@_CXXOPTFLAGS1)" \
			FFLAGS_OPT1="$($@_FOPTFLAGS1)" JFLAGS_OPT1="$($@_JOPTFLAGS1)" LDFLAGS_OPT1="$($@_LDOPTFLAGS1)")
	$(eval OPTFLAGS2 = CFLAGS_OPT2="$($@_COPTFLAGS2)" CXXFLAGS_OPT2="$($@_CXXOPTFLAGS2)" \
			FFLAGS_OPT2="$($@_FOPTFLAGS2)" JFLAGS_OPT2="$($@_JOPTFLAGS2)" LDFLAGS_OPT2="$($@_LDOPTFLAGS2)")
	$(eval OPTFLAGS = $(OPTFLAGS0) $(OPTFLAGS1) $(OPTFLAGS2))
	+make $(OPTFLAGS) -C "$(WLDIR)/$@"

clean:
	$(RM) $(BINS) $(ALLBINS) core *~ */*~ *.cfg
	@for subdir in $(shell ls $(WLDIR) | grep -v Makefile | xargs)\
	; do \
           make -C "$(WLDIR)/$${subdir}" clean \
	; done

spotless: clean items_clean
	$(RM) -rf $(TMPDIR)/
	$(RM) -rf listbaseopt.mk
	$(RM) -rf TestCases.pl
	$(RM) -f *.log

#
# framework
#
include $(FRMDIR)/Makefile

#
# Testcases Peak Optimization Configuration for the integrated test programs
#
ifeq ($(IPEAKOPTCFG),)
  IPEAKOPTCFG = peakopt_integrated.mk
endif
ifneq ($(wildcard $(IPEAKOPTCFG)),)
  include $(IPEAKOPTCFG)
endif
