# 小工具

## perl 代码优化
TestCases.pl 中的基准用例格式读起来不是很优雅，可以用 perltidy 命令优化。
1. 安装 perltidy 工具 `apt install perltidy`
2. 执行命令 `perltidy -cab=0 -l=200 TestCases.pl`
3. 会生成对应的.tdy文件，查看即可 `vim TestCases.pl.tdy`


