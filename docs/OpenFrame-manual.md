# 《OpenFrame SDK开发使用手册》

## 简介

OpenFrame 是一个开放平台框架，服务于各个计算机性能评估基准工具。采用perl语言开发。

为支撑多种使用场景、不同测试目标、统一评价模型，OpenFrame 打造了层次丰富、配置灵活、使用简洁的平台工具。

为融合数量众多、类型各异的测试用例，OpenFrame 提供了接口统一、集成便捷的框架服务。

代码路径：https://gitee.com/perfbenchmark/OpenFrame 。

 

| 特性 | 描述                                              |
| -------------- | ------------------------------------------------------------ |
| 多种使用场景   | 支持存储业务、计算服务、图形图像等多种使用场景               |
| 不同测试目标   | 面向存储服务器、web服务器、图形工作站、台式机、笔记本等不同测试目标 |
| 统一评价模型   | 针对带宽、延迟、时长等指标进行归一化处理，提供统一的分数评价模型 |
| 层次丰富       | 包括测试用例层、测试分类层和测试集合层，使得平台层次丰富，为配置灵活、使用简洁两个特性构建主体结构 |
| 配置灵活       | 可配测试用例权重、测试分类权重、运行参数、编译优化等，从而平台配置灵活，为多种使用场景、不同测试目标打造核心能力 |
| 使用简洁       | 具有集中可见、易学易用、缺省默认的参数配置，支撑平台使用简洁，为用户快速上手营造便利环境 |
| 数量众多       | 支撑成百上千的独立程序，方便框架集成数量众多的测试用例，为多种使用场景、不同测试目标提供有效支持 |
| 类型各异       | 涵盖CPU、网络、磁盘、图形、运行时等测试用例，协助框架支持类型各异的测试用例，为多种使用场景、不同测试目标形成关键支撑 |
| 接口统一       | 提供标准的输入输出、运行命令、支持工具等，使得框架接口统一，为统一评价模型夯实重要基础 |
| 集成便捷       | 仅需较少改动测试用例即可完成集成，由此框架对测试用例集成便捷，为测试用例给予有效服务 |

## 逻辑特性

### 负载

输入、基准处理、输出、结果校验。

### 框架

1. 编译配置、环境配置、负载分组；
2. 编译、运行、评分；
3. 日志、报告；
4. 其它；

### 兼容性

兼容操作系统：统信操作系统、麒麟操作系统、Ubuntu等。

兼容硬件架构：龙芯、鲲鹏、飞腾、海光、兆芯、申威等CPU。

 

## 使用方法

运行方式很简单，直接执行./Run即可。默认执行一轮用例全集，进行单进程测试。

## 命令参数说明

执行 ./Run help，提示信息如下：

```
Usage:
    Run [Options] [testcases]
    Run [Options] [testsuites]

Examples:
    Run
    Run -i 2
    Run -i 3 -c 2
    Run -i 3 -c 2 dhry2reg fstime
    Run -i 1 -t 3 hpcg_s
    Run -i 1 -c 2 -t 4 COMPLEX FILEIO GRAPHIO NETIO RUNTIME SYSTEM
    Run -i 3 -c 4 -f example_testcases_min
    Run -i 3 -c 4 -b indexbase

Options:
    -i: set iterations number
    -c: set copies number
    -t: set threads number
    -h: show help info

    -q: show log in quiet mode
    -v: show log in verbose mode
    -a, -affinity: specify affinity
    -f: specify config file with weight
    -listcases: show testcases list
    -listcasesattr: show testcases attribute list
    -onlybuild: compile testcases

Details:
    The testcases should be choosed from below: < hpcg_s dhry2reg fstime >, total number=3.
    The testsuites should be choosed from below: < COMPLEX FILEIO GRAPHIO NETIO RUNTIME SYSTEM >.
    If testcases and testsuites are not specified, default value is the all testcases.
```



参数说明：

-i：指定迭代运行次数；

-c：指定多任务副本数；

-t：指定单任务并发线程数；

-f：指定权重配置文件，该文件在config目录下；

## 命令运行示例

1. 执行./Run，运行默认参数，运行两轮用例全集，分别是单核测试和满核测试。
2. 执行./Run -c 8 -i 3 RUNTIME，运行8任务副本、3次，用例为RUNTIME用例组。
3. 执行./Run -c 1 -i 5 dhry2reg，运行单任务副本、5次，用例为dhry2reg。
4. 执行./Run -t 8 -i 5 hpcg_s，运行单任务8线程、5次，用例为hpcg_s。
5. 执行./Run -c 1 -i 1 -f example_min，运行单进程、单次，用例见config/example_min文件。
6. 执行./Run -c 1 -i 1 -b desktop，运行单进程、单次，用例见config/desktop.base文件。

## 代码结构

### 代码目录

```
├── config
│   ├── baseopt.mk
│   ├── example_testcases_min
│   ├── server.base
│   ├── desktop.base
│   ├── remote_ip
│   └── TestDefines.pl
├── docs
│   ├── config.txt
│   └── results.txt
├── frame
│   ├── bins.mk
│   ├── items.mk
│   ├── output
│   │   ├── Console.pl
│   │   ├── CSV.pl
│   │   ├── Html.pl
│   │   └── Text.pl
│   ├── peakopt.mk
│   ├── Run.pl
│   ├── src
│   │   └── support
│   ├── TestCases.plt
│   └── testdir
│       └── benchmark.logo
├── Makefile
├── pgms
│   ├── ofdiff/
│   ├── ofdiff.sh
│   ├── runloop.sh
│   └── try_workloads.sh
├── README.md
├── Run
├── TestCases.pl
└── workloads
    └── Makefile
```

详细说明：

| 目录或文件               | 功能             |      |
| ---------------------------- | -------------------- | ---- |
| config/ TestCases.pl         | 负载配置             |      |
| config/TestDefines.pl        | 负载或负载组名称定义 |      |
| config/server.base            | 服务器操作系统负载的基线配置  |      |
| config/desktop.base            | 桌面操作系统负载的基线配置   |      |
| config/opt.cfg               | 编译优化选项         |      |
| config/example_testcases_min | 负载权重配置示例     |      |
| docs                         | 说明文档             |      |
| Makefile                     | 编译文件             |      |
| results                      | 测试报告和测试日志   |      |
| Run                          | Shell脚本，程序入口  |      |
| frame/                       | 框架代码             |      |
| frame/Run.pl                 | 框架核心             |      |
| frame/src/support            | 支撑性代码           |      |
| frame/TestCases.plt          | API 接口模板         |      |
| workloads/                   | 工作负载             |      |
| workloads/demo/CaseConfig.pl | 负载接口             |      |

### 基准配置

负载的参数配置方式：包括负载名、参数选项、迭代模式、日志输出、依赖等。对应代码TestCases.pl中变量 $testParams 和Run中的 $baseParams。

| 名称       | 默认值       | 含义                                                         |
| ---------- | ------------ | ------------------------------------------------------------ |
| prog       | 默认为负载名 | 该基准指定的可执行程序                                       |
| options    | “”           | 可执行程序的命令行参数                                       |
| repeat     | short        | 负载的迭代次数模式。long模式：和迭代次数一致；short模式：迭代次数除以3；single模式：只能执行一次 |
| stdout     | 1            | 非0：将负载的标准输出写入到日志文件中；0：如果负载的标准输出过多，可以置零； |
| stdin      | “”           | 指定 testdir目录的一个文件作为输入参数送给可执行程序，例如dc负载的输入数据文件为dc.dat |
| logmsg     | “”           | 该负载的详细信息                                             |
| buildep    | “”           | 该负载依赖的可执行二进制程序，例如dc负载依赖looper           |
| maxThreads | 1            | 该负载的最大并发数。如果设置为1 ，说明只支持单任务单线程运行，例如lat_udp负载 |
| maxCopies  | 0            | 该负载的最大副本数。如果设置为1 ，说明只支持单副本运行。     |
| checkself  | 0            | 设置为0表示没有自检，1表示有自校验, 10表示需人工校验（例如图形显示） |

负载的系统分组配置：包括分组名称、分组权重配置等。对应TestCases.pl中变量 $testCats 。

| 字段名称     | 含义                                                         |      |
| ------------ | ------------------------------------------------------------ | ---- |
| name         | 分类名称，默认配有6个分类: SYSTEM、FILEIO、NETIO、GRAPHIO、COMPLEX、RUNTIME |      |
| maxCopies    | 如果设置为1，说明只支持单核运行，例如GRAPHIO负载             |      |
| ratio        | -f 参数指定配置文件中，该分类的权重。                        |      |
| caseRatioSum | -f 参数指定配置文件中，该分类中所有负载的权重之和            |      |

同时也支持自定义分组配置：方式1：在TestDefines.pl中定义负载分组。方式二：在config中建立自定义的分组负载方式以及权重配置，通过-f参数指定。

### 评分基线设置

包括负载名、运行时间、单位、迭代次数等。对应index.base ，以负载dc为例，字段含义如下：

```dc|60|lpm|0|8515.6|3```

| No.  | 字段名称    | 含义                                                         | dc     | 测试报告  | 日志格式 |
| ---- | ----------- | ------------------------------------------------------------ | ------ | --------- | -------- |
| 1    | name        | 负载名                                                       | dc     | dc        |          |
| 2    | time        | 负载运行时间间隔，(TIME \| elapsed)                          | 60     | 30.0s     | COUNT1   |
| 3    | score label | 运行得分的单位                                               | lpm    | lpm       | COUNT2   |
| 4    | sum         | 和score相同，该字段已弃用                                    | 0      | ---       |          |
| 5    | score       | 得分                                                         | 8515.6 | 47979.9   | COUNT0   |
| 6    | iters       | 运行次数，该字段未用，实际使用了入参-i。long: iters=10, samples=7short: iters=3, samples=2single: iters=1, samples=1 | 3      | 2 samples |          |

results测试报告： ```dc Dc: sqrt(2) to 99 decimal places 47979.9 lpm (30.0 s, 2 samples)```

标准错误捕获格式：```COUNT|score|timebase|prefix```。

例如 ```fprintf(stderr,"COUNT|%lu|60|lpm\n", iter);```


### 添加一个新负载

例如添加一个新负载名为sample，步骤如下：

1. 将新负载源码放入workloads目录，在workloads/sample中添加Makefile。

2. 源码中新负载源码的输出方式修改，```fprintf(stderr,"COUNT|%lu|60|lpm\n", iter);``` 输出结果是值越大性能越好。

3. 在workloads/sample中添加CaseConfig.pl中添加新负载配置；

4. 在index.base中添加新负载基线；

5. 执行Run [新负载名称]，进行验证；

### 负载测试结果单位含义

| Unit | Description                                        | 说明     |
| -------------- | ------------------------------------------------------------ | ------------------ |
| fps            | Frame Per Second                                             | 每秒帧数           |
| KBps           | Killo Bytes Per Second                                       | 每秒的千字节数     |
| lpd            | Loops Per Day                                                | 每天运行次数       |
| lph            | Loops Per Hour                                               | 每小时运行次数     |
| lpm            | Loops Per Minute                                             | 每分钟运行次数     |
| lpmo           | Loops Per Month                                              | 每月运行次数       |
| lps            | Loops Per Second                                             | 每秒运行次数       |
| MBps           | Million Bytes Per Second                                     | 每秒的兆字节数     |
| Mflops         | Million Floating Point Operations Per Second                 | 每秒的兆浮点运算数 |
| MWIPS          | Million Whetstones Instructions Per Second                   | 每秒的兆指令数     |
| Bps            | Bytes Per Second                                             | 每秒字节数         |
| recps          | Records Per Second. (from ebizzy)                            | 每秒记录数         |
| iops           | IO (the geometric mean of read, write, and other) Per Second. | 每秒的IO数         |